# Design system

This is the code implementation of the design system as according to the
[Figma](https://www.figma.com/file/WRWUWArhAkmxpA5QJVFw1i/Rainbow-Design-System) designs.

The project is based on React, Typescript and Vite. Components are extended from [NextUI](https://nextui.org/) as much as possible.
[Tailwind](https://tailwindcss.com/docs) is used for styling, and [Storybook](https://storybook.js.org/docs/react/get-started/why-storybook) for documentation.

It is also published as a component library for other Astron apps to use:

<img src="src/assets/docs/design-system.svg" alt="design-system" />

## Deployment

Deployment is automated via Gitlab CI/CD.

Test environment: https://sdc-dev.astron.nl/design-system/  
Production environment: https://sdc.astron.nl/design-system/

Release notes: https://gitlab.com/astron-sdc/design-system/-/releases

## Usage

Steps to use this design system in your client application:

1. Create a `.npmrc` file in your repo with the following

   ```
   @astron-sdc:registry=https://gitlab.com/api/v4/packages/npm/
   ```

2. Install the package and its dependencies:

   ```shell
   npm i @astron-sdc/design-system @nextui-org/react react-datepicker
   ```

3. Import the Tailwind config:

   ```javascript
   // tailwind.config.js
   import tailwindConfig from "@astron-sdc/design-system/tailwind.config.js";

   /** @type {import("tailwindcss").Config} */

   export default {
     ...tailwindConfig,
     // your overrides
   };
   ```

4. You should now be able to use all components and classnames.

## Contact

This repository is maintained by Team Rainbow. Feel free to contact us on
[Slack](https://radio-observatory.slack.com/archives/C02H4G65ERL)!

## Contributing

Install nodejs, clone the repository, then run

```shell
npm install
npm run storybook
```

to see the Storybook at http://localhost:6006/.

### Docs

General documentation that is not related to any component in particular are written as `.mdx`
files in the `src/docs/` directory, and can be viewed at http://localhost:6006/?path=/docs/.

### Components

For consistency, please extend from the NextUI component library as much as possible.

Each component is paired with a story file, which is responsible for showcasing said component
on the Storybook. The story file for `src/components/ComponentName` shall be found at
`src/stories/ComponentName.stories.ts`.

For components meant for external use (by other apps), remember to export them in `src/index.ts`.

### Icons

This is the current workflow:

Download or copy the SVG code from Figma. Save it into `src/assets/icons/your-file-name.svg`.
In the file, change the stroke color to `currentColor`. This is to enable dynamic stroke colors.
Add an _export_ entry to the `src/assets/icons/index.ts`:

```tsx
export { default as YourFileName } from "./your-file-name.svg";
```

Then, you can use your icon like this (note the casing):

```tsx
<Icon name="YourFileName" color="secondary" />
```

### Classnames

We use `clsx` to concatenate classnames.

Since Tailwind [doesn't support](https://tailwindcss.com/docs/content-configuration#dynamic-class-names)
dynamic class names, there are some functions in `src/components/utils/classes.ts` to help reduce boilerplate
code. They also convert the color names that we use in Figma into the semantic color names in NextUI.
Please use them (or add new ones there) as much as possible instead of supplying colors via another
manner. So we can keep the styling code consistent and organized.

### Linting

The command `npm run lint-staged-add` lints staged files and stages the changes. It is configured
as a pre-commit hook so in principle does not need to be manually run. There is also
`npm run lint-changed` to lint changed but not added files.

Imports are sorted and absolute, but relative imports are allowed for siblings.

Please use [semantic commit messages](https://www.conventionalcommits.org/en/v1.0.0/#summary)
when committing, this is to enable automatic releases and changelogs.

### Sonar Cloud
The Pipeline runs SonarCloud.  You can also get Sonarcloud insights by adding the sonarlint plugin into your ide

### Adding dependencies

All additional dependencies should be in the `devDependencies` category. There should be nothing in
`dependencies`.

### Using a local version instead of using the npm registry in your client application

1. Run the following in the `design-system` directory:

```
npm run build
cd dist; npm link
```

2. Run the following in your client application directory:

```
npm uninstall @astron-sdc/design-system
npm link @astron-sdc/design-system
```

You can now start your client application and the symlinked local design-system directory will be used. You need to run `npm run build` again to apply changes in the design system. 

N.B. running `npm install` in your client application will break the link.


### Testing

#### Test runner

> WARNING: The test runner tests are very slow at the moment.
> They don't add much value as they are right now, so it is not recommended to run them.

When you run tests for the first time or when Playwright is updated,
run the following command to install dependencies:

```
npx playwright install --with-deps
```

Also make sure you have a running instance of storybook on your localhost (see [above](#Contributing)).

```
npm run test-storybook
```

checks that all stories render without error across major browsers.

#### Accessibility tests

In each rendered story, there is an Accessibility tab which shows whether the component complies
with accessibility rules. 


## License

Apache License 2.0
