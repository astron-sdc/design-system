import { nextui } from "@nextui-org/react";

const palette = {
  baseWhite: "#FFFFFF",
  baseBlack: "#000000",
  lightGrey: "#DADADA",
  grey: "#777777",
  mediumGrey: "#313131",
  darkModeBlue: "#000022",
  darkBlue: "#181828",
  lightBlue: "#00ADEE",
  blue: "#3A5896",
  yellow: "#EEEE22",
  orange: "#FF9933",
  red: "#FF4444",
  green: "#25CC25",
  violet: "#DD55FF",
  turquoise: "#11BBAA",
};

/** @type {import('tailwindcss').Config} */

export default {
  content: [
    "src/components/**/*.{ts,tsx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    fontFamily: {
      heading: ["Montserrat", "Arial", "sans-serif"],
      body: ["Inter", "Arial", "sans-serif"],
    },
    colors: {
      neutral: palette.turquoise,
      ...palette,
    },
    extend: {
      boxShadow: {
        "3xl": "0 0 8px rgba(0, 0, 0, 0.15)",
      },
      animation: {
        fadeIn: 'fadeIn .7s ease-in-out',
      },

      keyframes: {
        fadeIn: {
          from: { opacity: 0 },
          to: { opacity: 1 },
        },
      },
    },
  },
  plugins: [
    nextui({
      themes: {
        light: {
          colors: {
            background: palette.baseWhite,
            foreground: {
              heading: palette.blue,
              heading2: palette.lightBlue,
              body: palette.mediumGrey,
            },
            primary: palette.lightBlue,
            secondary: palette.blue,
            warning: palette.orange,
            success: palette.green,
            danger: palette.red,
            default: "transparent", // needs to be transparent for some components
          },
        },
        dark: {
          colors: {
            background: palette.darkBlue,
            foreground: {
              heading: palette.baseWhite,
              heading2: palette.lightBlue,
              body: palette.baseWhite,
            },
            primary: palette.lightBlue,
            secondary: palette.blue,
            warning: palette.orange,
            success: palette.green,
            danger: palette.red,
            default: "transparent", // needs to be transparent for some components
          },
        },
      },
    }),
  ],
  darkMode: "class",
};
