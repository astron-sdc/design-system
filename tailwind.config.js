import { nextui } from "@nextui-org/react";
import { astronAdditional, astronBrand, baseUI } from "./palette.ts";

const palette = {
  ...astronBrand,
  ...astronAdditional,
  ...baseUI,
};

/** @type {import('tailwindcss').Config} */

const tailwindConfig = {
  content: [
    "src/components/**/*.{ts,tsx}",
    "src/stories/**/*.{ts,tsx}",
    "src/layouts/**/*.{ts,tsx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    fontFamily: {
      heading: ["Montserrat", "Arial", "sans-serif"],
      body: ["Inter", "Arial", "sans-serif"],
    },
    colors: palette,
    extend: {
      boxShadow: {
        "3xl": "0 0 8px rgba(0, 0, 0, 0.15)",
        "4xl": "0px 0px 20px 0px rgba(0, 0, 0, 0.10)",
        "left-bar-4": "inset 4px 0px 0px currentColor",
        "design-shadow-r": "0px 0px 20px 0px #0000001A",
      },
      animation: {
        fadeIn: "fadeIn .7s ease-in-out",
      },

      keyframes: {
        fadeIn: {
          from: { opacity: 0 },
          to: { opacity: 1 },
        },
      },
    },
  },
  plugins: [
    nextui({
      themes: {
        
        light: {
          colors: {
            background: {
              DEFAULT: palette.baseWhite,
              panel: palette.baseWhite,
              sidebar: palette.baseWhite,
              input: {
                disabled: palette.lightGrey,
                DEFAULT: palette.baseWhite,
              },
            },
            foreground: {
              heading: palette.blueWCAG,
              heading2: palette.lightBlue,
              body: palette.mediumGrey,
            },
            primary: palette.lightBlue,
            secondary: palette.blueWCAG,
            warning: palette.orange,
            success: palette.green,
            danger: palette.red,
            neutral: palette.turquoise,
            default: "transparent", // needs to be transparent for some components
          },
        },
        dark: {
          colors: {
            background: {
              panel: palette.darkModePanel,
              sidebar: palette.darkBlue,
              input: {
                disabled: palette.astronBg,
                DEFAULT: palette.mediumGrey,
              },
            },
            foreground: {
              heading: palette.baseWhite,
              heading2: palette.lightBlue,
              body: palette.baseWhite,
            },
            primary: palette.lightBlue,
            secondary: palette.blueWCAG,
            warning: palette.orange,
            success: palette.green,
            danger: palette.red,
            neutral: palette.turquoise,
            default: "transparent", // needs to be transparent for some components
          },
        },
      },
    }),
  ],
  darkMode: "class",
};

export default tailwindConfig;
