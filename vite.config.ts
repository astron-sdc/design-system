import react from "@vitejs/plugin-react";
import { resolve } from "path";
import { defineConfig } from "vite";
import dts from "vite-plugin-dts";
import tsConfigPaths from "vite-tsconfig-paths";
import * as packageJson from "./package.json";
import { viteStaticCopy } from "vite-plugin-static-copy";

export default defineConfig({
  plugins: [
    react(),
    tsConfigPaths(),
    dts({
      include: ["src"],
    }),
    viteStaticCopy({
      targets: [
        { src: "./tailwind.config.js", dest: "." },
        { src: "./palette.ts", dest: "." },
      ],
    }),
  ],
  build: {
    lib: {
      entry: resolve("src", "index.ts"),
      name: "astron-design-system",
      formats: ["es", "cjs"],
      fileName: (format) =>
        `astron-design-system.${format === "cjs" ? "cjs" : "es.js"}`,
    },
    rollupOptions: {
      external: Object.keys(packageJson.peerDependencies),
    },
  },
  resolve: {
    alias: {
      src: "/src", // required for absolute imports
    },
  },
});
