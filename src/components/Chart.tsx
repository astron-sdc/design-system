import { Chart as PrimeChart } from "primereact/chart";
import { Chart as ChartJS } from "chart.js";

// TODO: import from tailwind config file
const duplicatedFromTailwindConfig = {
  fontFamily: {
    body: ["Inter", "Arial", "sans-serif"],
  },
  colors: {
    mediumGrey: "#313131",
    lightGrey: "#DADADA",
    lightBlue: "#00ADEE",
  },
};

ChartJS.defaults.font.family =
  duplicatedFromTailwindConfig.fontFamily.body.join(", ");

export type ChartType = "bar" | "doughnut" | "line" | "pie";
export type LegendPosition = "top" | "right" | "bottom" | "left";

export type BarChartOptions = {
  skipNull?: boolean;
};

export type PieChartOptions = {
  cutout?: number | string;
  radius?: number | string;
  circumference?: number;
  rotation?: number;
};

export type LineChartOptions = {
  spanGaps?: boolean | number;
  showLine?: boolean;
};

export type ChartOptions = BarChartOptions | PieChartOptions | LineChartOptions;

export type ChartData = {
  labels: string[];
  datasets: {
    label: string;
    data: (number | null)[];
  }[];
};

export type ChartProps = {
  data: ChartData;
  options?: ChartOptions;
  chartType: ChartType;
  customClass?: string;
  useDarkMode?: boolean;
  x_label?: string;
  y_label?: string;
  legend_position?: LegendPosition;
};

type ScaledChartOptions = {
  scales?: {
    x?: {
      grid?: {
        drawBorder?: boolean;
        lineWidth?: number;
      };
      border?: {
        display?: boolean;
      };
      ticks?: {
        color?: string;
      };
      title?: {
        display?: boolean;
        text?: string;
      };
    };
    y?: {
      grid?: {
        drawBorder?: boolean;
        lineWidth?: number;
        color?: (context: { index: number }) => string;
      };
      border?: {
        display?: boolean;
      };
      ticks?: {
        color?: string;
      };
      title?: {
        display?: boolean;
        text?: string;
      };
    };
  };
};

const colors = {
  light: {
    chartTitleColor: duplicatedFromTailwindConfig.colors.mediumGrey,
    gridLineYColor: duplicatedFromTailwindConfig.colors.lightGrey,
    gridLabelColor: duplicatedFromTailwindConfig.colors.mediumGrey,
  },
  dark: {
    chartTitleColor: duplicatedFromTailwindConfig.colors.lightGrey,
    gridLineYColor: duplicatedFromTailwindConfig.colors.mediumGrey,
    gridLabelColor: duplicatedFromTailwindConfig.colors.lightGrey,
  },
};

const defaultBarColors = {
  backgroundColor: duplicatedFromTailwindConfig.colors.lightBlue,
  borderWidth: 0,
};

const Chart = ({
  data,
  options,
  chartType,
  customClass,
  x_label,
  y_label,
  useDarkMode = false,
  legend_position = "top",
}: ChartProps) => {
  const colorTheme = useDarkMode ? "dark" : "light";

  const baseScaledChartOptions: ScaledChartOptions = {
    scales: {
      x: {
        grid: {
          drawBorder: false,
          lineWidth: 0,
        },
        border: {
          display: false,
        },
        ticks: {
          color: colors[colorTheme].gridLabelColor,
        },
        title: {
          display: !!x_label,
          text: x_label,
        },
      },
      y: {
        grid: {
          drawBorder: false,
          lineWidth: 1,
          color: (context) =>
            context.index === 0
              ? "transparent"
              : colors[colorTheme].gridLineYColor,
        },
        border: {
          display: false,
        },
        ticks: {
          color: colors[colorTheme].gridLabelColor,
        },
        title: {
          display: !!y_label,
          text: y_label,
        },
      },
    },
  };

  const finalChartOptions = {
    color: colors[colorTheme].chartTitleColor,
    ...options,
    ...(["bar", "line"].includes(chartType) ? baseScaledChartOptions : {}),
    plugins: {
      legend: {
        position: legend_position,
      },
    },
  };

  data.datasets = data.datasets.map((dataset) => ({
    ...dataset,
    ...(["bar"].includes(chartType) ? defaultBarColors : {}),
  }));

  return (
    <PrimeChart
      data={data}
      options={finalChartOptions}
      type={chartType}
      className={customClass}
    />
  );
};

export default Chart;
