import { forwardRef } from "react";
import DatePicker, { ReactDatePickerCustomHeaderProps } from "react-datepicker";

import { format } from "date-fns";
import { BaseInput, LabelPlacement } from "./utils/baseComponents.tsx";
import Icon from "./Icon.tsx";
import Typography from "./Typography.tsx";

import { textColor } from "./utils/classes.ts";

export type DateNull = Date | null;
export type DateArray = [DateNull, DateNull];

type DateInputProps = {
  isRequired?: boolean;
  isDisabled?: boolean;
  label: string;
  labelPlacement?: LabelPlacement;
  labelClass?: string;
  labelCustomColor?: boolean;
  isRange?: boolean;
  value?: DateNull;
  startDate?: DateNull;
  endDate?: DateNull;
  onValueChange: (date: Date | DateArray) => void;
  isInvalid?: boolean;
  errorMessage?: string;
  dateFormat?: string;
  showTimeSelect?: boolean;
  timeFormat?: string;
};

const DateInput = ({
  isRequired = false,
  isDisabled = false,
  label,
  labelPlacement = "outside",
  labelClass = "",
  labelCustomColor = false,
  value,
  startDate,
  endDate,
  onValueChange,
  isInvalid = false,
  errorMessage = "",
  isRange = false,
  dateFormat = "dd/MM/yyyy",
  showTimeSelect = false,
  timeFormat = "HH:mm",
}: DateInputProps) => {
  if (value && (startDate || endDate || isRange))
    throw Error(
      "Supply `value` when `isRange` is false (default), OR supply `startDate` and `endDate` when `isRange` is true."
    );

  type inputProps = {
    isRequired: boolean;
    value?: string;
    onClick?: () => void;
  };

  const CustomInput = forwardRef(
    (
      { isRequired, value, onClick }: inputProps,
      // @ts-expect-error the "ref" prop is needed to avoid console errors.
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      ref
    ) => {
      return (
        <div onClick={onClick}>
          <BaseInput
            isRequired={isRequired}
            isDisabled={isDisabled}
            label={label}
            labelPlacement={labelPlacement}
            labelClass={labelClass}
            labelCustomColor={labelCustomColor}
            endContent={
              <Icon
                name="Datepicker"
                // Note: textColor("body") has a different grey, hence the below is used
                customClass="text-grey dark:text-baseWhite"
              />
            }
            placeholder={isRange ? " " : dateFormat}
            value={value}
            onValueChange={() => {}}
            isInvalid={isInvalid}
            errorMessage={errorMessage}
          />
        </div>
      );
    }
  );

  type PaginationProps = {
    text: string;
    onClickPrev: () => void;
    onClickNext: () => void;
  };

  const CustomPagination = ({
    text,
    onClickPrev,
    onClickNext,
  }: PaginationProps) => (
    <div className="flex flex-row justify-between items-center px-2 h-[30px] border-b-1 border-grey">
      <button onClick={onClickPrev} className="w-[16px] h-[16px]">
        <Icon name="Previous" color="body" />
      </button>
      <Typography
        text={text}
        variant="note"
        customColor={true}
        customClass={textColor("body")}
      />
      <button onClick={onClickNext} className="w-[16px] h-[16px]">
        <Icon name="Next" color="body" />
      </button>
    </div>
  );

  const datePickerBaseProps: object = {
    dateFormat: `${dateFormat}${showTimeSelect ? " " + timeFormat : ""}`,
    wrapperClassName: "w-full",
    customInput: <CustomInput isRequired={isRequired} />,
    showPopperArrow: false,
    calendarStartDay: 1,
    formatWeekDay: (day: string) => <div>{day.charAt(0)}</div>,
    weekDayClassName: () =>
      "bg-baseWhite dark:bg-mediumGrey text-grey w-[33px] h-[30px] pt-[2px] m-0 mb-[5px]",
    calendarClassName:
      "!bg-baseWhite dark:!bg-mediumGrey !font-body border-none rounded-[4px] shadow-3xl",
    renderCustomHeader: ({
      date,
      decreaseYear,
      increaseYear,
      decreaseMonth,
      increaseMonth,
    }: ReactDatePickerCustomHeaderProps) => (
      <div className="!bg-baseWhite dark:!bg-mediumGrey rounded-t-[4px]">
        <CustomPagination
          text={format(date, "yyyy")}
          onClickPrev={decreaseYear}
          onClickNext={increaseYear}
        />
        <CustomPagination
          text={format(date, "MMMM")}
          onClickPrev={decreaseMonth}
          onClickNext={increaseMonth}
        />
      </div>
    ),
  };

  const singleDatePickerProps: object = {
    openToDate: value || new Date(),
    selected: value,
    showTimeSelect: showTimeSelect,
    timeFormat: timeFormat,
    dayClassName: () =>
      "!text-foreground-body !bg-default aria-selected:!bg-lightBlue",
    timeClassName: () =>
      "!text-foreground-body !bg-default dark:!bg-mediumGrey aria-selected:!bg-lightBlue dark:aria-selected:!bg-lightBlue",
  };

  const rangeDatePickerProps: object = {
    openToDate: startDate || new Date(),
    selected: startDate,
    startDate: startDate,
    endDate: endDate,
    dayClassName: () =>
      "!text-foreground-body bg-default hover:aria-[selected=false]:bg-default mx-0 w-8",
  };

  const datePickerProps = {
    ...datePickerBaseProps,
    ...(isRange ? rangeDatePickerProps : singleDatePickerProps),
  };

  return (
    <DatePicker
      selectsRange={isRange}
      onSelect={(date) => onValueChange(date)}
      onChange={(date) => !!date && onValueChange(date)}
      {...datePickerProps}
      popperPlacement={
        labelPlacement === "outside-left" ? "bottom-end" : "bottom-start"
      }
      disabled={isDisabled}
    />
  );
};

export default DateInput;
