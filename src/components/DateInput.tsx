import { fromDate, getLocalTimeZone, ZonedDateTime, now } from "@internationalized/date";
import { DatePicker } from "@nextui-org/date-picker";
import { Dispatch, ReactNode, SetStateAction } from "react";
import { I18nProvider } from "@react-aria/i18n";
import InputLabel from "src/components/utils/InputLabel";
import { baseInputClassNames, inputWidth, LabelPlacement } from "src/components/utils/BaseInput";
import Icon from "src/components/Icon";

export type DateNull = Date | null;

type DateInputProps = {
  value: DateNull;
  onValueChange: Dispatch<SetStateAction<DateNull>>;
  isRequired?: boolean;
  isDisabled?: boolean;
  label: string | ReactNode;
  labelPlacement?: LabelPlacement;
  labelClass?: string;
  labelCustomColor?: boolean;
  isInvalid?: boolean;
  errorMessage?: string;
  showTimeSelect?: boolean;
  locale?: string;
  forceUTC?: boolean;
};

const DateInput = ({
  value,
  onValueChange,
  label,
  isDisabled = false,
  isRequired = false,
  labelPlacement = "outside",
  labelClass = "",
  labelCustomColor = false,
  isInvalid = false,
  errorMessage = "",
  showTimeSelect = false,
  locale = "en-GB",
  forceUTC = false,
}: DateInputProps) => {
  const classNames = baseInputClassNames(isDisabled, labelPlacement);

  const getDateValue = (): ZonedDateTime => {
    if (forceUTC) {
      return fromDate(value ?? new Date(), "UTC")
    } else {
      return fromDate(value ?? new Date(), getLocalTimeZone())
    }
  }

  return (
    <InputLabel
      label={{
        text: label,
        customClass: labelClass,
        customColor: labelCustomColor,
      }}
      labelPlacement={labelPlacement}
    >
      <I18nProvider locale={locale}>
        <DatePicker
          aria-label="Choose Date"
          value={getDateValue()}
          onChange={(v) => v && onValueChange(v.toDate())}
          isDisabled={isDisabled}
          isRequired={isRequired}
          isInvalid={isInvalid}
          errorMessage={errorMessage}
          granularity={showTimeSelect ? "minute" : "day"}
          hideTimeZone={false}
          defaultValue={now(getLocalTimeZone())}
          disableAnimation
          // style
          variant={"bordered"}
          hourCycle={24}
          selectorIcon={
            <Icon
              name="Datepicker"
              customClass="text-grey dark:text-baseWhite"
            />
          }
          selectorButtonProps={{ variant: "faded" }}
          popoverProps={{
            crossOffset: -10,
            classNames: { content: "rounded-md shadow-3xl" },
            placement: labelPlacement === "outside-left" ? "bottom-end" : "bottom-start",
          }}
          dateInputClassNames={{
            base: classNames.mainWrapper,
            inputWrapper: classNames.inputWrapper,
            segment: "data-[invalid=true]:!text-foreground",
          }}
          calendarProps={{
            prevButtonProps: { variant: "faded" },
            nextButtonProps: { variant: "faded" },
          }}
          timeInputProps={{
            radius: "sm",
            variant: "bordered",
          }}
          classNames={{
            base: inputWidth(labelPlacement),
            calendar: "!font-heading !font-bold uppercase",
          }}
        />
      </I18nProvider>
    </InputLabel>
  );
};
export default DateInput;
