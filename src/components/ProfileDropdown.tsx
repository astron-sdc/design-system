import { Avatar } from "@nextui-org/react";
import { useState } from "react";
import MenuBase, { MenuProps } from "src/components/utils/MenuBase.tsx";
import Typography from "./Typography";
import Icon from "./Icon";

const getInitials = (name: string) => {
  const [firstName, lastName] = name.split(" ");
  return (firstName.charAt(0) + lastName.charAt(0)).toUpperCase();
};

const ProfileDropdown = ({ label, menu }: MenuProps) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggleState = () => setIsOpen((prev) => !prev);

  const Trigger = (
    <div
      className="flex cursor-pointer gap-2 items-center mb-2 float-right"
      onClick={toggleState}
    >
      <Typography
        text={label}
        variant="h5"
        customColor={true}
        customClass="text-foreground-body"
      />
      <Avatar
        isFocusable
        color="primary"
        className="text-baseWhite w-7 h-7 shrink-0"
        fallback={
          <Typography
            text={getInitials(label)}
            variant="note"
            customColor={true}
          />
        }
      />
      <Icon name={isOpen ? "Up" : "Dropdown"} color="primary" />
    </div>
  );

  return (
    <MenuBase
      trigger={Trigger}
      menu={menu}
      customClass="w-72 absolute left-[-290px]"
      placement="bottom-end"
    />
  );
};

export default ProfileDropdown;
