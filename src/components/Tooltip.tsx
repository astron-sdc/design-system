import { Tooltip as NextTooltip } from "@nextui-org/react";
import { OverlayPlacement } from "@nextui-org/aria-utils";
import { ReactNode } from "react";
import Typography from "src/components/Typography.tsx";

type TooltipProps = {
  text: string;
  children: ReactNode;
  placement?: OverlayPlacement;
};

const Tooltip = ({ text, children, placement }: TooltipProps) => (
  <NextTooltip
    // if this style shows up more than once, can put the text style into tailwind config
    content={
      <Typography
        text={text}
        variant="paragraph"
        customColor={true}
        customClass="text-background"
      />
    }
    placement={placement}
    showArrow={true}
    color="foreground"
    shadow="md"
    radius="sm"
    offset={10}
  >
    {children}
  </NextTooltip>
);

export default Tooltip;
