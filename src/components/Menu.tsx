import { Button as NextButton } from "@nextui-org/button";
import MenuBase, { MenuProps } from "src/components/utils/MenuBase.tsx";
import Icon from "./Icon.tsx";
import { buttonBaseClass } from "./Button.tsx";
import Typography from "./Typography.tsx";

const ActionMenu = ({ label, menu, isDisabled }: MenuProps) => {
  const Trigger = (
    <NextButton
      startContent={<Icon name="Dropdown" customClass="text-baseBlack" />}
      className={buttonBaseClass} // same as Button component (src/components/Button.tsx)
      type="button"
      color="primary"
      isDisabled={isDisabled}
    >
    {label && (
        <Typography text={label} variant="paragraph" customColor={true}/>
    )}
    </NextButton>
  );

  return (
    <MenuBase
      trigger={Trigger}
      menu={menu}
      customClass="w-48"
      placement="bottom-start"
    />
  );
};
export default ActionMenu;
