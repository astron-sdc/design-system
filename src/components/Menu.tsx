import {
  Avatar,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownTrigger,
} from "@nextui-org/react";
import { ReactNode } from "react";
import { Button as NextButton } from "@nextui-org/button";
import { clsx } from "clsx";
import { BreadcrumbItemProps } from "@react-types/breadcrumbs";
import Icon, { IconName } from "./Icon.tsx";
import { buttonBaseClass } from "./Button.tsx";
import Typography from "./Typography.tsx";
import { textColor } from "./utils/classes.ts";

type MenuComponentProps = {
  menuTrigger: ReactNode;
  onAction?: (key: string | number) => void;
  items: MenuItemProps[];
};

export interface MenuItemProps extends BreadcrumbItemProps {
  iconName?: IconName | null;
  key: string;
  itemName: string;
  href?: string;
  danger?: boolean;
}

export const MenuComponent = ({
  menuTrigger,
  onAction,
  items,
}: MenuComponentProps) => {
  if (!items.length) throw Error("`items` must not be empty");

  const MenuItem = (
    { iconName, key, itemName, href, danger = false, children }: MenuItemProps,
    idx: number
  ) => {
    const itemContent = () => {
      if (children) return children;
      return (
        <>
          {iconName && <Icon name={iconName} />}
          <Typography text={itemName} variant="paragraph" customColor={true} />
        </>
      );
    };
    return (
      <DropdownItem
        key={key}
        href={href || ""}
        showDivider={idx < items.length - 1}
      >
        <div
          className={clsx(
            "flex flex-row gap-3 items-center pl-2",
            danger ? textColor("negative") : textColor("body")
          )}
        >
          {itemContent()}
        </div>
      </DropdownItem>
    );
  };

  const padding: string = "py-0.5 px-0";

  return (
    <Dropdown classNames={{ content: padding }}>
      <DropdownTrigger>{menuTrigger}</DropdownTrigger>
      <DropdownMenu onAction={onAction} classNames={{ base: padding }}>
        {items.map((item, idx) => MenuItem(item, idx))}
      </DropdownMenu>
    </Dropdown>
  );
};

type MenuProps = {
  label: string;
  items: MenuItemProps[];
  onAction?: (key: string | number) => void;
};
export const ActionMenu = ({ label, items, onAction }: MenuProps) => {
  return (
    <MenuComponent
      items={items}
      onAction={onAction}
      menuTrigger={
        <NextButton
          startContent={<Icon name="Dropdown" />}
          className={buttonBaseClass} // same as Button component (src/components/Button.tsx)
          type="button"
          color="primary"
        >
          {label && (
            <Typography text={label} variant="paragraph" customColor={true} />
          )}
        </NextButton>
      }
    />
  );
};

export const ProfileMenu = ({ label, items, onAction }: MenuProps) => {
  const getInitials = (name: string) => {
    const [firstName, lastName] = name.split(" ");
    return (firstName.charAt(0) + lastName.charAt(0)).toUpperCase();
  };
  return (
    <MenuComponent
      items={items}
      onAction={onAction}
      menuTrigger={
        <div className="min-w-72 max-w-fit flex flex-row gap-2 items-center aria-expanded:scale-1 aria-expanded:opacity-100">
          <Typography
            text={label}
            variant="overtitle"
            customColor={true}
            customClass={textColor("body")}
          />
          <Avatar
            isFocusable
            color="primary"
            className="w-[28px] h-[28px] text-baseWhite"
            fallback={
              <Typography
                text={getInitials(label)}
                variant="note"
                customColor={true}
              />
            }
          />
          <Icon name="Dropdown" color="primary" />
        </div>
      }
    />
  );
};
