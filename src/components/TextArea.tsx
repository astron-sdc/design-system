import { clsx } from "clsx";
import { Textarea } from "@nextui-org/react";
import { TextInputProps } from "./TextInput";
import { formFieldBg, cursorPointer } from "./utils/classes.ts";
import { LabelPlacement } from "./utils/baseComponents.tsx";
import Typography from "./Typography.tsx";

interface TextAreaProps extends Omit<TextInputProps, OmittedTextInputProps> {
  minRows?: number;
  maxRows?: number;
  disableAutosize?: boolean;
  labelPlacement?: LabelPlacement;
  labelClass?: string;
  labelCustomColor?: boolean;
}

type OmittedTextInputProps = "isClearable" | "type" | "inputMode";

type TextAreaSlots =
  | "base"
  | "label"
  | "inputWrapper"
  | "input"
  | "description"
  | "errorMessage";

type TextAreaClassNames = { [key in TextAreaSlots]?: string };

const classNames = (
  isDisabled: boolean,
  disableAutosize: boolean,
  labelPlacement: LabelPlacement
): TextAreaClassNames => {
  const inputWrapperClass = (isDisabled: boolean) =>
    isDisabled
      ? "text-mediumGrey dark:text-darkModeBlue"
      : "text-grey dark:text-mediumGrey hover:text-baseBlack focus-within:!text-primary";

  return {
    base: clsx(
      {
        "flex flex-row justify-between items-start":
          labelPlacement === "outside-left",
      },
      cursorPointer(isDisabled)
    ),
    label: clsx("!text-foreground-body ml-[1px]", {
      "w-1/3": labelPlacement === "outside-left",
    }),
    inputWrapper: clsx(
      inputWrapperClass(isDisabled),
      "px-2 rounded-[4px]",
      formFieldBg(isDisabled)
    ),
    input: clsx("!text-foreground-body bg-default", {
      "resize-y": disableAutosize,
    }),
  };
};

const TextArea = ({
  isRequired = false,
  isDisabled = false,
  isReadOnly = false,
  label = "",
  minRows = 3,
  maxRows = 8,
  maxLength,
  value,
  onValueChange,
  isInvalid = false,
  errorMessage,
  disableAutosize = false,
  labelPlacement = "outside",
  labelClass = "",
  labelCustomColor = false,
}: TextAreaProps) => {
  return (
    <Textarea
      isRequired={isRequired}
      isDisabled={isDisabled}
      isReadOnly={isReadOnly}
      label={
        label && (
          <Typography
            text={label}
            variant="paragraph"
            customClass={labelClass}
            customColor={labelCustomColor}
          />
        )
      }
      minRows={minRows}
      maxRows={maxRows}
      maxLength={maxLength}
      value={value}
      onValueChange={onValueChange}
      isInvalid={isInvalid}
      errorMessage={errorMessage}
      disableAutosize={disableAutosize}
      labelPlacement={labelPlacement}
      variant="bordered"
      classNames={classNames(isDisabled, disableAutosize, labelPlacement)}
    />
  );
};

export default TextArea;
