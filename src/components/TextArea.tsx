import { clsx } from "clsx";
import { Textarea } from "@nextui-org/react";
import { InputSlots } from "@nextui-org/theme";
import {
  inputWidth,
  LabelPlacement,
  mainWrapperClass,
} from "src/components/utils/BaseInput.tsx";
import InputLabel from "src/components/utils/InputLabel.tsx";
import { TextInputProps } from "./TextInput";
import { formFieldBg, cursorPointer } from "./utils/classes.ts";
import Typography, { paragraphFont } from "./Typography.tsx";

interface TextAreaProps extends Omit<TextInputProps, OmittedTextInputProps> {
  minRows?: number;
  maxRows?: number;
  disableAutosize?: boolean;
  labelPlacement?: LabelPlacement;
  labelClass?: string;
  labelCustomColor?: boolean;
}

type OmittedTextInputProps = "isClearable" | "type" | "inputMode";

type TextAreaClassNames = { [key in InputSlots]?: string };

const classNames = (
  isDisabled: boolean,
  disableAutosize: boolean,
  labelPlacement: LabelPlacement
): TextAreaClassNames => ({
  base: clsx(cursorPointer(isDisabled), inputWidth(labelPlacement)),
  inputWrapper: clsx(
    mainWrapperClass(isDisabled),
    "py-0 px-2 rounded-[4px]",
    formFieldBg(isDisabled)
  ),
  input: clsx("!text-foreground-body bg-default", paragraphFont, {
    "resize-y": disableAutosize,
  }),
  helperWrapper: "p-0",
});

const TextArea = ({
  isRequired = false,
  isDisabled = false,
  isReadOnly = false,
  label = "",
  minRows = 3,
  maxRows = 8,
  maxLength,
  value,
  onValueChange,
  isInvalid = false,
  errorMessage,
  disableAutosize = false,
  labelPlacement = "outside",
  labelClass = "",
  labelCustomColor = false,
}: TextAreaProps) => {
  return (
    <InputLabel
      labelPlacement={labelPlacement}
      label={{
        text: label,
        customClass: labelClass,
        customColor: labelCustomColor,
      }}
    >
      <Textarea
        aria-label={label}
        isRequired={isRequired}
        isDisabled={isDisabled}
        isReadOnly={isReadOnly}
        minRows={minRows}
        maxRows={maxRows}
        maxLength={maxLength}
        value={value}
        onValueChange={onValueChange}
        isInvalid={isInvalid}
        errorMessage={<Typography text={errorMessage} variant="errorMessage" />}
        disableAutosize={disableAutosize}
        variant="bordered"
        classNames={classNames(isDisabled, disableAutosize, labelPlacement)}
      />
    </InputLabel>
  );
};

export default TextArea;
