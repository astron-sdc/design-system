import { Dispatch, ReactNode, SetStateAction , isValidElement } from "react";
import { DataTable, type DataTableRowClickEvent, type DataTableValueArray } from "primereact/datatable";
import { Column } from "primereact/column";
import type {
  ColumnBodyOptions,
  ColumnEditorOptions,
  ColumnEvent,
} from "primereact/column";
import { clsx } from "clsx";
import { bgColor, textColor } from "./utils/classes.ts";
import Typography from "./Typography.tsx";
import { NoResults } from "./EmptyState.tsx";
import { ColorType } from "./utils/colors.ts";

export type ColumnProps = {
  field: string;
  header?: string | ReactNode;
  className?: string;
  sortable?: boolean;
  style?: React.CSSProperties;
  editor?: (options: ColumnEditorOptions) => ReactNode;
  onCellEditComplete?: (event: ColumnEvent) => void;
  bodyTemplate?: (rowData: unknown, options: ColumnBodyOptions) => ReactNode;
};

type TableProps = {
  columns: ColumnProps[];
  rows: DataTableValueArray;
  headerContent?: ReactNode;
  isSelectable?: boolean;
  selected?: DataTableValueArray;
  highlightedRows?: DataTableValueArray;
  setSelected?: Dispatch<SetStateAction<DataTableValueArray>>;
  highlightSelectedRow?: boolean;
  highlightedRowColor?: ColorType
  isReorderable?: boolean;
  onReorder?: Dispatch<SetStateAction<DataTableValueArray>>;
  onRowClicked?: Dispatch<DataTableRowClickEvent>;
  onRowDoubleClicked?: Dispatch<DataTableRowClickEvent>;
  editMode?: "cell" | "row";
  emptyMessage?: ReactNode;
};

const defaultEmptyMessage = (
  <div className="my-4">
    <NoResults
      title=""
      content={<Typography text="No results found." variant="paragraph" />}
    />
  </div>
);

// todo pagination, expand rows (in design system but not needed in proposal tool yet)
const Table = ({
  columns,
  rows,
  headerContent,
  isSelectable = false,
  selected = [],
  highlightedRows = [],
  setSelected,
  highlightSelectedRow = false,
  highlightedRowColor = "secondary",
  isReorderable = false,
  onReorder,
  onRowClicked,
  onRowDoubleClicked,
  editMode,
  emptyMessage = defaultEmptyMessage,
}: TableProps) => {

  const isColumnEditable = (field: string) =>
    columns.some((col) => col.field === field && col.editor);
 
  const columnHeaderKey = (col: ColumnProps) =>  
    isValidElement(col.header) ? columns.indexOf(col).toString() : col.header?.toString();

  const columnHeaderNode = (col: ColumnProps) => 
    isValidElement(col.header) ? col.header : 
    <Typography
      text={col.header ?? ""}
      variant="h5"
      customColor={true}
      customClass={clsx(
        "min-h-[30px] pl-[5px] text-left",
        textColor("body"),
      )}
    />


  return (
    <DataTable
      editMode={editMode}
      emptyMessage={emptyMessage}
      header={headerContent}
      value={rows}
      selectionMode={isSelectable ? "multiple" : null}
      showSelectAll={true}
      selection={selected}
      onSelectionChange={(e: { value: DataTableValueArray }) =>
        setSelected?.(e.value)
      }
      reorderableRows={isReorderable}
      onRowReorder={(e) => onReorder?.(e.value)}
      className="rounded-[8px] bg-lightGrey/30 dark:bg-background-panel p-[20px] pb-[10px] design-shadow-r"
      rowClassName={(row) =>
        clsx("border-t border-grey/50 text-[13px] font-body", textColor("body"),
        highlightSelectedRow && selected.includes(row) ? bgColor(highlightedRowColor) : "",
        highlightedRows.includes(row) ? bgColor(highlightedRowColor) : "")
      }
      onRowClick={(row) => onRowClicked?.(row)}
      onRowDoubleClick={(row) => onRowDoubleClicked?.(row)}
      cellClassName={(_value, options) =>
        clsx(
          "h-12 pl-[5px]",
          isColumnEditable(options.field) &&
          "cursor-pointer hover:bg-lightGrey/30 hover:dark:bg-darkBlue rounded-[4px] p-2",
        )
      }
    >
      {isSelectable && (
        <Column selectionMode="multiple" headerClassName="pb-[10px] pl-[5px]" />
      )}
      {isReorderable && <Column rowReorder={isReorderable} />}
      {columns.map((col) => (
        <Column
          key={columnHeaderKey(col) + "_" + col.field}
          sortable={col.sortable}
          className={col.className}
          style={col.style}
          field={col.field}
          header={columnHeaderNode(col)}
          editor={col.editor}
          onCellEditComplete={col.onCellEditComplete}
          body={col.bodyTemplate}
        />
      ))}
    </DataTable>
  );
};

export default Table;
