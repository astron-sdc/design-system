import { Dispatch, ReactNode, SetStateAction } from "react";
import { DataTable, type DataTableValueArray } from "primereact/datatable";
import { Column } from "primereact/column";
import type { ColumnBodyOptions, ColumnEditorOptions, ColumnEvent } from "primereact/column";
import { clsx } from "clsx";
import { textColor } from "./utils/classes.ts";
import Typography from "./Typography.tsx";
import {NoResults} from "./EmptyState.tsx";

type ColumnProps = {
  field: string;
  header: string;
  className?: string;
  style?: React.CSSProperties;
  editor?: (options: ColumnEditorOptions) => ReactNode;
  onCellEditComplete?: (event: ColumnEvent) => void;
  bodyTemplate?: (rowData: unknown, options: ColumnBodyOptions) => ReactNode;
};

type TableProps = {
  columns: ColumnProps[];
  rows: DataTableValueArray;
  headerContent?: ReactNode;
  isSelectable?: boolean;
  selected?: DataTableValueArray;
  setSelected?: Dispatch<SetStateAction<DataTableValueArray>>;
  isReorderable?: boolean;
  onReorder?: Dispatch<SetStateAction<DataTableValueArray>>;
  editMode?: "cell" | "row";
  emptyMessage?: ReactNode;
};

const defaultEmptyMessage = (
  <div className="my-4"><NoResults title="" content={<Typography text="No results found." variant="paragraph" />} /> </div>
)

// todo pagination, expand rows (in design system but not needed in proposal tool yet)
const Table = ({
  columns,
  rows,
  headerContent,
  isSelectable = false,
  selected = [],
  setSelected,
  isReorderable = false,
  onReorder,
  editMode,
  emptyMessage = defaultEmptyMessage
}: TableProps) => {
  
  const isColumnEditable = (field: string) => columns.some((col) => col.field === field && col.editor);

  return (
    <DataTable
      editMode={editMode}
      emptyMessage={emptyMessage}
      header={headerContent}
      value={rows}
      selectionMode={isSelectable ? "multiple" : null}
      showSelectAll={true}
      selection={selected}
      onSelectionChange={(e: { value: DataTableValueArray }) =>
        setSelected && setSelected(e.value)
      }
      reorderableRows={isReorderable}
      onRowReorder={(e) => onReorder && onReorder(e.value)}
      className="rounded-[8px] bg-lightGrey/30 dark:bg-baseBlack/40 p-[20px] pb-[10px]"
      rowClassName={() =>
        clsx("border-t border-grey/50 text-[13px] font-body", textColor("body"))
      }
      cellClassName={(_value, options) => clsx("h-12 pl-[5px]", isColumnEditable(options.field) && "cursor-pointer hover:bg-lightGrey/30 hover:dark:bg-darkBlue rounded-[4px] p-2")}
    >
      {isSelectable && (
        <Column selectionMode="multiple" headerClassName="pb-[10px] pl-[5px]" />
      )}
      {isReorderable && <Column rowReorder={isReorderable} />}
      {columns.map((col, i) => (
        <Column
          key={i}
          className={col.className}
          style={col.style}
          field={col.field}
          header={
            <div className="h-[30px] pl-[5px]">
              <Typography
                text={col.header}
                variant="h5"
                customColor={true}
                customClass={textColor("body")}
              />
            </div>
          }
          editor={col.editor}
          onCellEditComplete={col.onCellEditComplete}
          body={col.bodyTemplate}
        />
      ))}
    </DataTable>
  );
};

export default Table;
