import {
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  Modal as NextModal,
} from "@nextui-org/react";
import { clsx } from "clsx";
import Button, { ButtonProps } from "./Button.tsx";
import Icon from "./Icon.tsx";
import Typography from "./Typography.tsx";
import { textColor } from "./utils/classes.ts";

type ModalProps = {
  isOpen: boolean;
  onOpenChange: () => void;
  title?: string;
  message: string;
  // If the 0th button does not have onClick prop, it will be granted the "close" action
  buttons?: ButtonProps[] | null;
};

/**
 * A simple information pop-up screen
 *
 * @param title: string
 * @param message: string
 * @param buttons: Button[] | null
 */
const Modal = ({
  isOpen,
  onOpenChange,
  title = "",
  message,
  buttons = null,
}: ModalProps) => {
  const closeButton = (
    <button>
      <Icon name="Cross" />
    </button>
  );
  return (
    <>
      <NextModal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        backdrop="opaque"
        closeButton={closeButton}
        classNames={{
          backdrop: "bg-darkBlue/90 dark:bg-baseBlack/90",
          base: "rounded-[8px] min-w-[500px] shadow-2xl p-[19px]",
          body: "p-0 pb-[10px] pr-[20px]",
          closeButton: clsx(textColor("heading"), "mt-[17px] mr-[17px] p-0"),
          footer: "flex flex-row justify-start gap-x-5 p-0 pt-[10px]",
          header: "flex flex-col gap-1 px-0 py-[10px]",
        }}
      >
        <ModalContent>
          {(onClose) => (
            <>
              {title && (
                <ModalHeader>
                  <Typography text={title} variant="h3" />
                </ModalHeader>
              )}

              <ModalBody>
                <Typography text={message} variant="paragraph" />
              </ModalBody>

              {buttons && (
                <ModalFooter>
                  {buttons.map((props, index) =>
                    index === 0 && !props.onClick ? (
                      <Button {...props} onClick={onClose} />
                    ) : (
                      <Button {...props} />
                    )
                  )}
                </ModalFooter>
              )}
            </>
          )}
        </ModalContent>
      </NextModal>
    </>
  );
};

export default Modal;
