import clsx from "clsx";
import Icon, { IconName } from "./Icon";
import Typography from "./Typography";
import { bgColor, textColor } from "./utils/classes";
import { ColorType } from "./utils/colors";

export type NotificationBarProps = {
    message: string;
    subMessage?: string;
    color: ColorType;
    iconName: IconName;
    inverted?: boolean;
};

const NotificationBar = ( {message, subMessage, color, iconName, inverted }: NotificationBarProps) => {
    return (
        <div className={clsx("flex flex-row items-center space-x-2 h-full", inverted ? bgColor(color) : "bg-lightGrey/30 dark:bg-background-panel")}>
            <div className={clsx('w-2 h-full', inverted ? "dark:bg-lightGrey bg-baseBlack/30" : bgColor(color))} />
            <Icon name={iconName} customClass={inverted ? "text-foreground-body" : textColor(color)}/>
            <Typography text={message} variant="paragraph" customColor={true} customClass={inverted ? "text-foreground-body" : textColor(color)} />
            <Typography text={subMessage} variant="paragraph" customColor={true} />
        </div>
    )
}

export default NotificationBar;