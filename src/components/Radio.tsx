import { RadioGroup, Radio as NextRadio } from "@nextui-org/react";
import { Dispatch, SetStateAction } from "react";
import { clsx } from "clsx";
import { formFieldBg, cursorPointer } from "./utils/classes.ts";

type RadioProps = {
  isRequired?: boolean;
  label?: string;
  valueOptions: { value: string; label: string; isDisabled?: boolean }[];
  value?: string;
  onValueChange?: Dispatch<SetStateAction<string | undefined>>;
};

const Radio = ({
  isRequired = false,
  label,
  valueOptions,
  value,
  onValueChange,
}: RadioProps) => {
  return (
    <RadioGroup
      isRequired={isRequired}
      label={label}
      value={value}
      onValueChange={onValueChange}
    >
      {valueOptions.map((option) => (
        <NextRadio
          isDisabled={option.isDisabled || false}
          key={option.value}
          value={option.value}
          disableAnimation
          classNames={{
            base: cursorPointer(option.isDisabled || false),
            wrapper: clsx(
              "w-[16px] h-[16px] border-[1px] !border-grey dark:border-none",
              formFieldBg(option.isDisabled || false)
            ),
            control: "w-[10px] h-[10px]",
          }}
        >
          {option.label}
        </NextRadio>
      ))}
    </RadioGroup>
  );
};

export default Radio;
