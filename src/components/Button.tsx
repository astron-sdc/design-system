import { Button as NextButton } from "@nextui-org/react";
import { clsx } from "clsx";
import { ReactNode } from "react";
import Icon, { IconName } from "./Icon.tsx";
import Typography from "./Typography.tsx";
import { ColorType } from "./utils/colors.ts";
import { bgColor, cursorPointer } from "./utils/classes.ts";
import Spinner from "./Spinner.tsx";

export type ButtonProps = {
  label?: string | ReactNode;
  color?: ColorType;
  icon?: IconName;
  iconClass?: string;
  isDisabled?: boolean;
  type?: "button" | "submit";
  showLoader?: boolean;
  loadingText?: string;
  onClick?: () => void;
};

export const buttonBaseClass: string =
  "px-[9px] h-[32px] min-w-fit max-w-fit py-[5px] text-baseBlack rounded-[4px] flex  focus:!outline-0";

/**
 * A simple button with optional icon
 *
 * @param label: string
 * @param color: Color string
 * @param icon: null | IconName
 * @param disabled: boolean
 */
const Button = ({
  label,
  color = "primary",
  icon,
  iconClass,
  isDisabled = false,
  type = "button",
  showLoader = false,
  loadingText = "Loading...",
  onClick = () => {},
}: ButtonProps) => {
  if (!label && !icon) throw Error("Button cannot be empty");

  const bgClass = (isDisabled: boolean) =>
    isDisabled ? "bg-lightGrey !opacity-75" : bgColor(color);

  const buttonLabelClass = (isDisabled: boolean) =>
    isDisabled ? "opacity-50" : "";

  const iconElement = icon && (
    <Icon name={icon} customClass={clsx(iconClass, "text-baseBlack") } />
  );
  const labelText = showLoader ? loadingText : label;

  const labelElement = labelText && (
    <Typography text={labelText} variant="paragraph" customColor={true} customClass={clsx(buttonLabelClass(isDisabled))}/>
  );

  const loaderElement = showLoader && <Spinner size="sm" />;
  
  return (
    <NextButton
      startContent={loaderElement || iconElement}
      className={clsx(
        bgClass(isDisabled),
        cursorPointer(isDisabled),
        buttonBaseClass,
      )}
      isDisabled={showLoader || isDisabled}
      type={type}
      onClick={onClick}
    >
      {labelElement}
    </NextButton>
  );
};

export default Button;
