import { Button as NextButton } from "@nextui-org/react";
import { clsx } from "clsx";
import Icon, { IconName } from "./Icon.tsx";
import Typography from "./Typography.tsx";
import { ColorType } from "./utils/colors.ts";
import { bgColor, cursorPointer } from "./utils/classes.ts";
import Spinner from "./Spinner.tsx";

export type ButtonProps = {
  label?: string | null;
  color?: ColorType;
  icon?: IconName | null;
  iconClass?: string;
  isDisabled?: boolean;
  type?: "button" | "submit";
  showLoader?: boolean;
  loadingText?: string;
  onClick?: () => void;
};

export const buttonBaseClass: string =
  "px-[9px] h-[32px] min-w-fit max-w-fit py-[5px] text-baseWhite rounded-[4px] flex focus:!outline-0";

/**
 * A simple button with optional icon
 *
 * @param label: string
 * @param color: Color string
 * @param icon: null | IconName
 * @param disabled: boolean
 */
const Button = ({
  label = null,
  color = "primary",
  icon = null,
  iconClass,
  isDisabled = false,
  type = "button",
  showLoader = false,
  loadingText = "Loading...",
  onClick = () => {},
}: ButtonProps) => {
  if (!label && !icon) throw Error("Button cannot be empty");

  const bgClass = (isDisabled: boolean) =>
    isDisabled ? "bg-lightGrey !opacity-100" : bgColor(color);

  const iconElement = icon && 
    <Icon
      name={icon}
      customClass={clsx(iconClass, "w-[16px] h-[16px]")}
    />
  ;

  const labelText = showLoader ? loadingText : label;

  const labelElement = labelText && 
    <Typography text={labelText} variant="paragraph" customColor={true} />
  ;

  const loaderElement = showLoader && <Spinner customClass="!w-4 !h-4" />;

  return (
    <NextButton
      startContent={loaderElement || iconElement}
      className={clsx(
        bgClass(isDisabled),
        cursorPointer(isDisabled),
        buttonBaseClass
      )}
      isDisabled={showLoader || isDisabled}
      type={type}
      onClick={onClick}
    >
      {labelElement}
    </NextButton>
  );
};

export default Button;
