import { clsx } from "clsx";
import { ReactNode } from "react";
import { textColor } from "./utils/classes.ts";

export type TypographyProps = {
  text: string | ReactNode;
  variant:
    | "overtitle"
    | "title"
    | "subtitle"
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "h5"
    | "paragraph"
    | "note"
    | "errorMessage";
  customColor?: boolean;
  customClass?: string;
};

export const paragraphFont = "text-[12px] leading-[22px] font-body";

/**
 * Basic styled text component
 *
 * @param text: string
 * @param variant: "overtitle" | "title" | "subtitle" | "h1" | "h2" | "h3" | "h4" | "h5" | "paragraph" | "note"
 * @param customColor: boolean
 * @param customClass: string
 */
const Typography = ({
  text,
  variant,
  customColor = false,
  customClass = "",
}: TypographyProps) => {
  // font-body is Inter, font-heading is Montserrat
  const font = {
    overtitle: "text-[16px] font-heading font-bold",
    title: "text-[70px] font-heading font-bold",
    subtitle: "text-[48px] font-heading font-normal",
    h1: "text-[48px] font-heading font-bold",
    h2: "text-[40px] font-heading font-bold",
    h3: "text-[32px] font-heading font-bold",
    h4: "text-[24px] font-heading font-bold",
    h5: "text-[16px] font-heading font-bold",
    paragraph: paragraphFont,
    note: "text-[10px] leading-[19px] uppercase font-heading font-bold",
    errorMessage: "text-[12px] font-heading font-medium leading-[19px]",
  };
  const color = {
    overtitle: textColor("heading2"),
    title: textColor("heading"),
    subtitle: textColor("heading"),
    h1: textColor("heading"),
    h2: textColor("heading"),
    h3: textColor("heading"),
    h4: textColor("heading"),
    h5: textColor("heading"),
    paragraph: textColor("body"),
    note: textColor("heading"),
    errorMessage: textColor("negative"),
  };

  const getTagForVariant = (variant: TypographyProps["variant"]) => {
    switch (variant) {
      case "paragraph":
        return "p"
      case "note":
        return "blockquote"
      case "errorMessage":
        return "p"
      case "overtitle":
        return "p";
      case "title":
        return "h1";
      case "subtitle":
        return "h2";
      default:
        return variant;
    }
  };

  const Tag = getTagForVariant(variant);

  return (
    <Tag
      className={clsx(font[variant], customClass, {
        [color[variant]]: !customColor,
      })}
    >
      {text}
    </Tag>
  );
};

export default Typography;
