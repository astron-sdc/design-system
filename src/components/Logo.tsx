import { ReactSVG } from "react-svg";
import logoDark from "src/assets/images/astron-logo-dark.svg";
import logoLight from "src/assets/images/astron-logo-light.svg";

const Logo = () => {
  return (
    <>
      <ReactSVG src={logoLight} className="block dark:hidden" />
      <ReactSVG src={logoDark} className="hidden dark:block" />
    </>
  );
};

export default Logo;
