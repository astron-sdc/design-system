import { Dispatch, SetStateAction } from "react";
import { Switch } from "@nextui-org/react";
import { clsx } from "clsx";
import { cursorPointer } from "./utils/classes.ts";
import Typography from "./Typography.tsx";

type ToggleProps = {
  isDisabled?: boolean;
  isSelected: boolean;
  setIsSelected?: Dispatch<SetStateAction<boolean>>;
  label?: string;
  labelPlacement?: "right" | "left";
  labelClass?: string;
  labelCustomColor?: boolean;
};

const Toggle = ({
  isDisabled = false,
  isSelected,
  setIsSelected,
  label,
  labelPlacement = "right",
  labelClass = "",
  labelCustomColor = false,
}: ToggleProps) => {
  const selectedClass = (isSelected: boolean) =>
    isSelected ? "group-data-[selected=true]:ml-[11px] bg-primary" : "bg-grey";
  return (
    <div className="flex flex-row">
      {labelPlacement === "left" && label && (
        <Typography
          variant="paragraph"
          customClass={clsx("mr-2 w-1/3", labelClass)}
          text={label}
          customColor={labelCustomColor}
        />
      )}
      <Switch
        isDisabled={isDisabled}
        // Does not support "isRequired": https://github.com/nextui-org/nextui/issues/1610
        isSelected={isSelected}
        onValueChange={setIsSelected}
        color="secondary"
        classNames={{
          base: cursorPointer(isDisabled),
          thumb: clsx("h-[14px] w-[14px]", selectedClass(isSelected)),
          wrapper:
            "h-[8px] w-[25px] p-0 overflow-visible bg-lightGrey dark:bg-mediumGrey",
        }}
      />
      {labelPlacement === "right" && label && (
        <Typography
          variant="paragraph"
          text={label}
          customClass={labelClass}
          customColor={labelCustomColor}
        />
      )}
    </div>
  );
};

export default Toggle;
