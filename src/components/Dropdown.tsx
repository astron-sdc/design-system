import { Autocomplete, AutocompleteItem } from "@nextui-org/react";
import { Dispatch, ReactNode, SetStateAction } from "react";
import { clsx } from "clsx";
import {
  baseInputClassNames,
  inputWidth,
} from "src/components/utils/BaseInput.tsx";
import InputLabel from "src/components/utils/InputLabel.tsx";
import { paragraphFont } from "src/components/Typography.tsx";
import Icon from "./Icon.tsx";
import { textColor } from "./utils/classes.ts";
import Badge from "./Badge.tsx";
import { ColorType } from "./utils/colors.ts";

type DropdownProps = {
  isRequired?: boolean;
  isDisabled?: boolean;
  label?: string | ReactNode;
  labelPlacement?: "outside" | "outside-left";
  labelClass?: string;
  labelCustomColor?: boolean;
  valueOptions: { value: string; label: string; badgeColor?: ColorType }[];
  value?: string;
  values?: string[];
  onValueChange?: Dispatch<SetStateAction<string | undefined>>;
  onValuesChange?: Dispatch<SetStateAction<string[]>>;
  multiSelect?: boolean;
  maxSelectCount?: number;
};

const Dropdown = ({
  isRequired = false,
  isDisabled = false,
  label,
  labelPlacement = "outside",
  labelClass = "",
  labelCustomColor = false,
  valueOptions,
  value,
  values = [],
  onValueChange,
  onValuesChange,
  multiSelect,
  maxSelectCount = Number.MAX_VALUE
}: DropdownProps) => {

  if (multiSelect && onValueChange) {
    throw Error("When using multiselect, use 'onValuesChange' instead of 'onValueChange'");
  }

  const focusClass =
    "data-[focus=true]:bg-lightGrey/50 dark:data-[focus=true]:bg-grey/50";
  const hoverCLass =
    "data-[selected=true]:!bg-primary hover:!bg-lightGrey dark:hover:!bg-grey";

  const onMultiSelect = (item: string) => {
    if (values.includes(item)) {
      onValuesChange?.(values.filter(selection => selection !== item));
    } else if (values.length < maxSelectCount) {
      onValuesChange?.([...values, item]);
    }
  }

  const onMultiDeselect = (item: string) => {
    onValuesChange?.(values.filter(selection => selection !== item));
  }
  
  return (
    <InputLabel
      labelPlacement={labelPlacement}
      label={{
        text: label,
        customClass: labelClass,
        customColor: labelCustomColor,
      }}
    >
      <div className={inputWidth(labelPlacement)}>
        <Autocomplete
          isRequired={isRequired}
          isDisabled={isDisabled}
          selectedKey={multiSelect ? "" : value}
          onSelectionChange={(key) => onValueChange?.(key as string)}
          aria-label={label as unknown as string}
          disableAnimation
          disableSelectorIconRotation
          inputProps={{
            classNames: baseInputClassNames(
              isDisabled,
              labelPlacement,
              "Dropdown",
              multiSelect
            ),
            variant: "bordered",
            isRequired: isRequired,
          }} // use the same props as BaseInput
          selectorIcon={<Icon name="Dropdown" />}
          isClearable={false} // so the "cross" button does not show
          classNames={{
            selectorButton: clsx(
              textColor("body"),
              "data-[hover=true]:bg-default opacity-100",
            ),
            popoverContent: clsx(
              "rounded-[4px] bg-background-panel",
              paragraphFont,
            ),
          }}
          startContent={ 
            multiSelect && (<div className="flex flex-row flex-wrap -ml-1 w-full">
              {
                values.map((item) => (
                  <div key={item} className="pr-1 pb-1 cursor-default">
                    <Badge text={valueOptions.find(v => v.value === item)?.label ?? item} color={valueOptions.find(v => v.value === item)?.badgeColor} onClose={() => {
                      onMultiDeselect(item);
                    }} />
                  </div>
              ))
              }
            </div>)
          }>
          {valueOptions.map((option) => {
            return (
              <AutocompleteItem
                onClick={() => {
                  if (multiSelect) {
                    onMultiSelect(option.value);  
                  }
                }}
                key={option.value}
                value={option.value}
                className={clsx(focusClass, hoverCLass)}
                endContent={
                  values.includes(option.value) && (
                    <Icon name="Checkmark"/>
                  )
                }
              >
                {option.label}
              </AutocompleteItem>
            );
          })}
        </Autocomplete>
      </div>
    </InputLabel>
  );
};

export default Dropdown;
