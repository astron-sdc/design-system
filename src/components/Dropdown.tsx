import { Autocomplete, AutocompleteItem } from "@nextui-org/react";
import { Dispatch, SetStateAction } from "react";
import { clsx } from "clsx";
import { baseInputClassNames } from "./utils/baseComponents.tsx";
import Icon from "./Icon.tsx";
import { textColor } from "./utils/classes.ts";
import Typography from "./Typography.tsx";

type DropdownProps = {
  isRequired?: boolean;
  isDisabled?: boolean;
  label?: string;
  labelPlacement?: "outside" | "outside-left";
  labelClass?: string;
  labelCustomColor?: boolean;
  valueOptions: { value: string; label: string }[];
  value?: string;
  onValueChange: Dispatch<SetStateAction<string | undefined>>;
};

const Dropdown = ({
  isRequired = false,
  isDisabled = false,
  label,
  labelPlacement = "outside",
  labelClass = "",
  labelCustomColor = false,
  valueOptions,
  value,
  onValueChange,
}: DropdownProps) => {
  const focusClass =
    "data-[focus=true]:bg-lightGrey/50 dark:data-[focus=true]:bg-grey/50";
  const hoverCLass =
    "data-[selected=true]:!bg-primary hover:!bg-lightGrey dark:hover:!bg-grey";
  return (
    <Autocomplete
      isRequired={isRequired}
      isDisabled={isDisabled}
      label={
        label && (
          <Typography
            text={label}
            variant="paragraph"
            customClass={labelClass}
            customColor={labelCustomColor}
          />
        )
      }
      selectedKey={value}
      onSelectionChange={(key) => onValueChange(key as string)}
      labelPlacement="outside"
      placeholder=" " // so the label stays outside
      disableAnimation
      disableSelectorIconRotation
      inputProps={{
        classNames: baseInputClassNames(isDisabled, labelPlacement),
        variant: "bordered",
        isRequired: isRequired,
      }} // use the same props as BaseInput
      selectorIcon={<Icon name="Dropdown" />}
      isClearable={false} // so the "cross" button does not show
      classNames={{
        selectorButton: clsx(
          textColor("body"),
          "data-[hover=true]:bg-default opacity-100"
        ),
        popoverContent: "rounded-[4px]",
      }}
    >
      {valueOptions.map((option) => {
        return (
          <AutocompleteItem
            key={option.value}
            value={option.value}
            className={clsx(focusClass, hoverCLass)}
          >
            {option.label}
          </AutocompleteItem>
        );
      })}
    </Autocomplete>
  );
};

export default Dropdown;
