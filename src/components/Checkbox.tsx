import { Checkbox as NextCheckbox } from "@nextui-org/react";
import { Dispatch, SetStateAction } from "react";
import { clsx } from "clsx";
import Icon from "./Icon.tsx";
import { formFieldBg, cursorPointer } from "./utils/classes.ts";
import Typography from "./Typography.tsx";

type CheckboxProps = {
  isDisabled?: boolean;
  isRequired?: boolean;
  isSelected: boolean;
  setIsSelected?: Dispatch<SetStateAction<boolean>>;
  label?: string;
  labelClass?: string;
  labelCustomColor?: boolean;
};

const Checkbox = ({
  isDisabled = false,
  isRequired = false,
  isSelected,
  setIsSelected,
  label,
  labelClass = "",
  labelCustomColor = false,
}: CheckboxProps) => {
  return (
    <NextCheckbox
      isRequired={isRequired}
      isDisabled={isDisabled}
      isSelected={isSelected}
      onValueChange={setIsSelected}
      icon={isSelected ? <Icon name="Checkmark" color="body" /> : <></>}
      // This seems to be the only way to enable customization of background and border color
      // that's why the color "default" is set to "transparent" in tailwind.config.js
      color="default"
      disableAnimation
      classNames={{
        base: cursorPointer(isDisabled),
        wrapper: clsx(
          "w-[24px] h-[24px] rounded-[4px]",
          "border-[1px] border-grey dark:border-none",
          formFieldBg(isDisabled)
        ),
      }}
    >
      {label && (
        <Typography
          text={label}
          variant="paragraph"
          customClass={labelClass}
          customColor={labelCustomColor}
        />
      )}
    </NextCheckbox>
  );
};

export default Checkbox;
