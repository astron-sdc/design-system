import { ReactSVG } from "react-svg";
import { clsx } from "clsx";
import * as Icons from "src/assets/icons";
import { TextColorType } from "./utils/colors.ts";
import { textColor } from "./utils/classes.ts";

export type IconName = keyof typeof Icons;
export const iconNameOptions = Object.keys(Icons) as IconName[]; // for storybook dropdown

type IconProps = {
  name: IconName;
  color?: TextColorType | null;
  customClass?: string;
};

/**
 * A SVG Icon; optionally styled by color and/or custom css
 *
 * @param name: IconName
 * @param color: TextColorType | null
 * @param customClass: string | undefined
 */
const Icon = ({ name, color = null, customClass = "" }: IconProps) => {
  const src = Icons[name];

  if (!color && !customClass) {
    return <ReactSVG src={src} style={{ color: "currentColor" }} />;
  }
  return (
    <ReactSVG
      src={src}
      className={clsx(
        { [textColor(color as TextColorType)]: !!color },
        customClass
      )}
    />
  );
};

export default Icon;
