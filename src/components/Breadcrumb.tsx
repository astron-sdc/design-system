import { clsx } from "clsx";
import { BreadcrumbItem, Breadcrumbs } from "@nextui-org/react";
import { textColor } from "src/components/utils/classes.ts";
import MenuBase from "src/components/utils/MenuBase.tsx";
import Icon from "./Icon.tsx";
import Typography from "./Typography.tsx";

export interface BreadcrumbItem {
  name: string;
  href: string;
}

type BreadcrumbProps = {
  items: BreadcrumbItem[];
};

const BreadCrumbItemChild = (item: BreadcrumbItem) => (
  <Typography text={item.name} variant="note" customColor={true} />
);


const Breadcrumb = ({ items }: BreadcrumbProps) => {
  if (items.length === 0) items = [{ name: "Application Home", href: "/" }];


  return (
    <Breadcrumbs
      maxItems={5}
      itemsBeforeCollapse={1}
      itemsAfterCollapse={2}
      separator={
        <Icon name="Next" color="body" customClass="w-[7px] h-[7px]" />
      }
      itemClasses={{
        item: clsx(
          textColor("primary"),
          "data-[current=true]:text-mediumGrey dark:data-[current=true]:text-baseWhite",
        ),
      }}
      renderEllipsis={({ ellipsisIcon, separator }) => {
        return (
          <div className="flex items-center">
            <MenuBase
              trigger={ellipsisIcon}
              menu={items.slice(1, items.length - 2).map((item, idx) => ({
                sectionKey: idx.toString(),
                sectionContent: [
                  { key: item.name, content: BreadCrumbItemChild(item) },
                ],
              }))}
              customClass="w-fit ml-8 mt-1"
            />
            {separator}
          </div>
        );
      }}
    >
      {items.map((item, idx) => (
        <BreadcrumbItem
          key={item.name}
          href={item.href}
          isCurrent={idx === items.length - 1}
        >
          {BreadCrumbItemChild(item)}
        </BreadcrumbItem>
      ))}
    </Breadcrumbs>
  );
};

export default Breadcrumb;
