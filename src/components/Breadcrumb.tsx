import { Breadcrumbs, BreadcrumbItem } from "@nextui-org/react";
import { clsx } from "clsx";
import Icon from "./Icon.tsx";
import Typography from "./Typography.tsx";
import { textColor } from "./utils/classes.ts";
import { MenuComponent, MenuItemProps } from "./Menu.tsx";

type BreadcrumbProps = {
  items: { name: string; href: string }[];
};

const Breadcrumb = ({ items }: BreadcrumbProps) => {
  if (items.length === 0) items = [{ name: "Home", href: "/" }];
  return (
    <Breadcrumbs
      maxItems={5}
      itemsBeforeCollapse={1}
      itemsAfterCollapse={2}
      separator={
        <Icon name="Next" color="body" customClass="w-[7px] h-[7px]" />
      }
      itemClasses={{
        item: clsx(
          textColor("primary"),
          "data-[current=true]:text-mediumGrey dark:data-[current=true]:text-baseWhite"
        ),
      }}
      renderEllipsis={({ items, ellipsisIcon, separator }) => {
        return (
          <div className="flex items-center">
            <MenuComponent
              menuTrigger={ellipsisIcon}
              items={items as MenuItemProps[]}
            />
            {separator}
          </div>
        );
      }}
    >
      {items.map((item, idx) => (
        <BreadcrumbItem
          key={idx}
          href={item.href}
          isCurrent={idx === items.length - 1}
        >
          <Typography text={item.name} variant="note" customColor={true} />
        </BreadcrumbItem>
      ))}
    </Breadcrumbs>
  );
};

export default Breadcrumb;
