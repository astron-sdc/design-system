import { ReactNode } from "react";
import { ReactSVG } from "react-svg";
import noEntries from "src/assets/images/no-entries.svg";
import noResults from "src/assets/images/no-results.svg";
import noAccess from "src/assets/images/no-access.svg";
import notFound from "src/assets/images/not-found.svg";
import generalError from "src/assets/images/general-error.svg";
import { textColor } from "./utils/classes.ts";
import Typography from "./Typography.tsx";

type TemplateProps = {
  src: string;
  title: string;
  content: ReactNode;
};
const Template = ({ src, title, content }: TemplateProps) => {
  return (
    <div className="flex flex-col items-center">
      <ReactSVG src={src} />
      <Typography
        text={title}
        variant="h4"
        customClass={textColor("body")}
        customColor={true}
      />
      {content}
    </div>
  );
};

type EmptyStateProps = {
  title: string;
  content: ReactNode;
};

export const NoEntries = ({ title, content }: EmptyStateProps) => (
  <Template src={noEntries} title={title} content={content} />
);

export const NoResults = ({ title, content }: EmptyStateProps) => (
  <Template src={noResults} title={title} content={content} />
);

export const NoAccess = ({ title, content }: EmptyStateProps) => (
  <Template src={noAccess} title={title} content={content} />
);

export const NotFound = ({ title, content }: EmptyStateProps) => (
  <Template src={notFound} title={title} content={content} />
);

export const GeneralError = ({ title, content }: EmptyStateProps) => (
  <Template src={generalError} title={title} content={content} />
);
