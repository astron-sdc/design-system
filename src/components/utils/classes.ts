import { ColorType, TextColorType } from "./colors.ts";

export const cursorPointer = (isDisabled: boolean) =>
  isDisabled ? "pointer-events-auto cursor-not-allowed" : "";

export const formFieldBg = (isDisabled: boolean) =>
  isDisabled ? "bg-background-input-disabled" : "bg-background-input";

export const bgColor = (color: ColorType) => {
  switch (color) {
    case "primary":
      return "bg-primary";
    case "secondary":
      return "bg-secondary";
    case "positive":
      return "bg-success";
    case "negative":
      return "bg-danger";
    case "warning":
      return "bg-warning";
    case "neutral":
      return "bg-neutral";
  }
};
export const borderColor = (color: ColorType) => {
  switch (color) {
    case "primary":
      return "border-primary";
    case "secondary":
      return "border-secondary";
    case "positive":
      return "border-success";
    case "negative":
      return "border-danger";
    case "warning":
      return "border-warning";
    case "neutral":
      return "border-neutral";
  }
};
export const textColor = (color: TextColorType) => {
  switch (color) {
    case "heading":
      return "text-foreground-heading";
    case "heading2":
      return "text-foreground-heading2";
    case "body":
      return "text-foreground-body";
    case "transparent":
      return "text-default";
    case "primary":
      return "text-primary";
    case "secondary":
      return "text-secondary";
    case "positive":
      return "text-success";
    case "negative":
      return "text-danger";
    case "warning":
      return "text-warning";
    case "neutral":
      return "text-neutral";
  }
};
