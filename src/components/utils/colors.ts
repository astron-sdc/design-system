export const baseColorOptions = [
  "primary",
  "positive",
  "negative",
  "warning",
] as const;

export type baseColorType = (typeof baseColorOptions)[number];

export const colorOptions = [
  ...baseColorOptions,
  "secondary",
  "neutral",
] as const;

export type ColorType = (typeof colorOptions)[number];

export const textColorOptions = [
  ...colorOptions,
  "heading",
  "heading2",
  "body",
  "transparent",
] as const;

export type TextColorType = (typeof textColorOptions)[number];
