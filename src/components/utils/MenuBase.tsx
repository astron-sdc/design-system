import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownSection,
  DropdownTrigger,
} from "@nextui-org/react";
import { clsx } from "clsx";
import { ReactNode } from "react";
import { OverlayPlacement } from "@nextui-org/aria-utils";
import { paragraphFont } from "src/components/Typography.tsx";
import Icon, { IconName } from "src/components/Icon.tsx";

type SectionContent = {
  key: string;
  icon?: IconName;
  content: ReactNode;
  customClass?: string;
}[];

type MenuItems = { sectionKey: string; sectionContent: SectionContent }[];

export type MenuProps = {
  label: string;
  menu: MenuItems;
  isDisabled?: boolean
};

type MenuBaseProps = {
  trigger: ReactNode;
  menu: MenuItems;
  customClass?: string;
  placement?: OverlayPlacement;
};

const MenuBase = ({
  trigger,
  menu,
  customClass,
  placement = "bottom-start",
}: MenuBaseProps) => (
  <Dropdown
    placement={placement}
    className={clsx(
      "p-0 bg-background-sidebar rounded-lg shadow-4xl min-w-0",
      customClass,
    )}
  >
    <DropdownTrigger className="aria-expanded:scale-1 aria-expanded:opacity-100">
      {trigger}
    </DropdownTrigger>

    <DropdownMenu className="pt-3 px-0">
      {menu.map((section, idx) => (
        <DropdownSection
          key={section.sectionKey}
          showDivider={idx < menu.length - 1}
        >
          {section.sectionContent.map((item) => (
            <DropdownItem key={item.key} textValue={item.key} className="py-0">
              <div
                className={clsx(
                  { "flex flex-row items-center": item.icon },
                  paragraphFont,
                  item.customClass,
                )}
              >
                {item.icon && <Icon name={item.icon} customClass="mr-[9px]" />}
                {item.content}
              </div>
            </DropdownItem>
          ))}
        </DropdownSection>
      ))}
    </DropdownMenu>
  </Dropdown>
);

export default MenuBase;
