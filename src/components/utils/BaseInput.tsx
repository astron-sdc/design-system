import { Input } from "@nextui-org/react";
import { Dispatch, ReactNode, SetStateAction } from "react";
import { InputSlots } from "@nextui-org/theme";
import { clsx } from "clsx";
import Typography, { paragraphFont } from "src/components/Typography.tsx";
import InputLabel from "src/components/utils/InputLabel";
import { formFieldBg, cursorPointer } from "./classes.ts";

export type LabelPlacement = "outside" | "outside-left";

type BaseInputProps = {
  // basic info
  name?: string; // allow for FormData to work
  label: string | ReactNode;
  labelPlacement: LabelPlacement;
  labelClass: string;
  labelCustomColor: boolean;
  endContent: ReactNode; // such as units or icon
  // value and validation
  placeholder: string;
  isClearable?: boolean;
  value?: string;
  onValueChange?: Dispatch<SetStateAction<string>>;
  isInvalid: boolean;
  errorMessage: string;
  maxLength?: number;
  type?: "text" | "search" | "url" | "tel" | "email" | "number" | "password";
  // interaction
  isRequired?: boolean;
  isDisabled?: boolean;
  isReadOnly?: boolean;
  inputMode?:
    | "none"
    | "text"
    | "tel"
    | "url"
    | "email"
    | "numeric"
    | "decimal"
    | "search";
};

type baseInputClassNamesObj = { [key in InputSlots]: string };

export const mainWrapperClass = (isDisabled: boolean) =>
  // this is the border color (use text-*)
  isDisabled
    ? "text-mediumGrey dark:text-background-input-disabled"
    : "text-grey dark:text-mediumGrey hover:text-baseBlack focus-within:!text-primary";

export const inputWidth = (
  labelPlacement: LabelPlacement,
  parentComponentName?: string,
): string =>
  labelPlacement === "outside-left" && parentComponentName !== "Dropdown"
    ? "w-2/3"
    : "w-full";

export const baseInputClassNames = (
  isDisabled: boolean,
  labelPlacement: LabelPlacement,
  parentComponentName?: string,
  isHidden?: boolean
): baseInputClassNamesObj => {
  const clearButtonClass = (isDisabled: boolean) => {
    if (isDisabled) return "";

    // we cannot do "[&>svg]:" + className because Tailwind does not support dynamic class names
    const clearButtonSvgClass: string =
      "[&>svg]:w-[30px] [&>svg]:h-[30px] [&>svg]:mt-[-4px] [&>svg]:ml-[-4px]";
    const clearButtonBaseClass: string =
      "bg-foreground-body text-background-input w-[22px] h-[22px] p-0 !opacity-100";
    return clsx(clearButtonSvgClass, clearButtonBaseClass);
  };

  return {
    base: clsx(
      cursorPointer(isDisabled),
      inputWidth(labelPlacement, parentComponentName),
    ),
    input: clsx("!text-foreground-body bg-default", paragraphFont, isHidden ? "hidden" : ""),
    inputWrapper: clsx(
      "px-2 rounded-[4px]",
      isHidden ? "h-auto" : "min-h-[33px] max-h-[33px]",
      formFieldBg(isDisabled),
      isHidden ? "!cursor-default" : ""
    ),
    mainWrapper: clsx("w-full", mainWrapperClass(isDisabled)),
    clearButton: clearButtonClass(isDisabled),
    helperWrapper: "p-0",
  } as baseInputClassNamesObj;
};

const BaseInput = ({
  name,
  label,
  labelPlacement = "outside",
  labelClass,
  labelCustomColor,
  endContent,
  placeholder,
  isClearable = false,
  value,
  onValueChange,
  isInvalid,
  errorMessage,
  maxLength,
  type = "text",
  isRequired = false,
  isDisabled = false,
  isReadOnly = false,
  inputMode = "text",
}: BaseInputProps) => {
  return (
    <InputLabel
      labelPlacement={labelPlacement}
      label={{
        text: label,
        customClass: labelClass,
        customColor: labelCustomColor,
      }}
    >
      <Input
        name={name}
        endContent={endContent}
        placeholder={placeholder}
        value={value}
        isClearable={isClearable}
        onValueChange={onValueChange}
        isInvalid={isInvalid}
        errorMessage={<Typography text={errorMessage} variant="errorMessage" />}
        maxLength={maxLength}
        type={type}
        isRequired={isRequired}
        isDisabled={isDisabled}
        isReadOnly={isReadOnly}
        inputMode={inputMode}
        // appearance
        variant="bordered"
        classNames={baseInputClassNames(isDisabled, labelPlacement)}
        disableAnimation
      />
    </InputLabel>
  );
};

export default BaseInput;
