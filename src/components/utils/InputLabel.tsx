import { clsx } from "clsx";
import { ReactNode } from "react";
import Typography, { TypographyProps } from "src/components/Typography.tsx";
import { LabelPlacement } from "src/components/utils/BaseInput.tsx";

type InputLabelProps = {
  labelPlacement?: LabelPlacement;
  label?: Omit<TypographyProps, "variant">;
  children: ReactNode;
};

const InputLabel = ({ labelPlacement, label, children }: InputLabelProps) => (
  <label
    className={clsx(
      "flex justify-between items-baseline",
      labelPlacement === "outside-left" ? "flex-row" : "flex-col",
    )}
  >
    {label && (
      <Typography
        text={label.text}
        variant="paragraph"
        customClass={clsx(
          { "w-1/4": labelPlacement === "outside-left" },
          "font-semibold leading-normal mb-1",
          label.customClass,
        )}
        customColor={label.customColor}
      />
    )}
    {children}
  </label>
);

export default InputLabel;
