import { Progress } from "@nextui-org/react";

type ProgressBarProps = {
  value?: number;
};
const ProgressBar = ({ value }: ProgressBarProps) => {
  return (
    <Progress
      isIndeterminate={!value}
      value={value as number}
      aria-label="progress"
      color="primary"
      disableAnimation
      classNames={{
        track: "bg-lightGrey dark:bg-mediumGrey",
      }}
    />
  );
};

export default ProgressBar;
