import { Sidebar as PRSidebar } from "primereact/sidebar";
import { Fragment, ReactNode, useState } from "react";
import { NavLink } from "react-router-dom";
import { Divider } from "@nextui-org/react";
import Tooltip from "src/components/Tooltip";
import Icon, { IconName } from "./Icon";
import Logo from "./Logo";
import Typography from "./Typography";

export type SidebarItem = {
  icon: IconName;
  text: string;
  linkTo: string;
  badge?: ReactNode;
  openInNewWindow?: boolean;
  showDivider?: boolean;
};

export type SidebarTop = {
  text: string;
  textLong?: string;
  icon: IconName;
  linkTo?: string;
}

interface SidebarProps {
  items: SidebarItem[];
  top?: SidebarTop;
}

interface SidebarContentProps extends SidebarProps {
  onHide: () => void;
}

const baseIconLinkStyle =
  "size-16 grid place-items-center hover:bg-secondary hover:text-baseWhite";
const activeIconLinkStyle = `${baseIconLinkStyle} text-primary shadow-left-bar-4`;

const baseLinkStyle = "block hover:bg-secondary hover:text-baseWhite";
const activeLinkStyle = `${baseLinkStyle} text-primary shadow-left-bar-4`;

const SideBarContent = ({ items, top, onHide }: SidebarContentProps) => {
  return (
    <div className="flex flex-col h-full">
      <div className="p-2">
        <Logo />
      </div>

      {top && (
        <Typography
          variant="note"
          text={top.textLong ?? top.text}
          customClass="px-2 pb-2"
        />
      )}
      {items.map((item) => (
        <Fragment key={item.linkTo}>
          <NavLink
            to={item.linkTo}
            key={item.linkTo}
            className={({ isActive }) =>
              isActive ? activeLinkStyle : baseLinkStyle
            }
          >
            <div className="flex">
              <div className="size-10 grid place-items-center">
                <Icon name={item.icon} />
              </div>
              <div className="h-10 grid place-items-center">
                <Typography text={item.text} variant="h5" customColor={true} />
              </div>
            </div>
          </NavLink>
          {item.showDivider && <Divider className="my-2" orientation="horizontal"/>}
        </Fragment>
      ))}

      <div className="mt-auto flex justify-end">
        <button
          className="block size-16 grid place-items-center hover:bg-secondary hover:text-baseWhite"
          onClick={onHide}
        >
          <Icon name="PreviousEnd" />
        </button>
      </div>
    </div>
  );
};

const Sidebar = ({ items, top }: SidebarProps) => {
  const [visible, setVisible] = useState(false);

  return (
    <>
      <div className="flex flex-col bg-background-sidebar text-foreground-body h-full w-16 design-shadow-r">
        {top && (
          <NavLink to={top.linkTo ?? "/"} key={top.linkTo ?? "/"}>
            <div className="size-16 place-items-center grid mt-2">
                <Icon name={top.icon} />
                <Typography variant="note" text={top.text} />
            </div>
          </NavLink>
        )}

        {/* Items in the list */}
        {items.map((item) => (
          <Fragment key={item.text}>
            <Tooltip text={item.text} key={item.text} placement="right">
              <NavLink
                to={item.linkTo}
                key={item.linkTo}
                className={({ isActive }) =>
                  isActive ? activeIconLinkStyle : baseIconLinkStyle
                }
                {...(item.openInNewWindow && {
                  target: "_blank",
                  rel: "noopener noreferrer",
                })}
              >
                {item.badge && <div className="float-right">{item.badge}</div>}
                <Icon name={item.icon} />
              </NavLink>
            </Tooltip>
            {item.showDivider && <Divider className="my-2" orientation="horizontal"/>}
          </Fragment>
        ))}

        {/* Bottom button */}
        <Tooltip text="Expand sidebar" placement="right">
          <button
            className="size-16 mt-auto grid place-items-center hover:bg-secondary hover:text-baseWhite"
            onClick={() => {
              setVisible(true);
            }}
          >
            <Icon name="NextEnd" />
          </button>
        </Tooltip>
      </div>

      <PRSidebar
        className="bg-background text-foreground-body"
        visible={visible}
        onHide={() => setVisible(false)}
        content={
          <SideBarContent
            items={items}
            top={top}
            onHide={() => setVisible(false)}
          />
        }
      ></PRSidebar>
    </>
  );
};

export default Sidebar;
