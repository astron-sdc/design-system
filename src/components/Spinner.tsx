import { Spinner as NextSpinner } from "@nextui-org/react";
import Typography from "src/components/Typography.tsx";

export type SpinnerProps = {
  label?: string;
  size?: "sm" | "md" | "lg";
};

const Spinner = ({ label, size = "md" }: SpinnerProps) => {
  return (
    <label className="flex flex-row gap-2 items-center">
      <NextSpinner
        role="progress"
        aria-label={label ?? "Loading"}
        size={size}
        classNames={{
          circle1: "animate-spin",
          circle2: "invisible",
          wrapper: "text-grey",
        }}
      />
      {label && <Typography text={label} variant="paragraph" />}
    </label>
  );
};

export default Spinner;
