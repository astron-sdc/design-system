import { Slider as NextSlider } from "@nextui-org/react";

type SliderProps = {
  min: number;
  max: number;
  defaultValue: number | number[];
  value?: number | number[] | null;
};

const Slider = ({ min, max, defaultValue }: SliderProps) => {
  return (
    <NextSlider
      minValue={min}
      maxValue={max}
      defaultValue={defaultValue}
      classNames={{
        trackWrapper: "max-w-md",
        track: "bg-lightGrey dark:bg-mediumGrey border-none h-[8px]",
        filler: "bg-foreground-heading rounded-full",
        thumb:
          "outline-none w-[14px] h-[14px] after:w-[14px] after:h-[14px] after:bg-foreground-heading2 after:shadow-none",
      }}
      disableThumbScale
    />
  );
};

export default Slider;
