import { Chip } from "@nextui-org/react";
import { clsx } from "clsx";
import Typography from "./Typography.tsx";
import { ColorType } from "./utils/colors.ts";
import { bgColor, borderColor, textColor } from "./utils/classes.ts";

type BadgeProps = {
  text: string;
  color?: ColorType;
  inverted?: boolean;
};

/**
 * A small status badge to signify a status or notification count e.g.
 *
 * @param text: string
 * @param color: Color
 * @param inverted: boolean
 */
const Badge = ({ text, color = "primary", inverted = false }: BadgeProps) => {
  const bgClass = (inverted: boolean) =>
    inverted ? "bg-transparent" : bgColor(color);
  const textClass = (inverted: boolean) =>
    inverted ? textColor(color) : "text-baseWhite";
  return (
    <Chip
      variant={inverted ? "bordered" : "solid"}
      classNames={{
        base: clsx(
          borderColor(color),
          bgClass(inverted),
          "rounded-[4px] h-[20px] px-[10px]"
        ),
        content: clsx(textClass(inverted), "px-0"),
      }}
    >
      <Typography
        text={text}
        variant="note"
        customColor={true}
        customClass="leading-[24px]"
      />
    </Chip>
  );
};

export default Badge;
