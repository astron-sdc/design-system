import { Chip } from "@nextui-org/react";
import { clsx } from "clsx";
import Typography from "./Typography.tsx";
import { ColorType } from "./utils/colors.ts";
import { bgColor, borderColor, textColor } from "./utils/classes.ts";
import Icon from "./Icon.tsx";


type BadgeProps = {
  text: string;
  color?: ColorType;
  inverted?: boolean;
  onClose?: () => void;
};

/**
 * A small status badge to signify a status or notification count e.g.
 *
 * @param text: string
 * @param color: Color
 * @param inverted: boolean
 * @param onClose: () => void;
 */
const Badge = ({ text, color = "primary", inverted = false, onClose }: BadgeProps) => {
  const bgClass = (inverted: boolean) =>
    inverted ? "bg-transparent" : bgColor(color);
  const textClass = (inverted: boolean) =>
    inverted ? textColor(color) : "text-baseBlack";
  return (
    <Chip
      onClick={(e) => {
        e.preventDefault();
      }}
      variant={inverted ? "bordered" : "solid"}
      classNames={{
        base: clsx(
          borderColor(color),
          bgClass(inverted),
          "rounded-[4px] h-[20px]",
          onClose ? "pl-[10px] pr-[4px]" : "px-[10px]"
        ),
        content: clsx(textClass(inverted), "px-0")
      }}
      endContent={onClose && (
        <div className={clsx(textClass(inverted), bgClass(inverted), "ml-2 scale-75 cursor-pointer")} onKeyDown={() => {}} onClick={(e) => {
          e.preventDefault();
          onClose();
        }}>
          <Icon name="Cross"/>
        </div>)
      }>
      <Typography
        text={text}
        variant="note"
        customColor={true}
        customClass="leading-[24px]"
      />
    </Chip>
  );
};

export default Badge;
