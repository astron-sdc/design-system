import { clsx } from "clsx";
import { useState } from "react";
import Icon, { IconName } from "./Icon.tsx";
import Typography from "./Typography.tsx";
import { baseColorType } from "./utils/colors.ts";
import { borderColor, textColor } from "./utils/classes.ts";

export type AlertProps = {
  title: string;
  message: string;
  alertType?: baseColorType;
  expireAfter?: number;
  onExpire?: () => void;
  shouldRemoveOnExpire?: boolean;
};

/**
 * An interruptive pop-up showing important information
 * Use sparingly
 *
 * @param title: string
 * @param message: string
 * @param alertType: "default" | "warning" | "positive" | "negative"
 * @param expireAfter: number
 * @param onExpire: () => void
 * @param shouldRemoveOnExpire: () => boolean
 */
const Alert = ({ title, message, alertType = "primary", expireAfter, onExpire, shouldRemoveOnExpire = false }: AlertProps) => {
  const [hasExpired, setHasExpired] = useState(false);
  const [shouldRemoveElement, setShouldRemoveElement] = useState(false);

  if (shouldRemoveElement) {
    return null;
  }

  const isToasty = !!expireAfter;

  if (expireAfter) {
    setTimeout(() => {
      setHasExpired(true);
      
      if (shouldRemoveOnExpire) {
        setTimeout(() => {
          setShouldRemoveElement(true);
          onExpire && onExpire();
        }, 1000);
      } else {
        onExpire && onExpire();
      }

    }, expireAfter);
  }

  const icon = {
    primary: "InfoCircle",
    warning: "AttentionCircle",
    positive: "CheckmarkCircle",
    negative: "CrossCircle",
  };
  
  const baseClass =
    clsx(
      {"transition-opacity ease-out duration-700 opacity-0": hasExpired },
      "flex flex-row items-start py-[13px] px-[15px] rounded-[8px] w-min border-l-8 border-y-0 border-r-0 bg-baseWhite dark:bg-mediumGrey shadow-2xl",
      {"animate-fadeIn bg-baseWhite/80 dark:bg-mediumGrey/80 fixed top-5 right-5": isToasty});

  const iconElement = isToasty ? null : <Icon name="Cross" color="body" customClass="cursor-pointer" />
  
  return (
    <div className={clsx(baseClass, borderColor(alertType))}>
      <Icon name={icon[alertType] as IconName} color={alertType} />
      <div className="flex flex-col w-[400px] ml-[13px]">
        <Typography
          text={title}
          variant="paragraph"
          customClass={textColor(alertType)}
          customColor={true}
        />
        <Typography text={message} variant="paragraph" />
      </div>
      {/* if used more often than below and in Modal, consider making it a CloseIcon */}
      {iconElement}
    </div>
  );
};

export default Alert;
