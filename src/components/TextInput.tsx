import { Dispatch, ReactNode, SetStateAction } from "react";
import { BaseInput, LabelPlacement } from "./utils/baseComponents.tsx";

type TextInputProps = {
  isRequired?: boolean;
  label?: string;
  labelPlacement?: LabelPlacement;
  labelClass?: string;
  labelCustomColor?: boolean;
  endContent?: ReactNode; // such as units (as text element e.g. <span>)
  isClearable?: boolean;
  value: string;
  onValueChange?: Dispatch<SetStateAction<string>>;
  isInvalid?: boolean;
  errorMessage?: string;
  isDisabled?: boolean;
  isReadOnly?: boolean;
  maxLength?: number;
  type?:
    | "text"
    | "search"
    | "url"
    | "tel"
    | "email"
    | "password"
    | "number"
    | (string & NonNullable<unknown>);
  inputMode?:
    | "none"
    | "text"
    | "tel"
    | "url"
    | "email"
    | "numeric"
    | "decimal"
    | "search";
};

const TextInput = ({
  isRequired = false,
  label = "",
  labelPlacement = "outside",
  labelClass = "",
  labelCustomColor = false,
  endContent,
  isClearable = false,
  value,
  onValueChange,
  isInvalid = false,
  errorMessage = "",
  isDisabled = false,
  isReadOnly = false,
  maxLength,
  type = "text",
  inputMode = "text",
}: TextInputProps) => {
  return (
    <BaseInput
      isRequired={isRequired}
      label={label}
      labelPlacement={labelPlacement}
      labelClass={labelClass}
      labelCustomColor={labelCustomColor}
      endContent={endContent}
      placeholder=" "
      isClearable={isClearable}
      value={value}
      onValueChange={onValueChange}
      isInvalid={isInvalid}
      errorMessage={errorMessage}
      isDisabled={isDisabled}
      isReadOnly={isReadOnly}
      maxLength={maxLength}
      type={type}
      inputMode={inputMode}
    />
  );
};

export default TextInput;
export type { TextInputProps };
