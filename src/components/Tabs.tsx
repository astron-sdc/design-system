import { Tabs as NextTabs, Tab } from "@nextui-org/react";
import { ReactNode } from "react";
import Typography from "./Typography";

type TabInfo = {
  key: string;
  title?: string;
  badge?: ReactNode;
  content?: ReactNode;
};

type TabsProps = {
  tabs: TabInfo[];
};

/**
 * Tabbed panel
 */
const Tabs = ({ tabs }: TabsProps) => {
  return (
    <NextTabs color="primary" variant="underlined">
      {tabs.map((t) => (
        <Tab
          key={t.key}
          title={
            <div className="flex flex-row space-x-2">
              <Typography
                variant="overtitle"
                customColor={true} // make non active text follow theme
                text={t.title || t.key}
              />
              {t.badge}
            </div>
          }
        >
          {t.content}
        </Tab>
      ))}
    </NextTabs>
  );
};

export default Tabs;
