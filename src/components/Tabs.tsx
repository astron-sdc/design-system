import { Tabs as NextTabs, Tab } from "@nextui-org/react";
import { Key, ReactNode, useState } from "react";
import Typography from "./Typography";
type TabInfo = {
  key: string;
  title?: string;
  badge?: ReactNode;
  content?: ReactNode;
};

type TabsProps = {
  tabs: TabInfo[];
  selectedTabKey?: string;
};

/**
 * Tabbed panel
 */
const Tabs = ({ tabs, selectedTabKey }: TabsProps) => {
  const [selected, setSelected] = useState<string>(
    selectedTabKey ?? tabs[0].key,
  );
  return (
    <NextTabs
      color="primary"
      variant="underlined"
      selectedKey={selected } 
      onSelectionChange={(e: Key) => 
        {
        setSelected(e as string)}
      }
    >
      {tabs.map((t) => (
        <Tab
          key={t.key}
          title={
            <div className="flex flex-row space-x-2">
              <Typography
                variant="overtitle"
                customColor={true} // make non active text follow theme
                text={t.title ?? t.key}
              />
              {t.badge}
            </div>
          }
        >
          {t.content}
        </Tab>
      ))}
    </NextTabs>
  );
};

export default Tabs;
