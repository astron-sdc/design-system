import { ReactNode } from "react";

type LayoutProps = {
  top: ReactNode;
  side: ReactNode;
  content: ReactNode;
};

const ApplicationLayout = ({ content, side, top }: LayoutProps) => {
  return (
    <div className="flex min-h-dvh bg-background text-foreground-body">
      <div>{side}</div>
      <div className="flex flex-auto flex-col">
        <div>{top}</div>
        <div
          className="flex-auto overflow-auto"
          style={{ height: "calc(100vh - 120px)" }}
        >
          {content}
        </div>
      </div>
    </div>
  );
};

export default ApplicationLayout;
