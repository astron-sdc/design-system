import { Meta } from "@storybook/react";
import NotificationBar from "src/components/NotficationBar";

/**
 * Width of the input is 100% of parent component unless [`labelPlacement === "outside-left"`](#label-on-the-left)
 */
const meta = {
    title: "Components/NotificationBar",
    component: NotificationBar,
  } satisfies Meta;
  
  export default meta;

  export const NotificationBarWarning = () => {
    return <div className="h-12"> 
    <NotificationBar message="You are now accessing the tool as administrator." subMessage="Optional extra content." color="warning" iconName="AttentionCircle" />
    </div> 
  };

  export const NotificationBarSuccess= () => {
    return <div className="h-16"> 
    <NotificationBar message="You have succesfully bought store credits." inverted={true} color="positive" iconName="CheckmarkCircle" />
    </div> 
  };