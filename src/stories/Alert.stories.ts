import { Meta, StoryObj } from "@storybook/react";
import Alert from "src/components/Alert.tsx";
import { baseColorOptions } from "src/components/utils/colors.ts";

/** Use alerts/notifications to inform users of updates or changes to system status. Communicating with users and providing immediate feedback are important for building trust. While notifications are an effective method of communicating with users, they are disruptive and should be used sparingly. */
const meta = {
  title: "Components/Alert",
  component: Alert,
  argTypes: {
    alertType: { control: "radio", options: baseColorOptions },
    onClick: () => {},
  },
} satisfies Meta;

export default meta;

export const PlainAlert: StoryObj = {
  args: {
    title: "Hello world!",
    message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
};

export const Toast: StoryObj = {
  args: {
    title: "I will fade away in 5 seconds",
    message: "Bye bye",
    expireAfter: 5000,
    onExpire: () => console.log("Alert expired, do some state changes now"),
  },
};
