import { Meta } from "@storybook/react";
import { useDisclosure } from "@nextui-org/react";
import { useState } from "react";
import Dropdown from "src/components/Dropdown.tsx";
import Modal from "src/components/Modal.tsx";
import Button from "src/components/Button.tsx";
import Typography from "src/components/Typography";

const meta = {
  title: "Components/Modal",
  component: Modal,
} satisfies Meta;

export default meta;

export const ModalWithTitleAndButtons = () => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure({ defaultOpen: true });

  return (
    <>
      <Button onClick={onOpen} label="Open modal again" />
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        title="Title"
        message="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt maximus turpis, 
        id aliquam tellus iaculis eget. Nam sapien purus, vehicula a risus non, molestie fermentum turpis."
        buttons={[
          { label: "Click me!" },
          { label: "No, me!", color: "secondary" },
        ]}
      />
    </>
  );
};

export const ModalWithTitle = () => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure({ defaultOpen: true });

  return (
    <>
      <Button onClick={onOpen} label="Open modal again" />
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        title="Title"
      >
        <Typography text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt maximus turpis, 
        id aliquam tellus iaculis eget. Nam sapien purus, vehicula a risus non, molestie fermentum turpis." variant="paragraph" />
      </Modal>
    </>
  );
};

export const ModalWithButtons = () => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure({ defaultOpen: true });

  return (
    <>
      <Button onClick={onOpen} label="Open modal again" />
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        message="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt maximus turpis, id aliquam tellus iaculis eget. Nam sapien purus, vehicula a risus non, molestie fermentum turpis."
        buttons={[
          { label: "Click me!" },
          { label: "No, me!", color: "secondary" },
        ]}
      >
        <Typography text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt maximus turpis, 
        id aliquam tellus iaculis eget. Nam sapien purus, vehicula a risus non, molestie fermentum turpis." variant="paragraph" />
      </Modal>
    </>
  );
};


export const ModalWithChildContent = () => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure({ defaultOpen: true });
  const [dropdownValue, setDropdownValue] = useState<string>();
  const dropdownValueOptions = [{
    value: "vanilla",
    label: "Vanilla"
  }, {
    value: "mocha",
    label: "Mocha"
  }, {
    value: "mint",
    label: "Mint"
  }, {
    value: "raspberry",
    label: "Raspberry"
  }, {
    value: "apple",
    label: "Apple"
  }];
  return (
    <>
      <Button onClick={onOpen} label="Open modal again" />
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        title="Modal with child content"
        message="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt maximus turpis, id aliquam tellus iaculis eget. Nam sapien purus, vehicula a risus non, molestie fermentum turpis."
        buttons={[
          { label: "Click me!" },
          { label: "No, me!", color: "secondary" },
        ]}
      >
        <Typography text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt maximus turpis, 
        id aliquam tellus iaculis eget. Nam sapien purus, vehicula a risus non, molestie fermentum turpis." variant="paragraph" />
        <Dropdown label="What's your favourite ice cream?" valueOptions={dropdownValueOptions} onValueChange={setDropdownValue} />
        <Typography text={dropdownValue} variant="paragraph"/>
      </Modal>
    </>
  );
};