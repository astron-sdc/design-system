import { Meta } from "@storybook/react";
import { useDisclosure } from "@nextui-org/react";
import Modal from "src/components/Modal.tsx";
import Button from "src/components/Button.tsx";

const meta = {
  title: "Components/Modal",
  component: Modal,
} satisfies Meta;

export default meta;

export const ModalWithTitleAndButtons = () => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure({ defaultOpen: true });

  return (
    <>
      <Button onClick={onOpen} label="Open modal again" />
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        title="Title"
        message="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt maximus turpis, id aliquam tellus iaculis eget. Nam sapien purus, vehicula a risus non, molestie fermentum turpis."
        buttons={[
          { label: "Click me!" },
          { label: "No, me!", color: "secondary" },
        ]}
      />
    </>
  );
};

export const ModalWithTitle = () => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure({ defaultOpen: true });

  return (
    <>
      <Button onClick={onOpen} label="Open modal again" />
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        title="Title"
        message="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt maximus turpis, id aliquam tellus iaculis eget. Nam sapien purus, vehicula a risus non, molestie fermentum turpis."
      />
    </>
  );
};

export const ModalWithButtons = () => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure({ defaultOpen: true });

  return (
    <>
      <Button onClick={onOpen} label="Open modal again" />
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        message="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt maximus turpis, id aliquam tellus iaculis eget. Nam sapien purus, vehicula a risus non, molestie fermentum turpis."
        buttons={[
          { label: "Click me!" },
          { label: "No, me!", color: "secondary" },
        ]}
      />
    </>
  );
};
