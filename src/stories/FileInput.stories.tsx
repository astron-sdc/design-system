import { Meta } from "@storybook/react";
import { useState } from "react";
import FileInput from "src/components/FileInput";
import Typography from "src/components/Typography.tsx";

const meta = {
  title: "Components/File Input",
  component: FileInput,
} satisfies Meta;

export default meta;

export const SimpleFileInput = () => {
  const [files, setFiles] = useState<File[]>([]);

  return (
    <FileInput label="My File Input" files={files} onFilesChange={setFiles} />
  );
};

export const UseSelectedFile = () => {
  const [files, setFiles] = useState<File[]>([]);

  return (
    <div>
      <FileInput label="Managed file" files={files} onFilesChange={setFiles} />
      <Typography
        variant="paragraph"
        text={
          <>
            Selected file:
            <br />
            {files.length === 0 ? "No file selected." : files[0].name}
          </>
        }
      />
    </div>
  );
};

export const MultipleFilesInput = () => {
  const [files, setFiles] = useState<File[]>([]);

  return (
    <div>
      <FileInput
        label="Show me them files!"
        files={files}
        onFilesChange={setFiles}
        multiple
      />
      <Typography
        variant="paragraph"
        text={
          <>
            <span>Selected files:</span>
            <ul>
              {files.length === 0 && <li>No files selected.</li>}
              {files.map((f) => (
                <li key={f.name}>{f.name}</li>
              ))}
            </ul>
          </>
        }
      />
    </div>
  );
};

export const AcceptOnlyTxtFiles = () => {
  const [files, setFiles] = useState<File[]>([]);

  return (
    <div>
      <FileInput
        label="Text Only!"
        files={files}
        onFilesChange={setFiles}
        accept="text/plain"
      />
      <Typography
        variant="paragraph"
        text={
          <>
            Selected file:
            <br />
            {files.length === 0 ? "No file selected." : files[0].name}
          </>
        }
      />
    </div>
  );
};
