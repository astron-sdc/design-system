import { Meta } from "@storybook/react";
import { MemoryRouter } from "react-router-dom";
import { IconName } from "src/components/Icon";
import Sidebar, { SidebarItem, SidebarTop } from "src/components/Sidebar";

const meta = {
  title: "Components/Sidebar",
  component: Sidebar,
} satisfies Meta;

export default meta;

export const TheSidebar = () => {
  const items: SidebarItem[] = [
    { icon: "File", text: "Proposals", linkTo: "proposals" },
    {
      icon: "Review",
      text: "Review",
      linkTo: "reviews",
    },
    {
      icon: "Eye",
      text: "Helpdesk",
      linkTo: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
      openInNewWindow: true,
      showDivider: true,
    },
    {
      icon: "TMSS",
      text: "Telescope Manager Specification System",
      linkTo: "https://tmss.lofar.eu",
      openInNewWindow: true,
    },
    { icon: "QuestionMark", text: "Documentation", linkTo: "docs" },
  ];

  const top: SidebarTop = {
    icon: "Apt" as IconName,
    text: "APT",
    textLong: "Astron Proposal Tool",
    linkTo: "https://astron.nl"
  };

  // Assumes the use of React Router; since it uses NavLink components
  return (
    <MemoryRouter>
      {/* Height should be set */}
      <div className="h-96">
        <Sidebar items={items} top={top} />
      </div>
    </MemoryRouter>
  );
};
