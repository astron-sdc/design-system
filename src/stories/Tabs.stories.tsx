import { Meta } from "@storybook/react";
import Badge from "src/components/Badge";
import Tabs from "src/components/Tabs";

const meta = {
  title: "Components/Tabs",
  component: Tabs,
} satisfies Meta;

export default meta;

export const Default = () => {
  const tabs = [{ key: "Home" }, { key: "Info" }, { key: "Settings" }];
  return <Tabs tabs={tabs} />;
};

export const WithTitles = () => {
  const tabs = [
    { key: "home", title: "Home" },
    { key: "info", title: "Info" },
    { key: "settings", title: "Settings" },
  ];
  return <Tabs tabs={tabs} />;
};

export const WithBadges = () => {
  const tabs = [
    { key: "Home" },
    { key: "Messages", badge: <Badge text="2" /> },
    { key: "Errors", badge: <Badge color="negative" text="42" /> },
  ];
  return <Tabs tabs={tabs} />;
};

const HomeContent = () => {
  return (
    <div>
      <span>Home Content</span>
    </div>
  );
};

const InfoContent = () => {
  return (
    <div>
      <span>Some Info</span>
    </div>
  );
};

const SettingsContent = () => {
  return (
    <div>
      <span>All the settings</span>
    </div>
  );
};

export const WithContent = () => {
  const tabs = [
    { key: "Home", content: <HomeContent /> },
    { key: "Info", content: <InfoContent /> },
    { key: "Settings", content: <SettingsContent /> },
  ];
  return <Tabs tabs={tabs} />;
};
