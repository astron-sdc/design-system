import { Meta } from "@storybook/react";
import { clsx } from "clsx";
import {
  GeneralError,
  NoAccess,
  NoEntries,
  NoResults,
  NotFound,
} from "src/components/EmptyState.tsx";
import Typography from "src/components/Typography.tsx";
import Button from "src/components/Button.tsx";
import { textColor } from "src/components/utils/classes.ts";

const meta = {
  title: "Components/Empty states",
} satisfies Meta;

export default meta;

export const NoEntriesFound = () => {
  const text = (
    <p>
      Click <strong>upload files</strong> to upload your data and create a new
      entry.
    </p>
  );
  const content = (
    <>
      <Typography text={text} variant="paragraph" customClass="py-3" />
      <Button label="Upload files" icon="Plus" />
    </>
  );
  return <NoEntries title="Welcome Houston P," content={content} />;
};

export const NoResultsFound = () => {
  const text = "Adjust search terms or filters to continue your search";
  const content = (
    <>
      <Typography text={text} variant="paragraph" customClass="py-3" />
      <Button label="Reset filters" />
    </>
  );
  return <NoResults title="No results" content={content} />;
};

export const NoAccessToContent = () => {
  const text = "Please contact administrator for access rights.";
  const content = (
    <>
      <Typography text={text} variant="paragraph" customClass="py-3" />
      <a href=".">
        <Typography
          text="Return to dashboard"
          variant="paragraph"
          customClass={clsx(textColor("primary"), "underline")}
          customColor={true}
        />
      </a>
    </>
  );
  return <NoAccess title="You need permission to access" content={content} />;
};

export const PageNotFound = () => {
  const content = (
    <a href=".">
      <Typography
        text="Back to home"
        variant="paragraph"
        customClass={clsx(textColor("primary"), "underline py-3")}
        customColor={true}
      />
    </a>
  );
  return <NotFound title="Sorry, page not found" content={content} />;
};

export const OtherErrors = () => {
  const content = (
    <Typography
      text="Please try again in a few minutes. If the problem persists, contact your
    administrator."
      variant="paragraph"
      customClass="py-3"
    />
  );
  return <GeneralError title="Something went wrong!" content={content} />;
};
