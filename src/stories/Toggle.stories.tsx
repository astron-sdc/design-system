import { useState } from "react";
import { Meta } from "@storybook/react";
import Toggle from "src/components/Toggle.tsx";

const meta = {
  title: "Components/Toggle",
  component: Toggle,
} satisfies Meta;

export default meta;

export const SimpleToggle = () => {
  const [isSelected, setIsSelected] = useState(false);
  return (
    <Toggle
      isSelected={isSelected}
      setIsSelected={setIsSelected}
      label={isSelected ? "Yay I'm selected!" : "Select me!"}
    />
  );
};

export const DisabledToggle = () => {
  return (
    <div className="flex flex-col gap-4">
      <Toggle isDisabled={true} isSelected={true} label="I'm always right!" />
      <Toggle isDisabled={true} isSelected={false} label="I'm always left!" />
    </div>
  );
};

/** The label occupies one-third of the width of the parent. */
export const LabelOnTheLeft = () => {
  const [isSelected, setIsSelected] = useState(false);
  return (
    <div className="max-w-lg p-3 bg-lightGrey/30 dark:bg-baseBlack/40 rounded-[8px]">
      <Toggle
        isSelected={isSelected}
        setIsSelected={setIsSelected}
        label={isSelected ? "Yay I'm selected!" : "Select me!"}
        labelPlacement="left"
      />
    </div>
  );
};
