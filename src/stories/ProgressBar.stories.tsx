import { Meta } from "@storybook/react";
import { useState } from "react";
import ProgressBar from "src/components/ProgressBar.tsx";

const meta = {
  title: "Components/Progress Bar",
  component: ProgressBar,
} satisfies Meta;

export default meta;

/** The value of the progress bar can be set and controlled via state. */
export const WithValue = () => {
  const [value, setValue] = useState(0);
  setTimeout(() => {
    if (value < 100) setValue(value + 1);
    else setValue(0);
  }, 50);

  return (
    <div className="max-w-md">
      <ProgressBar value={value} />
    </div>
  );
};

export const IndeterminateValue = () => (
  <div className="max-w-md">
    <ProgressBar />
  </div>
);
