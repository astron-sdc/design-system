import { Meta } from "@storybook/react";
import Spinner from "src/components/Spinner.tsx";

const meta = {
    title: "Components/Spinner",
    component: Spinner,
  } satisfies Meta;
  
  export default meta;
  
export const DefaultSpinner = () => {
    return (
        <Spinner />
    )
};