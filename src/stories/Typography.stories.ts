import { Meta, StoryObj } from "@storybook/react";
import Typography from "src/components/Typography.tsx";

/**
 * Basic styled text component
 */
const meta = {
  title: "Docs/3_Typography",
  component: Typography,
} satisfies Meta;

export default meta;

export const Heading: StoryObj = {
  args: {
    text: "Hello world!",
    variant: "title",
  },
};

export const Paragraph: StoryObj = {
  args: {
    text: "Build-measure-learn (BML) is a continuous process of building/designing a product, measuring user metrics and learning from them to better respond to user needs and improve the product. The overall purpose of the cycle is to “Minimise the total time through the loop”.",
    variant: "paragraph",
  },
};

export const SideNote: StoryObj = {
  args: {
    text: "N.B. nota bene very important footnote",
    variant: "note",
  },
};
