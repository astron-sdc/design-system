import { Meta, StoryObj } from "@storybook/react";
import Icon, { iconNameOptions } from "src/components/Icon.tsx";
import { textColorOptions } from "src/components/utils/colors.ts";

/** Icons are visual symbols used to represent ideas, objects, or actions. Icons can also be used in buttons. They communicate messages at a glance, afford interactivity, and draw attention to important information. */
const meta = {
  title: "Components/Icon",
  component: Icon,
  argTypes: {
    name: { control: "select", options: iconNameOptions },
    color: { control: "select", options: textColorOptions },
  },
} satisfies Meta;

export default meta;

export const SimpleIcon: StoryObj = {
  args: {
    name: "AttentionCircle",
    color: "body",
    customClass: "w-[16px] h-[16px]",
  },
};
