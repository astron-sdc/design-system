import { Meta } from "@storybook/react";
import { useState } from "react";
import { MemoryRouter } from "react-router-dom";
import Tooltip from "src/components/Tooltip.tsx";
import Button from "src/components/Button.tsx";
import Typography from "src/components/Typography.tsx";
import DateInput from "src/components/DateInput.tsx";
import Dropdown from "src/components/Dropdown.tsx";
import Sidebar from "src/components/Sidebar.tsx";
import Icon from "src/components/Icon.tsx";

const meta = {
  title: "Components/Tooltip",
  component: Tooltip,
} satisfies Meta;

export default meta;

export const OnText = () => (
  <Typography
    text={
      <>
        {"Today's colloquium is about "}
        <Tooltip text="Definition: a technical term">
          <span>Transversality</span>
        </Tooltip>
        {", a word you've never seen before."}
      </>
    }
    variant="paragraph"
  />
);

export const OnButtonLabel = () => (
  <div className="m-16">
    <Button
      label={
        <Tooltip text="This action cannot be undone!" placement="top">
          Delete
        </Tooltip>
      }
      color="negative"
    />
  </div>
);

export const OnInputLabel = () => {
  const [date, setDate] = useState(new Date());
  const [value, setValue] = useState<string>();
  return (
    <form className="flex flex-col gap-4">
      <DateInput
        label={
          <Tooltip
            text="We need to know this for compliance reasons."
            children="What's your date of birth?"
          />
        }
        labelPlacement="outside-left"
        value={date}
        onValueChange={(date) => setDate(date as Date)}
      />
      <Dropdown
        label={
          <>
            {"What's your favourite "}
            <Tooltip
              text="An IDE is where you write your code."
              children={<u>IDE</u>}
              placement="bottom"
            />
            {"?"}
          </>
        }
        labelPlacement="outside-left"
        value={value}
        valueOptions={[
          { value: "Pycharm", label: "Pycharm" },
          { value: "VSCode", label: "VSCode" },
          { value: "Other", label: "Other" },
        ]}
        onValueChange={setValue}
      />
    </form>
  );
};

/**
 * The approach of wrapping the Icon inside a `div` would also work for other functional components.
 */
export const OnIcon = () => (
  <Tooltip text="Close" placement="right">
    <div className="max-w-fit">
      <Icon name="Cross" />
    </div>
  </Tooltip>
);

/**
 * Sidebar items have Tooltips built-in. They only show when the Sidebar is collapsed.
 */
export const OnSidebarItem = () => (
  <MemoryRouter>
    <Sidebar
      items={[
        { icon: "File", text: "Proposals", linkTo: "proposals" },
        {
          icon: "Review",
          text: "Review",
          linkTo: "reviews",
        },
      ]}
    />
  </MemoryRouter>
);
