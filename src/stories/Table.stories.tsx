/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Meta, StoryObj } from "@storybook/react";
import { useEffect, useState } from "react";
import { type DataTableValueArray } from "primereact/datatable";
import type { ColumnEditorOptions, ColumnEvent } from "primereact/column";
import Table from "src/components/Table.tsx";
import Badge from "src/components/Badge.tsx";
import Button from "src/components/Button.tsx";
import Typography from "src/components/Typography.tsx";
import TextInput from "src/components/TextInput.tsx";
import { Alert, Icon } from "src";

const meta = {
  title: "Components/Table",
  component: Table,
} satisfies Meta;

export default meta;

const textColumns = [
  { field: "name", header: "Name" },
  { field: "role", header: "Role" },
  { field: "status", header: "Status" },
];

const textRows = [
  {
    key: "1",
    name: "Tony",
    role: "CEO",
    status: "Active",
  },
  {
    key: "2",
    name: "Zoey",
    role: "Technical Lead",
    status: "Pause",
  },
  {
    key: "3",
    name: "Jane",
    role: "Senior Developer",
    status: "Active",
  },
  {
    key: "4",
    name: "William",
    role: "Community Manager",
    status: "Vacation",
  },
];

const columns = [
  { field: "name", header: "Name" },
  { field: "role", header: "Role" },
  { field: "status", header: "Status" },
  { field: "action", header: "Action" },
];

const rows = [
  {
    key: "1",
    name: "Tony",
    role: "CEO",
    status: <Badge text="Active" />,
    action: <Button label="Edit" color="secondary" icon="Edit" />,
  },
  {
    key: "2",
    name: "Zoey",
    role: "Technical Lead",
    status: <Badge text="Paused" color="warning" />,
    action: (
      <div className="flex flex-row gap-3 w-fit">
        <Button label="Edit" color="secondary" icon="Edit" />
        <Button label="Delete" color="negative" icon="Cross" />
      </div>
    ),
  },
  {
    key: "3",
    name: "Jane",
    role: "Senior Developer",
    status: <Badge text="Inactive" color="negative" />,
    action: (
      <div className="flex flex-row gap-3">
        <Button label="Edit" color="secondary" icon="Edit" />
        <Button label="Delete" color="negative" icon="Cross" />
      </div>
    ),
  },
  {
    key: "4",
    name: "William",
    role: "Community Manager",
    status: <Badge text="Vacation" color="neutral" />,
    action: (
      <div className="flex flex-row gap-3">
        <Button label="Edit" color="secondary" icon="Edit" />
        <Button label="View" color="primary" icon="Eye" />
      </div>
    ),
  },
];

export const TableWithText: StoryObj = {
  args: {
    columns: textColumns,
    rows: textRows,
  },
};

export const EmptyTable: StoryObj = {
  args: {
    columns: textColumns,
    rows: [],
  },
};

export const TableWithComponentsInHeader: StoryObj = {
  args: {
    columns: textColumns,
    rows: textRows,
    headerContent: (
      <div className="flex flex-row gap-2 pb-[10px]">
        <Button label="Create New" color="positive" icon="Plus" />
        <Button label="Edit" color="secondary" icon="Edit" />
      </div>
    ),
  },
};

export const TableWithComponentsInCells: StoryObj = {
  args: {
    columns: columns,
    rows: rows,
  },
};

/** The list of selected rows can be set and controlled via state. */
export const TableWithSelectableRows = () => {
  const [selected, setSelected] = useState<DataTableValueArray>([]);
  return (
    <>
      <Typography
        variant="h5"
        text={`Current selected: ${selected
          .map((row) => " " + row.name)
          .toString()}`}
        customClass="h-8"
      />
      <Table
        columns={columns}
        rows={rows}
        isSelectable={true}
        selected={selected}
        setSelected={setSelected}
      />
    </>
  );
};

/** The order of rows can be set and controlled via state. */
export const TableWithReorderableRows = () => {
  const [rowOrder, setRowOrder] = useState<DataTableValueArray>(textRows);
  return (
    <>
      <Typography
        variant="h5"
        text={`Current row order: ${rowOrder
          .map((row) => " " + row.name)
          .toString()}`}
        customClass="h-8"
      />
      <Table
        columns={textColumns}
        rows={rowOrder}
        isReorderable={true}
        onReorder={setRowOrder}
      />
    </>
  );
};

type Row = {
  row_id: string;
  name: string;
  role: string;
  salary: number;
  notes: string;
  action: JSX.Element;
};

const editableColumnRows = [
  {
    row_id: crypto.randomUUID(),
    name: "Tony",
    role: "CEO",
    salary: 150000,
    notes: "Nice guy",
  },
  {
    row_id: crypto.randomUUID(),
    name: "Olivia",
    role: "Senior Developer",
    salary: 10,
    notes: "Full stack",
  },
  {
    row_id: crypto.randomUUID(),
    name: "Bertrand",
    role: "Project manager",
    salary: 2000000,
    notes: "Micro manager",
  },
] as Row[];

/** Cells can be edited, cell values can be validated */
export const TableWithEditableCells = () => {
  const [isDeleting, setIsDeleting] = useState(false);
  
  const textEditor = (options: ColumnEditorOptions) => {
    return (
      <TextInput
        value={options.value as string}
        onValueChange={(e) =>
          options.editorCallback && options.editorCallback(e)
        }
      ></TextInput>
    );
  };

  const numberEditor = (options: ColumnEditorOptions) => {
    return (
      <TextInput
        type="number"
        value={options.value as string}
        onValueChange={(e) =>
          options.editorCallback && options.editorCallback(e)
        }
      ></TextInput>
    );
  };

  const onCellEditComplete = (e: ColumnEvent) => {
    const { rowData, newValue, field } = e;
    rowData[field] = newValue;
  };

  const isPositiveInteger = (val: unknown) => {
    let str = String(val);

    str = str.trim();

    if (!str) {
      return false;
    }

    str = str.replace(/^0+/, "") || "0";
    const n = Math.floor(Number(str));

    return n !== Infinity && String(n) === str && n >= 0;
  };

  const onSalaryEditComplete = (e: ColumnEvent) => {
    const { rowData, newValue, field, originalEvent: event } = e;
    if (isPositiveInteger(newValue)) {
      rowData[field] = newValue;
    } else {
      event.preventDefault();
    }
  };

  const nameBodytemplate = (rowData: unknown) => {
      return (
        <div className="flex justify-between">
              <div>{(rowData as Row).name}</div>
              <Icon name="Edit" />
        </div>
      )
  };

  const editableColumns = [
    { field: "row_id", header: "ID", style: { width: "30%" } },
    {
      field: "name",
      header: "Name",
      style: { width: "20%" },
      editor: textEditor,
      onCellEditComplete: onCellEditComplete,
      bodyTemplate: nameBodytemplate
    },
    { field: "role", header: "Role", style: { width: "20%" } },
    {
      field: "salary",
      header: "Salary",
      style: { width: "10%" },
      editor: numberEditor,
      onCellEditComplete: onSalaryEditComplete,
    },
    { field: "notes", header: "Notes", style: { width: "10%" } },
    { field: "action", header: "", style: { width: "10%" } },
  ];

  const [editableRows, setEditableRows] = useState<Row[]>([]);

  useEffect(() => {
    setEditableRows(editableColumnRows);
  }, []);

  const DeleteButton = (row: Row) => (
    <Button
      label="Delete"
      icon="Cross"
      onClick={() => {
        deleteEmployee(row);
        setIsDeleting(true);
        setTimeout(() => {
          setIsDeleting(false);
        }, 6000)
      }}
    />
  );

  for (const row of editableRows) {
    row.action = DeleteButton(row);
  }

  const addEmployee = () => {
    const newRow = {
      row_id: crypto.randomUUID(),
      name: "",
      role: "",
      salary: 0,
      notes: "",
    } as Row;
    newRow.action = DeleteButton(newRow);
    setEditableRows((old) => [...old, newRow]);
  };

  const deleteEmployee = (row: Row) => {
    const newRows = editableRows.filter((r) => r !== row);
    setEditableRows(newRows);
  };

  const alert = isDeleting ? <Alert message="Employee terminated" title="Boom baby" expireAfter={5000} /> : null;

  return <>
        <Table
      headerContent={
        <div className="flex flex-row gap-2 pb-[10px]">
          <Button
            color="positive"
            icon="Plus"
            label="Add Employee"
            onClick={addEmployee}
          />
        </div>
      }
      columns={editableColumns}
      rows={editableRows}
      editMode="cell"
    />
    {alert}
    </>
};
