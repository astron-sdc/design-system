import { Meta } from "@storybook/react";
import { useMemo, useState } from "react";
import TextArea from "src/components/TextArea.tsx";

const meta = {
  title: "Components/Text Area",
  component: TextArea,
} satisfies Meta;

export default meta;

export const Default = () => {
  const [value, setValue] = useState("");
  return (
    <TextArea
      label="Some long story goes here."
      value={value}
      onValueChange={setValue}
    ></TextArea>
  );
};

export const CustomValidation = () => {
  const [value, setValue] = useState("");

  const validateOnlyCapitals = (str: string) => str.match(/^[A-Z]+/g);

  const isInvalid = useMemo(() => {
    if (value === "") return false;
    return !validateOnlyCapitals(value);
  }, [value]);

  return (
    <TextArea
      label="Only capitals"
      value={value}
      onValueChange={setValue}
      isInvalid={isInvalid}
      errorMessage={isInvalid ? "Only capitals are allowed!" : ""}
    />
  );
};

export const Required = () => {
  const [value, setValue] = useState("");
  return (
    <TextArea
      label="I require a value"
      isRequired={true}
      value={value}
      onValueChange={setValue}
    />
  );
};

export const Disabled = () => {
  return (
    <TextArea
      label="You can't interact with this component!"
      isDisabled={true}
      value="*Evil laughter*"
    />
  );
};

export const ReadOnly = () => {
  return (
    <TextArea
      label="You can't change this value!"
      isReadOnly={true}
      value="*Neutral laughter*"
    />
  );
};

export const MaxLength = () => {
  const [value, setValue] = useState("");
  return (
    <TextArea
      label="Maximum of 100 characters"
      maxLength={100}
      value={value}
      onValueChange={setValue}
    />
  );
};

export const MaxRows = () => {
  const [value, setValue] = useState("");
  return (
    <TextArea
      label="Maximum of 2 rows"
      maxRows={2}
      value={value}
      onValueChange={setValue}
    />
  );
};

export const MinRows = () => {
  const [value, setValue] = useState("");
  return (
    <TextArea
      label="Minimum of 3 rows"
      maxRows={3}
      value={value}
      onValueChange={setValue}
    />
  );
};

export const DisableAutoSize = () => {
  const [value, setValue] = useState("");
  return (
    <TextArea
      label="Autosize is disabled"
      disableAutosize={true}
      value={value}
      onValueChange={setValue}
    />
  );
};

export const LabelOnTheLeft = () => {
  const [value, setValue] = useState("");
  return (
    <div className="w-full p-3 bg-lightGrey/30 dark:bg-baseBlack/40 rounded-[8px]">
      <TextArea
        label="Description"
        value={value}
        onValueChange={setValue}
        labelPlacement="outside-left"
      />
    </div>
  );
};
