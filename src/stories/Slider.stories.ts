import { Meta, StoryObj } from "@storybook/react";
import Slider from "src/components/Slider.tsx";

/** Sliders provide a visual indication of adjustable content, where the user can increase or decrease the value by moving the handle along a horizontal track.
A slider allows a user to select a single value or range of values available within the slider track. The slider comes in two different variants: value and range.*/
const meta = {
  title: "Components/Slider",
  component: Slider,
} satisfies Meta;

export default meta;

export const ValueSlider: StoryObj = {
  args: {
    min: 0,
    max: 100,
    defaultValue: 25,
  },
};

export const RangeSlider: StoryObj = {
  args: {
    min: 0,
    max: 100,
    defaultValue: [25, 60],
  },
};
