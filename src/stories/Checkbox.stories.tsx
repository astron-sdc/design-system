import { Meta } from "@storybook/react";
import { useState } from "react";
import Checkbox from "src/components/Checkbox.tsx";

const meta = {
  title: "Components/Checkbox",
  component: Checkbox,
} satisfies Meta;

export default meta;

export const SimpleCheckbox = () => {
  const [isSelected, setIsSelected] = useState(false);
  return (
    <Checkbox
      isSelected={isSelected}
      setIsSelected={setIsSelected}
      label={isSelected ? "Yay I'm selected!" : "Select me!"}
    />
  );
};

export const DisabledCheckbox = () => {
  return (
    <div className="flex flex-col gap-4">
      <Checkbox
        isDisabled={true}
        isSelected={true}
        label="I am always right!"
      />
      <Checkbox
        isDisabled={true}
        isSelected={false}
        label="I am always wrong :("
      />
    </div>
  );
};
