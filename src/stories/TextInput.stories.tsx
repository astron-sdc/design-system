import { Meta } from "@storybook/react";
import { useMemo, useState } from "react";
import TextInput from "src/components/TextInput.tsx";

/**
 * Width of the input is 100% of parent component unless [`labelPlacement === "outside-left"`](#label-on-the-left)
 */
const meta = {
  title: "Components/Text Input",
  component: TextInput,
} satisfies Meta;

export default meta;

export const CustomValidation = () => {
  const [value, setValue] = useState("");

  const validateEmail = (str: string) => {
    const regex = /^[\w\-.]+@([\w-]+\.)+[\w-]{2,}$/i;
    return regex.exec(str);
};

  const isInvalid = useMemo(() => {
    if (value === "") return false;
    return !validateEmail(value);
  }, [value]);

  return (
    <TextInput
      label="Contact email"
      value={value}
      onValueChange={setValue}
      isInvalid={isInvalid}
      errorMessage={isInvalid ? "Email is invalid" : ""}
    />
  );
};

export const IsClearable = () => {
  const [value, setValue] = useState("Click the cross!");

  return (
    <TextInput
      label="Search..."
      isClearable={true}
      value={value}
      onValueChange={setValue}
    />
  );
};

export const Disabled = () => {
  return (
    <TextInput
      label="You can't interact with this component!"
      isDisabled={true}
      value="*Evil laughter*"
    />
  );
};

export const ReadOnly = () => {
  return (
    <TextInput
      label="You can't change this value!"
      isReadOnly={true}
      value="*Neutral laughter*"
    />
  );
};

export const MaxLength = () => {
  const [value, setValue] = useState("");
  return (
    <TextInput
      label="Maximum of 10 characters"
      maxLength={10}
      value={value}
      onValueChange={setValue}
    />
  );
};

export const PredefinedType = () => {
  const [value, setValue] = useState("");
  return (
    <TextInput
      label="This input is rendered as a password type"
      type="password"
      value={value}
      onValueChange={setValue}
    />
  );
};

export const PredefinedInputMode = () => {
  const [value, setValue] = useState("");
  return (
    <TextInput
      label="The keyboard for this input is shown as a numeric type"
      inputMode="numeric"
      value={value}
      onValueChange={setValue}
    />
  );
};

/** The label occupies one-third of the width of the parent, and the form field two-thirds of it. */
export const LabelOnTheLeft = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  return (
    <div className="w-full p-3 bg-lightGrey/30 dark:bg-baseBlack/40 rounded-[8px]">
      <TextInput
        label="Name"
        labelPlacement="outside-left"
        value={name}
        onValueChange={setName}
      />
      <br />
      <TextInput
        label="Email"
        labelPlacement="outside-left"
        value={email}
        onValueChange={setEmail}
      />
    </div>
  );
};
