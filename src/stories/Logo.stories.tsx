import { Meta } from "@storybook/react";
import Logo from "src/components/Logo";

const meta = {
  title: "Components/Logo",
  component: Logo,
} satisfies Meta;

export default meta;

/**
 * Logo following dark mode automatically
 */
export const TheLogo = () => {
  return <Logo />;
};
