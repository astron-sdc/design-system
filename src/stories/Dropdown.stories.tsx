import { Meta } from "@storybook/react";
import { useState } from "react";
import Dropdown from "src/components/Dropdown.tsx";
import Typography from "src/components/Typography.tsx";
import { ColorType } from "src/components/utils/colors";

/**
 * Width of the input is 100% of parent component unless [`labelPlacement === "outside-left"`](#label-on-the-left)
 */
const meta = {
  title: "Components/Dropdown",
  component: Dropdown,
} satisfies Meta;

export default meta;

export const SimpleDropdown = () => {
  const [value, setValue] = useState<string>();

  const valueOptions = [
    { value: "vanilla", label: "Vanilla" },
    { value: "mocha", label: "Mocha" },
    { value: "mint", label: "Mint" },
    { value: "raspberry", label: "Raspberry" },
    { value: "apple", label: "Apple" },
  ];

  return (
    <>
      <Dropdown
        label="What's your favourite ice cream?"
        valueOptions={valueOptions}
        onValueChange={setValue}
      />
      <Typography
        text={`You selected: ${value || "nothing"}`}
        variant="paragraph"
      />
    </>
  );
};

const icecreams : { value: string; label: string; badgeColor?: ColorType }[] = [
  { value: "vanilla", label: "Vanilla", badgeColor: "warning" },
  { value: "mocha", label: "Mocha" },
  { value: "mint", label: "Mint", badgeColor: "positive" },
  { value: "raspberry", label: "Raspberry" },
  { value: "apple", label: "Apple" },
  { value: "smurf", label: "Smurf" },

  { value: "orange", label: "Orange" },
  { value: "banana", label: "Banana" },

  { value: "cinnamon", label: "Cinnamon" },
  { value: "lemon", label: "Lemon" },

  { value: "pistache", label: "Pistache" },
  { value: "peach", label: "Peach" },
];

export const MultiSelect = () => {
  const [selectedItems, setSelectedItems] = useState<string[]>([]);

  return (
    <>
      <Dropdown
        label="What's your favourite ice cream?"
        valueOptions={icecreams}
        values={selectedItems}
        onValuesChange={setSelectedItems}
        multiSelect={true}
      />
      <Typography
        text={`You selected: ${selectedItems.join(", ") || "nothing"}`}
        variant="paragraph"
      />
    </>
  );
};

export const MultiSelectLimit = () => {
  const [selectedItems, setSelectedItems] = useState<string[]>(["peach"]);

  return (
    <>
      <Dropdown
        label="What's your favourite ice cream? (3 max)"
        valueOptions={icecreams}
        values={selectedItems}
        onValuesChange={setSelectedItems}
        multiSelect={true}
        maxSelectCount={3}
      />
      <Typography
        text={`You selected: ${selectedItems.join(", ") || "nothing"}`}
        variant="paragraph"
      />
    </>
  );
};

export const Disabled = () => {
  return (
    <Dropdown
      label="You can't choose your favourite ice cream."
      valueOptions={[]}
      onValueChange={() => {}}
      isDisabled={true}
    />
  );
};

export const LabelOnTheLeft = () => {
  return (
    <div className="w-full p-3 bg-lightGrey/30 dark:bg-baseBlack/40 rounded-[8px]">
      <Dropdown
        label="I'm the label and I'm on the left now"
        valueOptions={[]}
        onValueChange={() => {}}
        labelPlacement="outside-left"
      />
    </div>
  );
};
