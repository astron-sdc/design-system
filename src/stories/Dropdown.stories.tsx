import { Meta } from "@storybook/react";
import { useState } from "react";
import Dropdown from "src/components/Dropdown.tsx";
import Typography from "src/components/Typography.tsx";

const meta = {
  title: "Components/Dropdown",
  component: Dropdown,
} satisfies Meta;

export default meta;

export const SimpleDropdown = () => {
  const [value, setValue] = useState<string>();

  const valueOptions = [
    { value: "vanilla", label: "Vanilla" },
    { value: "mocha", label: "Mocha" },
    { value: "mint", label: "Mint" },
    { value: "raspberry", label: "Raspberry" },
    { value: "apple", label: "Apple" },
  ];

  return (
    <>
      <Dropdown
        label="What's your favourite ice cream?"
        valueOptions={valueOptions}
        onValueChange={setValue}
      />
      <Typography
        text={`You selected: ${value || "nothing"}`}
        variant="paragraph"
      />
    </>
  );
};

export const Disabled = () => {
  return (
    <Dropdown
      label="You can't choose your favourite ice cream."
      valueOptions={[]}
      onValueChange={() => {}}
      isDisabled={true}
    />
  );
};
