import { Meta } from "@storybook/react";
import { useState } from "react";
import ProfileDropdown from "src/components/ProfileDropdown";
import Toggle from "src/components/Toggle";
import { IconName } from "src/components/Icon.tsx";
import { Typography } from "src/index.ts";
import { textColor } from "src/components/utils/classes.ts";

const meta = {
  title: "Components/Profile Dropdown",
  component: ProfileDropdown,
} satisfies Meta;

export default meta;

export const BasicProfileDropdown = () => {
  const [dark, setDark] = useState(false);
  const menu = [
    {
      sectionKey: "section1",
      sectionContent: [
        {
          key: "toggle",
          icon: "Eye" as IconName,
          content: (
            <Toggle
              isSelected={dark}
              setIsSelected={setDark}
              label="Dark mode"
              labelPlacement="left"
              wrapperClass="justify-between w-full"
            />
          ),
        },
      ],
    },
    {
      sectionKey: "section2",
      sectionContent: [
        { key: "1", content: "Plain text" },
        { key: "2", content: <Typography text="h5 text" variant="h5" /> },
      ],
    },
    {
      sectionKey: "section3",
      sectionContent: [
        {
          key: "3",
          icon: "Cross" as IconName,
          content: "Logout",
          customClass: textColor("negative"),
        },
      ],
    },
  ];

  return (
    <div className="h-64">
      <ProfileDropdown menu={menu} label="Ken Williams" />
    </div>
  );
};
