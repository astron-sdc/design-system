import { Meta, StoryObj } from "@storybook/react";
import { useState } from "react";
import Button from "src/components/Button.tsx";
import { iconNameOptions } from "src/components/Icon.tsx";
import { colorOptions } from "src/components/utils/colors.ts";
import Typography from "src/components/Typography.tsx";
import TextInput from "src/components/TextInput.tsx";

/** Use buttons as triggers for actions that are used in forms, toolbars, dialog footers and as stand-alone action triggers. Try to avoid the usage of buttons for navigation. The main difference between actions and navigation is that Actions are operations performed on objects, while Navigation refers to elements on the screen or view that take you to another context in the application. */
const meta = {
  title: "Components/Button",
  component: Button,
  argTypes: {
    color: { control: "radio", options: colorOptions },
    onClick: () => {},
  },
} satisfies Meta;

export default meta;

export const TextOnlyButton: StoryObj = {
  args: {
    label: "Click me!",
    color: "primary",
  },
};

export const IconButton: StoryObj = {
  args: {
    label: "Download",
    color: "primary",
    icon: "Download",
  },
  argTypes: {
    icon: { control: "select", options: iconNameOptions },
  },
};

export const DisabledButton: StoryObj = {
  args: {
    label: "You can't click me!",
    isDisabled: true,
  },
};

/** In order for the Button to listen for Enter keypress, mostly used for submitting forms. Provide `type="submit"` to the Button component, and wrap it in a `<form>` element. For the native form element, you might also want to use an `onSubmit` with `preventDefault` and `stopPropagation` so the page does not reload. But there are also form libraries that take care of that. */
export const SubmitButton = () => {
  const [value, setValue] = useState("");
  const [count, setCount] = useState(0);
  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        e.stopPropagation();
      }}
      className="max-w-md flex flex-col gap-2"
    >
      <TextInput value={value} onValueChange={setValue} />
      <Button
        label="Submit"
        type="submit"
        onClick={() => setCount(count + 1)}
      />
      <Typography
        text={`You have submitted ${count} times!`}
        variant="paragraph"
      />
    </form>
  );
};

export const ButtonWithLoader = () => {
  const [isLoading, setIsLoading] = useState(false);

  return (
    <Button
      label="Download"
      icon="Download"
      isDisabled={isLoading}
      showLoader={isLoading}
      onClick={() => {
        setIsLoading(true);
        setTimeout(() => {
          setIsLoading(false);
        }, 2000);
      }}
    />
  );
};
