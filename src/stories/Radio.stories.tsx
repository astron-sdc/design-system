import { Meta } from "@storybook/react";
import { useState } from "react";
import Typography from "src/components/Typography.tsx";
import Radio from "src/components/Radio.tsx";

const meta = {
  title: "Components/Radio",
  component: Radio,
} satisfies Meta;

export default meta;

export const SimpleRadio = () => {
  const [value, setValue] = useState<string>();

  const valueOptions = [
    { value: "vanilla", label: "Vanilla" },
    { value: "mocha", label: "Mocha" },
    { value: "mint", label: "Mint" },
    { value: "raspberry", label: "Raspberry" },
  ];

  return (
    <>
      <Radio
        label="What's your favourite ice cream?"
        valueOptions={valueOptions}
        onValueChange={setValue}
      />
      <Typography
        text={`You selected: ${value || "nothing"}`}
        variant="paragraph"
      />
    </>
  );
};

export const DisabledRadio = () => {
  const valueOptions = [
    { value: "mint", label: "Mint", isDisabled: true },
    { value: "raspberry", label: "Raspberry", isDisabled: true },
  ];

  return (
      <Radio
        label="Your favourite ice cream must be raspberry"
        valueOptions={valueOptions}
        value="raspberry"
      />
  );
};
