import { Meta } from "@storybook/react";
import ActionMenu from "src/components/Menu.tsx";
import { IconName } from "src/components/Icon.tsx";
import { textColor } from "src/components/utils/classes.ts";

const meta = {
  title: "Components/Menu",
} satisfies Meta;

export default meta;

export const ActionMenuWithoutIcons = () => {
  const menu = [
    {
      sectionKey: "section1",
      sectionContent: [{ key: "expand", content: "Expand" }],
    },
    {
      sectionKey: "section2",
      sectionContent: [{ key: "add", content: "Add" }],
    },
    {
      sectionKey: "section3",
      sectionContent: [
        {
          key: "remove",
          content: "Remove",
          customClass: textColor("negative"),
        },
      ],
    },
  ];
  return <ActionMenu label="Menu" menu={menu} />;
};

export const ActionMenuWithIcons = () => {
  const menu = [
    {
      sectionKey: "section1",
      sectionContent: [
        { key: "expand", content: "Expand", icon: "Expand" as IconName },
      ],
    },
    {
      sectionKey: "section2",
      sectionContent: [
        { key: "add", content: "Add", icon: "Plus" as IconName },
      ],
    },
    {
      sectionKey: "section3",
      sectionContent: [
        {
          key: "remove",
          content: "Remove",
          icon: "Cross" as IconName,
          customClass: textColor("negative"),
        },
      ],
    },
  ];
  return <ActionMenu label="Menu" menu={menu} />;
};
