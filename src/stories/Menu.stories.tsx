import { Meta } from "@storybook/react";
import { useState } from "react";
import {
  ActionMenu,
  MenuItemProps,
  ProfileMenu,
} from "src/components/Menu.tsx";
import Typography from "src/components/Typography.tsx";

const meta = {
  title: "Components/Menu",
} satisfies Meta;

export default meta;

const items = [
  { key: "expand", itemName: "Expand", iconName: "Expand" },
  { key: "add", itemName: "Add", iconName: "Plus" },
  { key: "remove", itemName: "Remove", iconName: "Cross", danger: true },
];

export const ActionMenuWithoutIcons = () => {
  const [selectedKey, setSelectedKey] = useState("");
  return (
    <>
      <Typography
        text={`You selected: ${selectedKey || "nothing"}`}
        variant="paragraph"
      />
      <ActionMenu
        label="Menu"
        items={
          items.map((item: object): object => ({
            ...item,
            iconName: null,
          })) as MenuItemProps[]
        }
        onAction={(key) => setSelectedKey(key as string)}
      />
    </>
  );
};

export const ActionMenuWithIcons = () => {
  const [selectedKey, setSelectedKey] = useState("");
  return (
    <>
      <Typography
        text={`You selected: ${selectedKey || "nothing"}`}
        variant="paragraph"
      />
      <ActionMenu
        label="Menu"
        items={items as MenuItemProps[]}
        onAction={(key) => setSelectedKey(key as string)}
      />
    </>
  );
};

export const ProfileMenuWithIcons = () => {
  const [selectedKey, setSelectedKey] = useState("");
  return (
    <>
      <Typography
        text={`You selected: ${selectedKey || "nothing"}`}
        variant="paragraph"
      />
      <ProfileMenu
        label="Johnny Depp"
        items={items as MenuItemProps[]}
        onAction={(key) => setSelectedKey(key as string)}
      />
    </>
  );
};
