import { Meta } from "@storybook/react";
import { useState } from "react";
import DateInput, { DateArray, DateNull } from "src/components/DateInput.tsx";

const meta = {
  title: "Components/Date Input",
} satisfies Meta;

export default meta;

export const WithoutLimits = () => {
  const [value, setValue] = useState<Date>();

  return (
    <DateInput
      label="When's your birthday?"
      value={value}
      onValueChange={(date) => setValue(date as Date)}
    />
  );
};

export const WithTimeSelect = () => {
  const [value, setValue] = useState<Date>();

  return (
    <DateInput
      label="Specify a date and time"
      value={value}
      showTimeSelect={true}
      onValueChange={(date) => setValue(date as Date)}
    />
  );
};

export const WithCustomDateAndTimeFormat = () => {
  const [value, setValue] = useState<Date>();

  return (
    <DateInput
      label="Specify a date and time with custom format"
      value={value}
      dateFormat="yyyy-MM-dd"
      timeFormat="hh:mm a"
      showTimeSelect={true}
      onValueChange={(date) => setValue(date as Date)}
    />
  );
};

export const DateRange = () => {
  const [startDate, setStartDate] = useState<DateNull>();
  const [endDate, setEndDate] = useState<DateNull>();

  const onChange = (dates: DateArray) => {
    if (dates.length === 2) {
      const [start, end] = dates;
      setStartDate(start);
      setEndDate(end);
    }
  };

  return (
    <DateInput
      label="When's your holiday?"
      startDate={startDate}
      endDate={endDate}
      onValueChange={(dates) => onChange(dates as DateArray)}
      isRange={true}
    />
  );
};

/** The label occupies one-third of the width of the parent, and the form field two-thirds of it. */
export const LabelOnTheLeft = () => {
  const [date, setDate] = useState<Date>();
  return (
    <div className="max-w-lg p-3 bg-lightGrey/30 dark:bg-baseBlack/40 rounded-[8px]">
      <DateInput
        label="When's the party?"
        labelPlacement="outside-left"
        value={date}
        onValueChange={(value) => setDate(value as Date)}
      />
    </div>
  );
};

export const Disabled = () => {
  return (
    <DateInput
      label="Can't touch this"
      onValueChange={() => {}}
      isDisabled={true}
    />
  );
};
