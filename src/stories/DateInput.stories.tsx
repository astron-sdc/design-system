import {Meta, StoryObj} from "@storybook/react";
import { useState } from "react";
import DateInput, {DateNull} from "src/components/DateInput";
import Typography from "src/components/Typography";

const meta = {
  title: "Components/Date Input",
  component: DateInput,
} satisfies Meta;

export default meta;

export const WithoutLimits = () => {
  const [value, setValue] = useState<DateNull>(null);
  return (
    <DateInput
      label="When's your birthday?"
      value={value}
      onValueChange={setValue}
    />
  );
};

export const WithTimeSelect = () => {
  const [valueTimeSelect, setValueTimeSelect] = useState<DateNull>(new Date());
  return (
    <>
    <Typography variant="paragraph" text={`Selected time: ${valueTimeSelect?.toUTCString()}`}/>
    <DateInput
      label="Specify a date and time"
      value={valueTimeSelect}
      showTimeSelect={true}
      onValueChange={setValueTimeSelect}
    />
    </>
  );}

export const ForceUTC = () => {
  const [utcValue, setUtcValue] = useState<DateNull>(new Date('1 Jan 2025 00:00:00 GMT+1'));
  return (
    <>
    <Typography variant="paragraph" text={`Selected time locale string: ${utcValue?.toLocaleString()}`}/>
    <Typography variant="paragraph" text={`Selected time UTC string: ${utcValue?.toUTCString()}`}/>
    <DateInput
      label="Specify a date and time in UTC"
      value={utcValue}
      showTimeSelect={true}
      forceUTC={true}
      onValueChange={setUtcValue}
    />
    </>
  );}

export const WithCustomLocale: StoryObj = {
  args: {
    label: "Specify a date and time with custom locale",
    value: new Date(),
    locale: "en-US",
    showTimeSelect: true,
    onValueChange: () => {},
  },
  parameters: {controls: {include: ["locale"]}},
}

export const Invalid: StoryObj = {
  args: {
    label: "Departure time",
    value: new Date(),
    onValueChange: () => {},
    isInvalid: true,
    showTimeSelect: true,
    errorMessage: "Input invalid!",
  },
  parameters: {controls: {include: ["isInvalid"]}},
}

export const Disabled: StoryObj = {
  args: {
    label: "Can't touch this",
    value: new Date(),
    onValueChange: () => {},
    isDisabled: true,
  },
  parameters: {controls: {include: ["isDisabled"]}},
}


/** The label occupies one-third of the width of the parent, and the form field two-thirds of it. */
export const LabelOnTheLeft = () => {
  const [value, setValue] = useState<DateNull>(null);
  return (
    <div className="w-full p-3 bg-lightGrey/30 dark:bg-baseBlack/40 rounded-[8px]">
      <DateInput
        label="When's the party?"
        labelPlacement="outside-left"
        value={value}
        onValueChange={setValue}
      />
    </div>
  );
};