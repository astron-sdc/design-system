import { Meta } from "@storybook/react";
import { MemoryRouter } from "react-router-dom";
import { IconName } from "src/components/Icon";
import Sidebar, { SidebarItem } from "src/components/Sidebar";
import ApplicationLayout from "src/layouts/ApplicationLayout";

const meta = {
  title: "Layouts/ApplicationLayout",
  component: ApplicationLayout,
  parameters: {
    layout: "fullscreen",
  },
} satisfies Meta;

export default meta;

const Content = () => {
  return (
    <div className="bg-lightGrey dark:bg-baseBlack h-full py-6 pl-20 pr-32">
      Awesome recipes!
    </div>
  );
};

const MenuBar = () => {
  return <div className="h-20 bg-grey dark:bg-secondary">I am A menu!</div>;
};


/**
 * Basic Side, Top and Content layout
 */
export const BasicLayout = () => {
  const items: SidebarItem[] = [
    { icon: "File", text: "Proposals", linkTo: "proposals" },
    {
      icon: "Review",
      text: "Review",
      linkTo: "reviews",
    },
    {
      icon: "Eye",
      text: "Helpdesk",
      linkTo: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
      openInNewWindow: true,
    },
    {
      icon: "TMSS",
      text: "Telescope Manager Specification System",
      linkTo: "https://tmss.lofar.eu",
      openInNewWindow: true,
    },
    { icon: "QuestionMark", text: "Documentation", linkTo: "docs" },
  ];

  const top = {
    icon: "Apt" as IconName,
    text: "APT",
    textLong: "Astron Proposal Tool",
  };



  return (
    <MemoryRouter>
      <ApplicationLayout
        side={<Sidebar items={items} top={top} />}
        top={<MenuBar />}
        content={<Content />}
      />
    </MemoryRouter>
  );
};
