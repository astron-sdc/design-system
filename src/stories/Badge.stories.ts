import { Meta, StoryObj } from "@storybook/react";
import Badge from "src/components/Badge.tsx";
import { colorOptions } from "src/components/utils/colors.ts";

/** Multiple or single badges can be used to categorize items or give a status. Use short labels for easy scanning. Use two words only if necessary to describe the status and differentiate it from other badges. */
const meta = {
  title: "Components/Badge",
  component: Badge,
  argTypes: {
    color: { control: "radio", options: colorOptions },
    backgroundColor: { control: "color" },
  },
} satisfies Meta;

export default meta;

export const NormalBadge: StoryObj = {
  args: {
    text: "Ready",
    color: "primary",
  },
};

export const InvertedBadge: StoryObj = {
  args: {
    text: "Draft",
    color: "secondary",
    inverted: true,
  },
};
