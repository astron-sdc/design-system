import { Meta } from "@storybook/react";
import Chart, {
  ChartData,
  LineChartOptions,
  PieChartOptions,
} from "src/components/Chart";

const meta = {
  title: "Components/Chart",
  component: Chart,
} satisfies Meta;

export default meta;

export const BarChart = () => {
  const consoleData: ChartData = {
    labels: ["PS4", "PS5", "Nintendo Switch", "Xbox Series X"],
    datasets: [
      {
        label: "Consoles owned",
        data: [1, 3, 2, 5],
      },
    ],
  };

  return <Chart data={consoleData} chartType="bar" />;
};

export const DoughnutChart = () => {
  const doughnutData: ChartData = {
    labels: ["Glazed", "Boston cream", "Apple cider", "Jelly-filled", "Sugar"],
    datasets: [
      {
        label: "Doughnut types",
        data: [1, 3, 2, 5],
      },
    ],
  };

  const options: PieChartOptions = {
    cutout: "5%",
    rotation: 180,
  };

  return <Chart data={doughnutData} options={options} chartType="doughnut" />;
};

export const LineChart = () => {
  const lineData: ChartData = {
    labels: ["1", "2", "3", "4", "5"],
    datasets: [
      {
        label: "Squares",
        data: [1, 4, 9, 16, 25],
      },
    ],
  };

  const options: LineChartOptions = {
    showLine: true,
  };

  return <Chart chartType="line" data={lineData} options={options} />;
};

export const PieChart = () => {
  const pieData: ChartData = {
    labels: ["Pacman", "Not pacman"],
    datasets: [
      {
        label: "Squares",
        data: [75, 25],
      },
    ],
  };

  const options: PieChartOptions = {
    cutout: 0,
    rotation: 135,
  };

  return <Chart chartType="pie" data={pieData} options={options} />;
};

export const SkyDistributionChart = () => {
  const labels = Array.from(Array(24).keys()).map(String);
  const data = Array.from(Array(24).keys()).map(
    (_, i) => (i + 1) * (Math.random() * 10),
  );

  const skyData: ChartData = {
    labels: labels,
    datasets: [
      {
        label: "Sky distribution",
        data: data,
      },
    ],
  };

  return (
    <Chart
      chartType="bar"
      data={skyData}
      x_label="RA (hour)"
      y_label="Time (hour)"
      legend_position="right"
    />
  );
};
