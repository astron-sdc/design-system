import { Meta, StoryObj } from "@storybook/react";
import Breadcrumb from "src/components/Breadcrumb.tsx";

const meta = {
  title: "Components/Breadcrumb",
  component: Breadcrumb,
} satisfies Meta;

export default meta;

export const OneLevel: StoryObj = {
  args: {
    items: [{ name: "Home", href: "/" }],
  },
};

export const MultipleLevels: StoryObj = {
  args: {
    items: [
      { name: "Home", href: "/" },
      { name: "Category", href: "/category" },
      { name: "Subcategory", href: "/category/subcategory" },
      { name: "This page", href: "/category/subcategory/this-page" },
    ],
  },
};

export const MoreThanFourLevels: StoryObj = {
  args: {
    items: [
      { name: "Home", href: "/" },
      { name: "Level 1", href: "/level-1" },
      { name: "Level 2", href: "/level-1/level-2" },
      { name: "Level 3", href: "/level-1/level-2/level-3" },
      { name: "This page", href: "/level-1/level-2/level-3/this-page" },
    ],
  },
};
