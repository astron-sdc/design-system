import { withThemeByClassName } from "@storybook/addon-styling";
import type { Preview } from "@storybook/react";
import "react-datepicker/dist/react-datepicker.css";
import "src/index.css";

export const decorators = [
  withThemeByClassName({
    themes: {
      light: "light",
      dark: "dark",
    },
    defaultTheme: "light",
  }),
];

export const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
};
