import type { StorybookConfig } from "@storybook/react-vite";

const config: StorybookConfig = {
  stories: ["../src/**/*.mdx", "../src/stories/*.stories.@(js|jsx|mjs|ts|tsx)"],
  addons: [
    {
      name: '@storybook/addon-essentials',
      options: {
        backgrounds: false, // use light/dark mode toggle instead of background button
      },
    },
    "@storybook/addon-coverage",
    "@storybook/addon-themes",
    '@storybook/addon-a11y',  // so the tab in the UI goes last
  ],
  framework: {
    name: "@storybook/react-vite",
    options: {},
  },
  docs: {
    autodocs: true,
  },
};
export default config;


