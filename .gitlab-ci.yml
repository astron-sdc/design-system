stages:
  - test
  - build
  - release
  - integration
  - deploy

workflow:
  rules:
    # don't create a pipeline if it's a commit pipeline, on a branch and that branch has open merge requests.
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never

    # Tag docker images with latest
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      variables:
        DOCKER_IMAGE_TAG: "latest"

    # Tag docker images with a slug of the branch name
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      variables:
        DOCKER_IMAGE_TAG: "$CI_COMMIT_REF_SLUG"

default:
  interruptible: true
  image: node:lts-slim
  tags:
    - "sdc-dev"
  before_script:
    - apt-get update && apt-get install -y git
    - node --version
    - npm ci

#test_storybook:
#  stage: test
#  needs: [build_static_files]
#  image: mcr.microsoft.com/playwright:v1.42.1-jammy
#  script:
#    - npm install
#    - npx playwright install --with-deps
#    - npm run test-storybook:ci

build_static_files:
  stage: build
  script:
    - npm run build-storybook
  artifacts:
    paths:
      - storybook-static
    expire_in: "1w"

build_package:
  stage: build
  script:
    - npm run build
  artifacts:
    paths:
      - dist
    expire_in: "1w"

semantic_release:
  stage: release
  needs: [build_package]
  interruptible: false
  before_script:
    - npm pkg delete scripts.prepare
    - apt-get update && apt-get install -y git
    - node --version
    - npm ci
  script:
    - echo "@astron-sdc:registry=https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/" > .npmrc
    - echo "//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}" >> .npmrc
    - npx semantic-release@22
  rules:
    - if: $CI_COMMIT_BRANCH == 'main'

build_docker_image:
  stage: build
  image: docker:20-cli
  needs: [build_static_files]
  services:
    - docker:20-dind
  before_script:
    - docker --version
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$DOCKER_IMAGE_TAG" .
    - docker push "$CI_REGISTRY_IMAGE:$DOCKER_IMAGE_TAG"

.deploy_step:
  stage: deploy
  image: docker:20-cli
  interruptible: false
  when: manual
  tags:
    - "sdc-dev"
  before_script:
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh
    - cp $SDC_KNOWN_HOSTS ~/.ssh/known_hosts
  script:
    - ssh $DEPLOY_USER@$DEPLOY_HOST "mkdir -p $SERVICE_DIR"
    - scp docker-compose.yml $DEPLOY_USER@$DEPLOY_HOST:$SERVICE_DIR/docker-compose.yml
    - |
      ssh $DEPLOY_USER@$DEPLOY_HOST "echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY &&\
      docker pull "$CI_REGISTRY_IMAGE:$DOCKER_IMAGE_TAG" &&\
      DESIGN_SYSTEM_VERSION=$DOCKER_IMAGE_TAG HOSTNAME=$HOSTNAME docker-compose -f $SERVICE_DIR/docker-compose.yml up -d --force-recreate"
    - echo "Application deployed"

deploy_to_test:
  extends: .deploy_step
  environment:
    name: sdc-dev
    url: https://sdc-dev.astron.nl/design-system
  variables:
    SERVICE_DIR: /docker_compose/design-system
    DEPLOY_HOST: dop814.astron.nl
    DEPLOY_USER: gitlab-deploy
    HOSTNAME: sdc-dev.astron.nl

deploy_to_production:
  extends: .deploy_step
  environment:
    name: sdc
    url: https://sdc.astron.nl/design-system
  variables:
    SERVICE_DIR: /opt/dockercompose/design-system
    DEPLOY_HOST: dop821.astron.nl
    DEPLOY_USER: sdco
    HOSTNAME: sdc.astron.nl
  only:
    - main
