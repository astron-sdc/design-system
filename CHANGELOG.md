## [3.20.1](https://gitlab.com/astron-sdc/design-system/compare/v3.20.0...v3.20.1) (2025-03-05)


### Bug Fixes

* add legend and scale title options ([15ee42d](https://gitlab.com/astron-sdc/design-system/commit/15ee42d960868e2eea20cf17b450ff23b9d65706))

# [3.20.0](https://gitlab.com/astron-sdc/design-system/compare/v3.19.0...v3.20.0) (2025-03-04)


### Features

* allow column header to be ReactNode ([cdbe17f](https://gitlab.com/astron-sdc/design-system/commit/cdbe17fb72982da7cf8d3a09831231d83aff6a11))

# [3.19.0](https://gitlab.com/astron-sdc/design-system/compare/v3.18.0...v3.19.0) (2025-02-27)


### Features

* new datepicker ([7c4a3b2](https://gitlab.com/astron-sdc/design-system/commit/7c4a3b25bc680ef9c800f47b0d0264daad58cc46))
* new datepicker ([749a0ce](https://gitlab.com/astron-sdc/design-system/commit/749a0ceb8d8d4587f547803b5d47acd6dbcfd038))

# [3.18.0](https://gitlab.com/astron-sdc/design-system/compare/v3.17.0...v3.18.0) (2025-02-24)


### Bug Fixes

* rename highlighted > highlightedRows ([684a0c7](https://gitlab.com/astron-sdc/design-system/commit/684a0c7c89cd33c9fcfa26a823f0cee000c1705f))


### Features

* add highighted rows, row click events to table ([b9b3e43](https://gitlab.com/astron-sdc/design-system/commit/b9b3e43ad30b42a4652caf21fd38a25836a9caec))
* add highlightSelectedRow and selectedRowColor props to Table ([92d9c2c](https://gitlab.com/astron-sdc/design-system/commit/92d9c2c34d37bad7899f346345cacde4fd01a90d))

# [3.17.0](https://gitlab.com/astron-sdc/design-system/compare/v3.16.1...v3.17.0) (2025-02-18)


### Features

* Allow undefined column headers ([c7d2ea0](https://gitlab.com/astron-sdc/design-system/commit/c7d2ea0a0a5e15edc5190022e183288eec1b6113))

## [3.16.1](https://gitlab.com/astron-sdc/design-system/compare/v3.16.0...v3.16.1) (2025-01-13)


### Bug Fixes

* readme change to trigger release ([570718a](https://gitlab.com/astron-sdc/design-system/commit/570718a5c258fe618b8db6cfcd2f71f2a2374f3a))

# [3.16.0](https://gitlab.com/astron-sdc/design-system/compare/v3.15.1...v3.16.0) (2025-01-09)


### Features

* Autocomplete multiselect ([d49e045](https://gitlab.com/astron-sdc/design-system/commit/d49e045be902a6631c105309a503edeebd90ab11))

## [3.15.1](https://gitlab.com/astron-sdc/design-system/compare/v3.15.0...v3.15.1) (2025-01-06)


### Bug Fixes

* make logo 32 pixels instead of 100% ([7001e50](https://gitlab.com/astron-sdc/design-system/commit/7001e50d83795b4f10b35f54aa99582256fe6a3c))

# [3.15.0](https://gitlab.com/astron-sdc/design-system/compare/v3.14.0...v3.15.0) (2024-12-19)


### Features

* add new logo ([3655190](https://gitlab.com/astron-sdc/design-system/commit/365519096275bd2a9c9b427461e5f27a2259e59b))

# [3.14.0](https://gitlab.com/astron-sdc/design-system/compare/v3.13.1...v3.14.0) (2024-12-16)


### Features

* add inverted option ([472cf0b](https://gitlab.com/astron-sdc/design-system/commit/472cf0b786fe9e26d10604534abdc3fd65713749))
* add inverted option ([2d17b52](https://gitlab.com/astron-sdc/design-system/commit/2d17b528fac3828f8bdcb61c3764ae008e6f6cf4))

## [3.13.1](https://gitlab.com/astron-sdc/design-system/compare/v3.13.0...v3.13.1) (2024-12-16)


### Bug Fixes

* Make menu expand button use same text color as regular buttons ([2479e1f](https://gitlab.com/astron-sdc/design-system/commit/2479e1f205e21aa2d4f6961e7df9cfcf544ab452))

# [3.13.0](https://gitlab.com/astron-sdc/design-system/compare/v3.12.0...v3.13.0) (2024-12-16)


### Features

* notification bar ([cf8c69b](https://gitlab.com/astron-sdc/design-system/commit/cf8c69b72e1dfe51f05902bc3e654e22bc9fdd76))
* notification bar ([c3d8ed3](https://gitlab.com/astron-sdc/design-system/commit/c3d8ed33540a251bd14b734b201937b3764c666b))

# [3.12.0](https://gitlab.com/astron-sdc/design-system/compare/v3.11.3...v3.12.0) (2024-11-29)


### Bug Fixes

* make button text black ([4cf9ffa](https://gitlab.com/astron-sdc/design-system/commit/4cf9ffa9c982134db5adbd9a0674af1c16ffe438))
* remove unused buttonlabel ([b3e50d0](https://gitlab.com/astron-sdc/design-system/commit/b3e50d04b2484521161673cc09f26d82e38e51d0))
* whitespace ([af6af6f](https://gitlab.com/astron-sdc/design-system/commit/af6af6f4b813344b3e6f689efd200eb555287b2c))


### Features

* add all button types to button stories ([876fa1a](https://gitlab.com/astron-sdc/design-system/commit/876fa1a2b76f508bd301039de9ce74224a94311a))
* make badge text black ([398779c](https://gitlab.com/astron-sdc/design-system/commit/398779c95f076ca269a1e65e8d10de8ce93dd1da))
* update palette, fix disabled button style ([a0a42f9](https://gitlab.com/astron-sdc/design-system/commit/a0a42f91afc848762966d30a4c59be61783a0f92))

## [3.11.3](https://gitlab.com/astron-sdc/design-system/compare/v3.11.2...v3.11.3) (2024-11-14)


### Bug Fixes

* readme change to trigger release ([39a866f](https://gitlab.com/astron-sdc/design-system/commit/39a866f648e65da07fb9e03b0788510fe97aa8d5))

## [3.11.2](https://gitlab.com/astron-sdc/design-system/compare/v3.11.1...v3.11.2) (2024-10-14)


### Bug Fixes

* export DateInput2 ([4865e51](https://gitlab.com/astron-sdc/design-system/commit/4865e5140f581e943e5d57b890267892fb7932bc))

## [3.11.1](https://gitlab.com/astron-sdc/design-system/compare/v3.11.0...v3.11.1) (2024-10-12)


### Bug Fixes

* datepicker should not submit on its own ([04b6626](https://gitlab.com/astron-sdc/design-system/commit/04b6626f96cdc20ef3879a2afb71df2f0d1374e0))

# [3.11.0](https://gitlab.com/astron-sdc/design-system/compare/v3.10.0...v3.11.0) (2024-10-02)


### Bug Fixes

* typo, cleanup ([fcb12ba](https://gitlab.com/astron-sdc/design-system/commit/fcb12ba48b3c141d45a184ca70746463430b61e8))


### Features

* add reactnode childcontent support to modal ([46aedbc](https://gitlab.com/astron-sdc/design-system/commit/46aedbcfc99c2dd3ec6bfabce4edf70fb2886703))

# [3.10.0](https://gitlab.com/astron-sdc/design-system/compare/v3.9.4...v3.10.0) (2024-09-17)


### Bug Fixes

* slider missing label and arialabel ([98530d3](https://gitlab.com/astron-sdc/design-system/commit/98530d3c94d968f4c4008fdb5b5e651c4bc12e86))


### Features

* export InputLabel ([5a58d0d](https://gitlab.com/astron-sdc/design-system/commit/5a58d0d1c546b966b1c4339852855fc689363faf))

## [3.9.4](https://gitlab.com/astron-sdc/design-system/compare/v3.9.3...v3.9.4) (2024-09-04)


### Bug Fixes

* table checkbox (copied from primereact theme css) ([b64bdbf](https://gitlab.com/astron-sdc/design-system/commit/b64bdbf0b93beace20bc469ef373a714b85f9f3d))
* table header height ([4c71172](https://gitlab.com/astron-sdc/design-system/commit/4c711722413a3b78532b1f2225cfd159389c78c9))

## [3.9.3](https://gitlab.com/astron-sdc/design-system/compare/v3.9.2...v3.9.3) (2024-09-03)


### Bug Fixes

* update packages ([be298d9](https://gitlab.com/astron-sdc/design-system/commit/be298d9ef9f1f314bf597f5e3a9692eb86b84fff))

## [3.9.2](https://gitlab.com/astron-sdc/design-system/compare/v3.9.1...v3.9.2) (2024-08-23)


### Bug Fixes

* Fix package contents ([24244ff](https://gitlab.com/astron-sdc/design-system/commit/24244ff823453bb8df4f46734257634722d59aa4))

## [3.9.1](https://gitlab.com/astron-sdc/design-system/compare/v3.9.0...v3.9.1) (2024-08-21)


### Bug Fixes

* Export missing chart types ([dda3fc6](https://gitlab.com/astron-sdc/design-system/commit/dda3fc6dd60abe446fd3d02c0ee34b5f5175dbb8))

# [3.9.0](https://gitlab.com/astron-sdc/design-system/compare/v3.8.0...v3.9.0) (2024-08-20)


### Features

* Add four chart types based on PrimeReact/ChartJS ([e615a5c](https://gitlab.com/astron-sdc/design-system/commit/e615a5c9a6af4e3d4d8fa82b119bc1a7ca520b51))
* Add four chart types based on PrimeReact/ChartJS ([d17afde](https://gitlab.com/astron-sdc/design-system/commit/d17afde2b110bb614ce63e03052f791fed4013b8))

# [3.8.0](https://gitlab.com/astron-sdc/design-system/compare/v3.7.0...v3.8.0) (2024-07-23)


### Features

* Trigger on purpose minor release for ProfileDropDown (No code change ([8ef8da2](https://gitlab.com/astron-sdc/design-system/commit/8ef8da2db723c59827feae1e11dc8ad8bcf144bd))

# [3.7.0](https://gitlab.com/astron-sdc/design-system/compare/v3.6.0...v3.7.0) (2024-07-09)


### Bug Fixes

* export all components (can we automate this) ([101a61c](https://gitlab.com/astron-sdc/design-system/commit/101a61c9fa430bd81f132aa59496c1bf1484ce33))
* tooltip text color ([89374be](https://gitlab.com/astron-sdc/design-system/commit/89374beb9ece7aa089185497d66ac3f8d5af525b))


### Features

* built-in tooltips on sidebar buttons ([4ef0e54](https://gitlab.com/astron-sdc/design-system/commit/4ef0e54a8d4a2fa22d474fdc2b214c481dd1eecc))
* tooltip component ([871dc2e](https://gitlab.com/astron-sdc/design-system/commit/871dc2edfa34d16b276a03cdaab2629e2d66f3b1))
* tooltip on icons and other functional components ([a44dd18](https://gitlab.com/astron-sdc/design-system/commit/a44dd1852772536da33b35699adf153c128d3e61))

# [3.6.0](https://gitlab.com/astron-sdc/design-system/compare/v3.5.1...v3.6.0) (2024-07-05)


### Features

* initial user profile ([7e76e63](https://gitlab.com/astron-sdc/design-system/commit/7e76e636f340288ca9edfd21f29c4c07fd56a8b6))

## [3.5.1](https://gitlab.com/astron-sdc/design-system/compare/v3.5.0...v3.5.1) (2024-06-19)


### Bug Fixes

* expose palette.ts for tailwind config import ([0b38802](https://gitlab.com/astron-sdc/design-system/commit/0b388027da4fa49d63186128ec68d198775de2e4))

# [3.5.0](https://gitlab.com/astron-sdc/design-system/compare/v3.4.1...v3.5.0) (2024-06-19)


### Bug Fixes

* colors.mdx cannot import nextui ([91da44b](https://gitlab.com/astron-sdc/design-system/commit/91da44be0e0f160a48db81adef14424f5eb78e3d))
* import palette ([82f8f00](https://gitlab.com/astron-sdc/design-system/commit/82f8f00ff82c8d1f1c3ad45ff10bc885ecf5beac))


### Features

* expose table column props ([910a773](https://gitlab.com/astron-sdc/design-system/commit/910a77306c016979657854e25bb9092fdfc69841))

## [3.4.1](https://gitlab.com/astron-sdc/design-system/compare/v3.4.0...v3.4.1) (2024-06-18)


### Bug Fixes

* Trigger release bot with a relevant change ([8de5eff](https://gitlab.com/astron-sdc/design-system/commit/8de5eff4189b6342856924f9d4d74d67d0473004))

# [3.4.0](https://gitlab.com/astron-sdc/design-system/compare/v3.3.1...v3.4.0) (2024-06-18)


### Features

* make nextui external ([cceb005](https://gitlab.com/astron-sdc/design-system/commit/cceb0055b23961f258022c8fdccc30b3bf7a8e27))

## [3.3.1](https://gitlab.com/astron-sdc/design-system/compare/v3.3.0...v3.3.1) (2024-06-17)


### Bug Fixes

* packages updates ([656a1cc](https://gitlab.com/astron-sdc/design-system/commit/656a1ccf3cd9aec020d40ba2fb4d85dffb840d87))

# [3.3.0](https://gitlab.com/astron-sdc/design-system/compare/v3.2.10...v3.3.0) (2024-06-17)


### Features

* add sorting ([cb4acf2](https://gitlab.com/astron-sdc/design-system/commit/cb4acf2efc1796fb3bdf1a8d42964321a2a4fffd))

## [3.2.10](https://gitlab.com/astron-sdc/design-system/compare/v3.2.9...v3.2.10) (2024-06-16)


### Bug Fixes

* added shadow from design ([86ef538](https://gitlab.com/astron-sdc/design-system/commit/86ef5384f614a56de932cffe61ede932c5078e8c))

## [3.2.9](https://gitlab.com/astron-sdc/design-system/compare/v3.2.8...v3.2.9) (2024-06-16)


### Bug Fixes

* sidebar mover Should not be block ([997c83e](https://gitlab.com/astron-sdc/design-system/commit/997c83eb7dcc7d8de2fd2fddd4217d2bb041400e))

## [3.2.8](https://gitlab.com/astron-sdc/design-system/compare/v3.2.7...v3.2.8) (2024-06-15)


### Bug Fixes

* respect color ([572cd7b](https://gitlab.com/astron-sdc/design-system/commit/572cd7bbf06877c09fb66420dffebfb09a7eaa9b))

## [3.2.7](https://gitlab.com/astron-sdc/design-system/compare/v3.2.6...v3.2.7) (2024-06-15)


### Bug Fixes

* default background is black according to design; and menu is darkblue ([06e4e0d](https://gitlab.com/astron-sdc/design-system/commit/06e4e0deb04cbd53110b8f33a575ad50f11ec0d3))

## [3.2.6](https://gitlab.com/astron-sdc/design-system/compare/v3.2.5...v3.2.6) (2024-06-15)


### Bug Fixes

* icon should not be block ([7b07f30](https://gitlab.com/astron-sdc/design-system/commit/7b07f30222f70d9018aa0bda60dd8db9eff151aa))

## [3.2.5](https://gitlab.com/astron-sdc/design-system/compare/v3.2.4...v3.2.5) (2024-06-15)


### Bug Fixes

*  sidebar  should be a grid and not a block for alignment ([703ed20](https://gitlab.com/astron-sdc/design-system/commit/703ed20a2ad8dc8f877430fdc6e02951714a2bdc))

## [3.2.4](https://gitlab.com/astron-sdc/design-system/compare/v3.2.3...v3.2.4) (2024-06-12)


### Bug Fixes

* Typography be like normal ([c19d141](https://gitlab.com/astron-sdc/design-system/commit/c19d141e026a11138a724cbcace5ba5af8f80177))

## [3.2.3](https://gitlab.com/astron-sdc/design-system/compare/v3.2.2...v3.2.3) (2024-06-04)


### Bug Fixes

* aria-label warning in dropdown component ([c3f2ef2](https://gitlab.com/astron-sdc/design-system/commit/c3f2ef2a52f6996dfa0fba06008008de39ea4506))

## [3.2.2](https://gitlab.com/astron-sdc/design-system/compare/v3.2.1...v3.2.2) (2024-06-03)


### Bug Fixes

* alert and toast z-index ([2e7db22](https://gitlab.com/astron-sdc/design-system/commit/2e7db2269504fe5a1c6ccf814beec8f9c5204c02))

## [3.2.1](https://gitlab.com/astron-sdc/design-system/compare/v3.2.0...v3.2.1) (2024-06-03)


### Bug Fixes

* trigger release for patch in content scroll ([4f0394a](https://gitlab.com/astron-sdc/design-system/commit/4f0394a9d57f2685ff86ff2ba3a8c90ec380ef2c))

# [3.2.0](https://gitlab.com/astron-sdc/design-system/compare/v3.1.2...v3.2.0) (2024-06-03)


### Bug Fixes

* width of input element should follow parent ([8f437c3](https://gitlab.com/astron-sdc/design-system/commit/8f437c3482097591434f6d6c6e606694d99f9d62))


### Features

* typography new variant errorMessage ([f3a51be](https://gitlab.com/astron-sdc/design-system/commit/f3a51be31bf06a06a2fac2409bfe5502b6a4b212))

## [3.1.2](https://gitlab.com/astron-sdc/design-system/compare/v3.1.1...v3.1.2) (2024-05-28)


### Bug Fixes

* make react-router-dom external ([f92e67b](https://gitlab.com/astron-sdc/design-system/commit/f92e67bd51fd4b22b0d13ae0fad54b8bcbae2187))

## [3.1.1](https://gitlab.com/astron-sdc/design-system/compare/v3.1.0...v3.1.1) (2024-05-27)


### Bug Fixes

* export SidebarItem and IconName type ([bb95633](https://gitlab.com/astron-sdc/design-system/commit/bb956336e9acf24e5a0188907cee8a333dca081f))

# [3.1.0](https://gitlab.com/astron-sdc/design-system/compare/v3.0.3...v3.1.0) (2024-05-23)


### Features

* Initial sidebar & application layout ([b70631b](https://gitlab.com/astron-sdc/design-system/commit/b70631bf598cc7eda22112fed130aac7e9d41539))

## [3.0.3](https://gitlab.com/astron-sdc/design-system/compare/v3.0.2...v3.0.3) (2024-05-07)


### Bug Fixes

* dropdown popover dark mode background ([131c17e](https://gitlab.com/astron-sdc/design-system/commit/131c17ea609cc5cdfd0a843ee1ea5270ba380004))

## [3.0.2](https://gitlab.com/astron-sdc/design-system/compare/v3.0.1...v3.0.2) (2024-05-06)


### Bug Fixes

* tailwind config in module exports ([9eec526](https://gitlab.com/astron-sdc/design-system/commit/9eec526d6a165a390d74f797d3a90daed11d4978))

## [3.0.1](https://gitlab.com/astron-sdc/design-system/compare/v3.0.0...v3.0.1) (2024-05-02)


### Bug Fixes

* dependencies ([5aba025](https://gitlab.com/astron-sdc/design-system/commit/5aba025290b869227c2ec14a8900dc0e31386365))
* tailwind config not exposed ([fd43ee7](https://gitlab.com/astron-sdc/design-system/commit/fd43ee7af9a817ebfb148eef6989fa0b8135aff8))
* update dependencies ([d22fbc2](https://gitlab.com/astron-sdc/design-system/commit/d22fbc2fba22ebc640e6cb3c3bfd80d266992f5f))
* vite version ([228910c](https://gitlab.com/astron-sdc/design-system/commit/228910c1c762dfd3749af6bf43a7740b7483f2d9))

# [3.0.0](https://gitlab.com/astron-sdc/design-system/compare/v2.10.0...v3.0.0) (2024-05-02)


### Features

* standardize colors, expose tailwind config ([6adc526](https://gitlab.com/astron-sdc/design-system/commit/6adc52625873a52dfcbec0982b674a70240f8a08))


### BREAKING CHANGES

* a hacky way to trigger a new release

# [2.10.0](https://gitlab.com/astron-sdc/design-system/compare/v2.9.0...v2.10.0) (2024-05-01)


### Bug Fixes

* fileinput text font ([37b5fce](https://gitlab.com/astron-sdc/design-system/commit/37b5fced1639faa95d9ec4d97810704eae42a784))
* radio label font ([df51e6c](https://gitlab.com/astron-sdc/design-system/commit/df51e6c8eecd6b978fd52665dffcc9df003abcae))
* toast background ([2ec658a](https://gitlab.com/astron-sdc/design-system/commit/2ec658a7a9c8c613b2ce5e7492f10e99cba561ba))


### Features

* breadcrumb support custom item component via template ([e9405d1](https://gitlab.com/astron-sdc/design-system/commit/e9405d14f23e7f47f798964cd553b88247c331de))

# [2.9.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.8.0...v2.9.0) (2024-04-24)


### Features

* general error component ([fba187c](https://git.astron.nl/astron-sdc/design-system/commit/fba187cfe19776b833a6cf6287131d723c838cd9))

# [2.8.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.7.0...v2.8.0) (2024-04-11)


### Features

* Export alert props and colors ([0aa1638](https://git.astron.nl/astron-sdc/design-system/commit/0aa16388874aaa60e5dac7b084de0dc6924609a8))

# [2.7.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.6.0...v2.7.0) (2024-04-11)


### Features

* export alertProps ([32f9192](https://git.astron.nl/astron-sdc/design-system/commit/32f91928871661b07904862730cbd8da24e06114))

# [2.6.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.5.0...v2.6.0) (2024-04-10)


### Features

* Make alert dom removal optional ([30ad616](https://git.astron.nl/astron-sdc/design-system/commit/30ad616bc73ead17feabe378a74e5c1339717610))

# [2.5.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.4.0...v2.5.0) (2024-04-10)


### Features

* add tabs ([6328dd7](https://git.astron.nl/astron-sdc/design-system/commit/6328dd72ca1e049f82851f074cc7393a19380061))

# [2.4.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.3.0...v2.4.0) (2024-04-09)


### Features

* onExpire callback for alerts, to do state changes ([f036395](https://git.astron.nl/astron-sdc/design-system/commit/f036395eac709a64069aeca262832151af662b77))

# [2.3.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.2.0...v2.3.0) (2024-04-09)


### Features

* Editable cell style in Table and expose body template property, toasty alerts, spinner ([9597393](https://git.astron.nl/astron-sdc/design-system/commit/9597393e8a8b03368c43a94eafc2d498ea2256c2))

# [2.2.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.1.1...v2.2.0) (2024-04-02)


### Bug Fixes

* icons in table stories ([647555c](https://git.astron.nl/astron-sdc/design-system/commit/647555cde72e8c1e5fed355d5f9911856b92a995))


### Features

* use empty state in table component ([469db3a](https://git.astron.nl/astron-sdc/design-system/commit/469db3a6712771c3f8752af2098b908f8c47b970))

## [2.1.1](https://git.astron.nl/astron-sdc/design-system/compare/v2.1.0...v2.1.1) (2024-03-28)


### Bug Fixes

* export all components ([6fbf5f6](https://git.astron.nl/astron-sdc/design-system/commit/6fbf5f6b525a9a5a1b867173616e787a51e64820))
* export all components ([d1617b3](https://git.astron.nl/astron-sdc/design-system/commit/d1617b30717db50eaefe10b0e62688e45a4a134f))

# [2.1.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.0.0...v2.1.0) (2024-03-28)


### Features

* empty states ([abcc1e0](https://git.astron.nl/astron-sdc/design-system/commit/abcc1e07760ea3aa064a12af91e6447701041304))

# [2.0.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.10.0...v2.0.0) (2024-03-27)


* feat!: make icons great again ([848ae25](https://git.astron.nl/astron-sdc/design-system/commit/848ae2585e96cb5fca52cf61ac770c8dbd9c8c29))


### BREAKING CHANGES

* changed icon names to PascalCase

# [1.10.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.9.1...v1.10.0) (2024-03-21)


### Features

* release editable datatable ([7f692bd](https://git.astron.nl/astron-sdc/design-system/commit/7f692bd10f4802222babda95db1491389429d885))

## [1.9.1](https://git.astron.nl/astron-sdc/design-system/compare/v1.9.0...v1.9.1) (2024-03-14)


### Bug Fixes

* explicitly set text color ([0ee58aa](https://git.astron.nl/astron-sdc/design-system/commit/0ee58aa2a2215725a7be6b0ef66eb29da19b0066))

# [1.9.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.8.4...v1.9.0) (2024-03-13)


### Features

* initial FileInput ([622589b](https://git.astron.nl/astron-sdc/design-system/commit/622589b488c3fcdf903e32400b9ec20e7ae89dd7))

## [1.8.4](https://git.astron.nl/astron-sdc/design-system/compare/v1.8.3...v1.8.4) (2024-03-11)


### Bug Fixes

* add name to input component ([8d85c69](https://git.astron.nl/astron-sdc/design-system/commit/8d85c69e1ce817cbdccc9bca7d711bae086b0708))

## [1.8.3](https://git.astron.nl/astron-sdc/design-system/compare/v1.8.2...v1.8.3) (2024-03-07)


### Bug Fixes

* prevent warning about missing key in breadcrumb ([a05b04e](https://git.astron.nl/astron-sdc/design-system/commit/a05b04e93dbbc815363aa829748245be23363527))

## [1.8.2](https://git.astron.nl/astron-sdc/design-system/compare/v1.8.1...v1.8.2) (2024-03-07)


### Bug Fixes

* toggle component styling same as other inputs ([064b121](https://git.astron.nl/astron-sdc/design-system/commit/064b121c74e9196d1ddcb1805404f7519ce68939))

## [1.8.1](https://git.astron.nl/astron-sdc/design-system/compare/v1.8.0...v1.8.1) (2024-02-29)


### Bug Fixes

* label styling ([8be3531](https://git.astron.nl/astron-sdc/design-system/commit/8be3531a94de5814a6fde3bb162e4676a4ef2992))

# [1.8.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.7.0...v1.8.0) (2024-02-29)


### Bug Fixes

* disabled form components ([87d65eb](https://git.astron.nl/astron-sdc/design-system/commit/87d65eb3c9429ef5907b0f448a10306203e6cdd9))


### Features

* label styling support for form components ([8337564](https://git.astron.nl/astron-sdc/design-system/commit/83375641e8de47579901cc2b3696bd873b41ed8c))

# [1.7.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.6.0...v1.7.0) (2024-02-28)


### Features

* support input label placement customization ([3e8b819](https://git.astron.nl/astron-sdc/design-system/commit/3e8b819177ec907ae21fa49f79cf29fd2668f0c4))

# [1.6.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.5.1...v1.6.0) (2024-02-27)


### Features

* add up icon ([39a2f74](https://git.astron.nl/astron-sdc/design-system/commit/39a2f742a9d1cf670d8c82b2a2b02194d15fbf66))

## [1.5.1](https://git.astron.nl/astron-sdc/design-system/compare/v1.5.0...v1.5.1) (2024-02-26)


### Bug Fixes

* define icon width and height ([b6d2e73](https://git.astron.nl/astron-sdc/design-system/commit/b6d2e730876a1fcffd09ce97e39e1896043df354))

# [1.5.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.4.1...v1.5.0) (2024-02-26)


### Features

* add maxlength, readonly, type, and input mode attributes to textinput ([30bbfda](https://git.astron.nl/astron-sdc/design-system/commit/30bbfda3dbe96d132cce0b7469d55722181b8a67))

## [1.4.1](https://git.astron.nl/astron-sdc/design-system/compare/v1.4.0...v1.4.1) (2024-02-23)


### Bug Fixes

* export all components ([c90551c](https://git.astron.nl/astron-sdc/design-system/commit/c90551cdc8dd3e9582d6d02fd197c34c83f135d1))

# [1.4.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.3.0...v1.4.0) (2024-02-23)


### Features

* basic table component ([8f14d8b](https://git.astron.nl/astron-sdc/design-system/commit/8f14d8b53796714be20898cc9ee73be401837139))
* table with custom cells and selectable rows ([cb936ae](https://git.astron.nl/astron-sdc/design-system/commit/cb936aee0a819f509e72a5483ba423f5443a1838))
* table with header content, reorderable rows ([b5decd7](https://git.astron.nl/astron-sdc/design-system/commit/b5decd7f8eed21eb40fc49f27f849059fb494834))

# [1.3.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.2.0...v1.3.0) (2024-02-06)

### Bug Fixes

- menu stories types ([14bd0e0](https://git.astron.nl/astron-sdc/design-system/commit/14bd0e0162867b66227879f7e53bfe11c654754e))
- react datepicker styling ([e4bf330](https://git.astron.nl/astron-sdc/design-system/commit/e4bf330636f56a54541c589c032f7fdbcab33c9e))
- temporary fix on datepicker popper ([dcec01f](https://git.astron.nl/astron-sdc/design-system/commit/dcec01f11a1d10846c44e2ea040b1ebb8e782a8c))

### Features

- add action menu and profile menu components ([cee33f4](https://git.astron.nl/astron-sdc/design-system/commit/cee33f47792c8dc07c76fe76ee2a7441bc942a3c))
- add breadcrumb component ([b7f73ce](https://git.astron.nl/astron-sdc/design-system/commit/b7f73ce732cff35e380bcb56fe0912de7b778a98))
- add support for date range input ([61fef91](https://git.astron.nl/astron-sdc/design-system/commit/61fef911cba2fb8edbfcaf6b5b88d87bc10f444a))
- progress bar component ([5fc1a3e](https://git.astron.nl/astron-sdc/design-system/commit/5fc1a3ef24023e60c37ade7ac91c9c2e337c49ac))

# [1.2.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.1.1...v1.2.0) (2023-12-13)

### Bug Fixes

- date input should open to selected date when it exists ([f563d18](https://git.astron.nl/astron-sdc/design-system/commit/f563d18afe2e294a4000be547ec72ec160771893))

### Features

- button should support option to listen for enter ([f648d97](https://git.astron.nl/astron-sdc/design-system/commit/f648d978a05bf351871bd6acf55818d514653d70))
- form components required prop ([ae89d32](https://git.astron.nl/astron-sdc/design-system/commit/ae89d321e956145be60689d18f53ba60919e643e))
- input hover and focus states ([6986ba4](https://git.astron.nl/astron-sdc/design-system/commit/6986ba44b89ce944058fdd452756280222c8fe4b))
- radio and checkbox disabled state ([c9cef27](https://git.astron.nl/astron-sdc/design-system/commit/c9cef27337c2777d3db4dcb43c59036fa7db4a19))
- text input disabled state ([4b4823f](https://git.astron.nl/astron-sdc/design-system/commit/4b4823f48e02531a547baa9a324b7d352b5a1462))
- toggle disabled state ([ee9562f](https://git.astron.nl/astron-sdc/design-system/commit/ee9562f600d65ed6a9329025c6a9cf782d8f7c17))

## [1.1.1](https://git.astron.nl/astron-sdc/design-system/compare/v1.1.0...v1.1.1) (2023-12-13)

### Bug Fixes

- dropdown focus color ([c725c87](https://git.astron.nl/astron-sdc/design-system/commit/c725c8799c05e6960ce6044e6dd891ccaf00c63b))
- dropdown selected and hover color ([5d25e6b](https://git.astron.nl/astron-sdc/design-system/commit/5d25e6b496296fd9ebbaa5bcca702cf8b55ed09f))
- toggle direction and color ([46cb9f3](https://git.astron.nl/astron-sdc/design-system/commit/46cb9f3a0fa8b2eda7a9906678265d3a5cafaa28))

# [1.1.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.0.1...v1.1.0) (2023-12-06)

### Bug Fixes

- install git from default before_script ([c80af9a](https://git.astron.nl/astron-sdc/design-system/commit/c80af9a06326a43884d838368f2a396c6238956f))
- remove release notes from commit msg ([0e6c1d8](https://git.astron.nl/astron-sdc/design-system/commit/0e6c1d878a9d737ea27aedd0940dabe52819b4ca))

### Features

- also publish package in semantic-release ([caa7ee8](https://git.astron.nl/astron-sdc/design-system/commit/caa7ee8f3bf71a8970f31ea29bb7925539e8d418))
- semantic release for real ([dd3dd47](https://git.astron.nl/astron-sdc/design-system/commit/dd3dd478b83324c950b1b477e8164922bf8dcc6a))
