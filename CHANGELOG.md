# [2.9.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.8.0...v2.9.0) (2024-04-24)


### Features

* general error component ([fba187c](https://git.astron.nl/astron-sdc/design-system/commit/fba187cfe19776b833a6cf6287131d723c838cd9))

# [2.8.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.7.0...v2.8.0) (2024-04-11)


### Features

* Export alert props and colors ([0aa1638](https://git.astron.nl/astron-sdc/design-system/commit/0aa16388874aaa60e5dac7b084de0dc6924609a8))

# [2.7.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.6.0...v2.7.0) (2024-04-11)


### Features

* export alertProps ([32f9192](https://git.astron.nl/astron-sdc/design-system/commit/32f91928871661b07904862730cbd8da24e06114))

# [2.6.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.5.0...v2.6.0) (2024-04-10)


### Features

* Make alert dom removal optional ([30ad616](https://git.astron.nl/astron-sdc/design-system/commit/30ad616bc73ead17feabe378a74e5c1339717610))

# [2.5.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.4.0...v2.5.0) (2024-04-10)


### Features

* add tabs ([6328dd7](https://git.astron.nl/astron-sdc/design-system/commit/6328dd72ca1e049f82851f074cc7393a19380061))

# [2.4.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.3.0...v2.4.0) (2024-04-09)


### Features

* onExpire callback for alerts, to do state changes ([f036395](https://git.astron.nl/astron-sdc/design-system/commit/f036395eac709a64069aeca262832151af662b77))

# [2.3.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.2.0...v2.3.0) (2024-04-09)


### Features

* Editable cell style in Table and expose body template property, toasty alerts, spinner ([9597393](https://git.astron.nl/astron-sdc/design-system/commit/9597393e8a8b03368c43a94eafc2d498ea2256c2))

# [2.2.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.1.1...v2.2.0) (2024-04-02)


### Bug Fixes

* icons in table stories ([647555c](https://git.astron.nl/astron-sdc/design-system/commit/647555cde72e8c1e5fed355d5f9911856b92a995))


### Features

* use empty state in table component ([469db3a](https://git.astron.nl/astron-sdc/design-system/commit/469db3a6712771c3f8752af2098b908f8c47b970))

## [2.1.1](https://git.astron.nl/astron-sdc/design-system/compare/v2.1.0...v2.1.1) (2024-03-28)


### Bug Fixes

* export all components ([6fbf5f6](https://git.astron.nl/astron-sdc/design-system/commit/6fbf5f6b525a9a5a1b867173616e787a51e64820))
* export all components ([d1617b3](https://git.astron.nl/astron-sdc/design-system/commit/d1617b30717db50eaefe10b0e62688e45a4a134f))

# [2.1.0](https://git.astron.nl/astron-sdc/design-system/compare/v2.0.0...v2.1.0) (2024-03-28)


### Features

* empty states ([abcc1e0](https://git.astron.nl/astron-sdc/design-system/commit/abcc1e07760ea3aa064a12af91e6447701041304))

# [2.0.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.10.0...v2.0.0) (2024-03-27)


* feat!: make icons great again ([848ae25](https://git.astron.nl/astron-sdc/design-system/commit/848ae2585e96cb5fca52cf61ac770c8dbd9c8c29))


### BREAKING CHANGES

* changed icon names to PascalCase

# [1.10.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.9.1...v1.10.0) (2024-03-21)


### Features

* release editable datatable ([7f692bd](https://git.astron.nl/astron-sdc/design-system/commit/7f692bd10f4802222babda95db1491389429d885))

## [1.9.1](https://git.astron.nl/astron-sdc/design-system/compare/v1.9.0...v1.9.1) (2024-03-14)


### Bug Fixes

* explicitly set text color ([0ee58aa](https://git.astron.nl/astron-sdc/design-system/commit/0ee58aa2a2215725a7be6b0ef66eb29da19b0066))

# [1.9.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.8.4...v1.9.0) (2024-03-13)


### Features

* initial FileInput ([622589b](https://git.astron.nl/astron-sdc/design-system/commit/622589b488c3fcdf903e32400b9ec20e7ae89dd7))

## [1.8.4](https://git.astron.nl/astron-sdc/design-system/compare/v1.8.3...v1.8.4) (2024-03-11)


### Bug Fixes

* add name to input component ([8d85c69](https://git.astron.nl/astron-sdc/design-system/commit/8d85c69e1ce817cbdccc9bca7d711bae086b0708))

## [1.8.3](https://git.astron.nl/astron-sdc/design-system/compare/v1.8.2...v1.8.3) (2024-03-07)


### Bug Fixes

* prevent warning about missing key in breadcrumb ([a05b04e](https://git.astron.nl/astron-sdc/design-system/commit/a05b04e93dbbc815363aa829748245be23363527))

## [1.8.2](https://git.astron.nl/astron-sdc/design-system/compare/v1.8.1...v1.8.2) (2024-03-07)


### Bug Fixes

* toggle component styling same as other inputs ([064b121](https://git.astron.nl/astron-sdc/design-system/commit/064b121c74e9196d1ddcb1805404f7519ce68939))

## [1.8.1](https://git.astron.nl/astron-sdc/design-system/compare/v1.8.0...v1.8.1) (2024-02-29)


### Bug Fixes

* label styling ([8be3531](https://git.astron.nl/astron-sdc/design-system/commit/8be3531a94de5814a6fde3bb162e4676a4ef2992))

# [1.8.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.7.0...v1.8.0) (2024-02-29)


### Bug Fixes

* disabled form components ([87d65eb](https://git.astron.nl/astron-sdc/design-system/commit/87d65eb3c9429ef5907b0f448a10306203e6cdd9))


### Features

* label styling support for form components ([8337564](https://git.astron.nl/astron-sdc/design-system/commit/83375641e8de47579901cc2b3696bd873b41ed8c))

# [1.7.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.6.0...v1.7.0) (2024-02-28)


### Features

* support input label placement customization ([3e8b819](https://git.astron.nl/astron-sdc/design-system/commit/3e8b819177ec907ae21fa49f79cf29fd2668f0c4))

# [1.6.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.5.1...v1.6.0) (2024-02-27)


### Features

* add up icon ([39a2f74](https://git.astron.nl/astron-sdc/design-system/commit/39a2f742a9d1cf670d8c82b2a2b02194d15fbf66))

## [1.5.1](https://git.astron.nl/astron-sdc/design-system/compare/v1.5.0...v1.5.1) (2024-02-26)


### Bug Fixes

* define icon width and height ([b6d2e73](https://git.astron.nl/astron-sdc/design-system/commit/b6d2e730876a1fcffd09ce97e39e1896043df354))

# [1.5.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.4.1...v1.5.0) (2024-02-26)


### Features

* add maxlength, readonly, type, and input mode attributes to textinput ([30bbfda](https://git.astron.nl/astron-sdc/design-system/commit/30bbfda3dbe96d132cce0b7469d55722181b8a67))

## [1.4.1](https://git.astron.nl/astron-sdc/design-system/compare/v1.4.0...v1.4.1) (2024-02-23)


### Bug Fixes

* export all components ([c90551c](https://git.astron.nl/astron-sdc/design-system/commit/c90551cdc8dd3e9582d6d02fd197c34c83f135d1))

# [1.4.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.3.0...v1.4.0) (2024-02-23)


### Features

* basic table component ([8f14d8b](https://git.astron.nl/astron-sdc/design-system/commit/8f14d8b53796714be20898cc9ee73be401837139))
* table with custom cells and selectable rows ([cb936ae](https://git.astron.nl/astron-sdc/design-system/commit/cb936aee0a819f509e72a5483ba423f5443a1838))
* table with header content, reorderable rows ([b5decd7](https://git.astron.nl/astron-sdc/design-system/commit/b5decd7f8eed21eb40fc49f27f849059fb494834))

# [1.3.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.2.0...v1.3.0) (2024-02-06)

### Bug Fixes

- menu stories types ([14bd0e0](https://git.astron.nl/astron-sdc/design-system/commit/14bd0e0162867b66227879f7e53bfe11c654754e))
- react datepicker styling ([e4bf330](https://git.astron.nl/astron-sdc/design-system/commit/e4bf330636f56a54541c589c032f7fdbcab33c9e))
- temporary fix on datepicker popper ([dcec01f](https://git.astron.nl/astron-sdc/design-system/commit/dcec01f11a1d10846c44e2ea040b1ebb8e782a8c))

### Features

- add action menu and profile menu components ([cee33f4](https://git.astron.nl/astron-sdc/design-system/commit/cee33f47792c8dc07c76fe76ee2a7441bc942a3c))
- add breadcrumb component ([b7f73ce](https://git.astron.nl/astron-sdc/design-system/commit/b7f73ce732cff35e380bcb56fe0912de7b778a98))
- add support for date range input ([61fef91](https://git.astron.nl/astron-sdc/design-system/commit/61fef911cba2fb8edbfcaf6b5b88d87bc10f444a))
- progress bar component ([5fc1a3e](https://git.astron.nl/astron-sdc/design-system/commit/5fc1a3ef24023e60c37ade7ac91c9c2e337c49ac))

# [1.2.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.1.1...v1.2.0) (2023-12-13)

### Bug Fixes

- date input should open to selected date when it exists ([f563d18](https://git.astron.nl/astron-sdc/design-system/commit/f563d18afe2e294a4000be547ec72ec160771893))

### Features

- button should support option to listen for enter ([f648d97](https://git.astron.nl/astron-sdc/design-system/commit/f648d978a05bf351871bd6acf55818d514653d70))
- form components required prop ([ae89d32](https://git.astron.nl/astron-sdc/design-system/commit/ae89d321e956145be60689d18f53ba60919e643e))
- input hover and focus states ([6986ba4](https://git.astron.nl/astron-sdc/design-system/commit/6986ba44b89ce944058fdd452756280222c8fe4b))
- radio and checkbox disabled state ([c9cef27](https://git.astron.nl/astron-sdc/design-system/commit/c9cef27337c2777d3db4dcb43c59036fa7db4a19))
- text input disabled state ([4b4823f](https://git.astron.nl/astron-sdc/design-system/commit/4b4823f48e02531a547baa9a324b7d352b5a1462))
- toggle disabled state ([ee9562f](https://git.astron.nl/astron-sdc/design-system/commit/ee9562f600d65ed6a9329025c6a9cf782d8f7c17))

## [1.1.1](https://git.astron.nl/astron-sdc/design-system/compare/v1.1.0...v1.1.1) (2023-12-13)

### Bug Fixes

- dropdown focus color ([c725c87](https://git.astron.nl/astron-sdc/design-system/commit/c725c8799c05e6960ce6044e6dd891ccaf00c63b))
- dropdown selected and hover color ([5d25e6b](https://git.astron.nl/astron-sdc/design-system/commit/5d25e6b496296fd9ebbaa5bcca702cf8b55ed09f))
- toggle direction and color ([46cb9f3](https://git.astron.nl/astron-sdc/design-system/commit/46cb9f3a0fa8b2eda7a9906678265d3a5cafaa28))

# [1.1.0](https://git.astron.nl/astron-sdc/design-system/compare/v1.0.1...v1.1.0) (2023-12-06)

### Bug Fixes

- install git from default before_script ([c80af9a](https://git.astron.nl/astron-sdc/design-system/commit/c80af9a06326a43884d838368f2a396c6238956f))
- remove release notes from commit msg ([0e6c1d8](https://git.astron.nl/astron-sdc/design-system/commit/0e6c1d878a9d737ea27aedd0940dabe52819b4ca))

### Features

- also publish package in semantic-release ([caa7ee8](https://git.astron.nl/astron-sdc/design-system/commit/caa7ee8f3bf71a8970f31ea29bb7925539e8d418))
- semantic release for real ([dd3dd47](https://git.astron.nl/astron-sdc/design-system/commit/dd3dd478b83324c950b1b477e8164922bf8dcc6a))
