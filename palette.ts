export const astronBrand = {
  lightBlue: "#00ADEE",
  blue: "#3A5896",
  blueWCAG: "#6C89C6"
};

export const astronAdditional = {
  yellow: "#D3D300",
  orange: "#FF9933",
  red: "#FF4444",
  green: "#20BB44",
  violet: "#AA33BB", // not used yet
  turquoise: "#11BBAA",
};

export const baseUI = {
  baseWhite: "#FFFFFF",
  baseBlack: "#000000",
  lightGrey: "#E9E9E9",
  grey: "#777777",
  mediumGrey: "#313131",
  darkModePanel: "#191919",
  astronBg: "#020202",
  darkBlue: "#181828",
};
