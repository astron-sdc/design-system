FROM nginx:mainline-alpine

COPY storybook-static /usr/share/nginx/html/design-system
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
